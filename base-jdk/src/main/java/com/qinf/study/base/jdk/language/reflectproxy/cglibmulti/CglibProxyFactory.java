package com.qinf.study.base.jdk.language.reflectproxy.cglibmulti;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * Created by qinf on 2020-10-05.
 */
public final class CglibProxyFactory implements MethodInterceptor {

    private final Enhancer enhancer = new Enhancer();

    private Object sourceObj;

    public Object getProxyObj(Object _sourceObj) {
        this.sourceObj = _sourceObj;
        enhancer.setSuperclass(this.sourceObj.getClass());
        // enhancer.setCallback(new DynamicProxyFactory());
        enhancer.setCallback(this);
        Object proxyObj = enhancer.create();
        // com.qinf.study.base.jdk.language.reflectproxy.cglibmulti.CglibEmptyProcessor$$EnhancerByCGLIB$$c8842bcd@1810399e
        System.out.println("proxy class has generated : " + proxyObj.getClass().getName());
        return proxyObj;
    }

    /**
     * The Object obj:  proxy object(a instance of CglibEmptyProcessor$$EnhancerByCGLIB$$c8842bcd@1810399e).
     * The Method method: is the method declared in delegate CglibEmptyProcessor.class.
     * The Object[] args: is the arguments passed in for method of delegate class.
     * The MethodProxy proxy: is a object can invoke CGLIB$performBusinessLogic$0() declared in proxy class.
     */
    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        System.out.println("Before perform business logic");
        Object result = proxy.invokeSuper(obj, args);
        System.out.println("After perform business logic");
        return result;
    }
}
