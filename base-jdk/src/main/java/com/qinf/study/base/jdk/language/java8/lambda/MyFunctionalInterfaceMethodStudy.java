package com.qinf.study.base.jdk.language.java8.lambda;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by qinf on 2018-12-19.<br>
 *
 * The concrete implementation class will extend all the
 * default methods defined in functional interface.
 */
public class MyFunctionalInterfaceMethodStudy implements MyFunctionalInterface {

    @Override
    public int mapSize(Map<?, ?> map) throws Exception {
        return 0;
    }

    public static void main(String... args) {
        Map<String, String> realMap = new HashMap<String, String>();
        realMap.put("k1", "v1");
        realMap.put("k2", "v2");

        MyFunctionalInterfaceMethodStudy methodStudy = new MyFunctionalInterfaceMethodStudy();
        methodStudy.printEachEle(realMap);
    }
}
