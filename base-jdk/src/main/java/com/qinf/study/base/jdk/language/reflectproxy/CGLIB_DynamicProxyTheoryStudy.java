package com.qinf.study.base.jdk.language.reflectproxy;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * Created by qinf on 2017/7/3 PM10:49:38
 * <p>
 * 1. The reason of using CGLib(code generator library) is :
 * When use JDK dynamic proxy, the delegate class must implement a interface, and the
 * generated proxy class can only agent the method declared in this interface. But in
 * CGLib, do not require the delegate class has to implement a interface, it can be a
 * common class, of cause, it can also implement a interface.
 * <p>
 * 2. The theory of CGLib dynamic proxy is :
 * In general, the theory of cglib is as same as jdk. The most obvious difference is the dynamic
 * generated proxy class. When in jdk dynamic proxy, the generated proxy class implements the interface
 * implemented by delegate class. But in cglib, the generated proxy class extends the delegate class.
 * Meanwhile you should pay attention that the private method cannot be proxied.
 * <p>
 * 3. The simplified step of using cglib dynamic proxy is :
 * As same as jdk.
 */
public class CGLIB_DynamicProxyTheoryStudy {

    public static void main(String[] args) {
        CGLIB_Delegate01Impl proxyClsInstance = (CGLIB_Delegate01Impl) new DynamicProxyFactory()
                .getDynamicProxyClassInstance(new CGLIB_Delegate01Impl());
        String performResult = proxyClsInstance.performBusinessLogic();
        System.out.println("the result of performing business logic is : " + performResult);
    }

    static final class DynamicProxyFactory implements MethodInterceptor {
        private final Enhancer proxyClsInstanceGenerator = new Enhancer();
        private Object delegateClsInstance;

        public Object getDynamicProxyClassInstance(Object realObj) {
            this.delegateClsInstance = realObj;
            proxyClsInstanceGenerator.setSuperclass(delegateClsInstance.getClass());
            // proxyClsInstanceGenerator.setCallback(new DynamicProxyFactory());
            proxyClsInstanceGenerator.setCallback(this);
            // the generated dynamic proxy class instance, the proxy class extended delegate class Delegate01Impl.class
            Object proxyClsInstance = proxyClsInstanceGenerator.create();
            // com.qinf.study.reflectproxy.Delegate01Impl$$EnhancerByCGLIB$$2ddc8973
            System.out.println("proxy class has generated : " + proxyClsInstance.getClass().getName());
            return proxyClsInstance;
        }

        /**
         * The Object obj :  is the generated cglib dynamic proxy class instance(a instance of Delegate01Impl$$EnhancerByCGLIB$$2ddc8973.class).
         * The Method method : is the method declared in delegate Delegate01Impl.class.
         * The Object[] args : is the arguments passed in for method of delegate class.
         * The MethodProxy proxy : is a object can invoke CGLIB$performBusinessLogic$0() declared in proxy class.
         */
        @Override
        public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
            System.out.println("before perform business logic");
            Object performResult = proxy.invokeSuper(obj, args);
            System.out.println("after perform business logic");
            return performResult;
        }
    }
}
