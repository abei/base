package com.qinf.study.base.jdk.cpu.juc.base_lock;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by qinf on 2018/1/1 PM9:02:02
 * <p>
 * 1. Write lock is exclusive lock, for other both write and read thread.
 * 2. Read lock is shared lock for multiple read threads, but is exclusive for
 * write thread(means read thread and write thread can not get ReadLock at the
 * same time).
 */
public class ExtSharedLock_ReentrantWriteReadLockStudy {
    private static Random randomer = new Random(System.currentTimeMillis());

    public static void main(String... strings) throws InterruptedException {
        ReentrantReadWriteLock RWLock = new ReentrantReadWriteLock();
        StringBuilder blackBoard = new StringBuilder(100);
        ExecutorService threadPool = Executors.newCachedThreadPool();

        threadPool.submit(new Teacher(RWLock, "ChiTeacher", blackBoard));
        threadPool.submit(new Teacher(RWLock, "EngTeacher", blackBoard));
        threadPool.submit(new Student(RWLock, "FEN", blackBoard));
        threadPool.submit(new Student(RWLock, "NAN", blackBoard));
        threadPool.submit(new Student(RWLock, "QAN", blackBoard));

        threadPool.shutdown();
        threadPool.awaitTermination(1L, TimeUnit.SECONDS);
    }

    private static class Teacher implements Runnable {
        final private ReentrantReadWriteLock RWLock;
        final private String tName;
        final private StringBuilder blackBoard;
        private AtomicInteger id = new AtomicInteger(0);

        Teacher(ReentrantReadWriteLock RWLock, String tName, StringBuilder blackBoard) {
            this.RWLock = RWLock;
            this.tName = tName;
            this.blackBoard = blackBoard;
        }

        @Override
        public void run() {
            for (int i = 0; i < 20; i++) {
                try {
                    RWLock.writeLock().lock();
                    System.out.println(tName + " has acquired wirte lock.");
                    blackBoard.delete(0, 99).append(id.getAndIncrement() + " of 100 JUC exercises");
                    System.out.println(tName + " has written on blackBoard : " + blackBoard);
                    TimeUnit.MILLISECONDS.sleep(randomer.nextInt(500));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    RWLock.writeLock().unlock();
                    System.out.println(tName + " has released wirte lock.");
                }
            }
        }
    }

    private static class Student implements Runnable {
        final private ReentrantReadWriteLock RWLock;
        final private String tName;
        final private StringBuilder blackBoard;

        Student(ReentrantReadWriteLock RWLock, String tName, StringBuilder blackBoard) {
            this.RWLock = RWLock;
            this.tName = tName;
            this.blackBoard = blackBoard;
        }

        @Override
        public void run() {
            for (int i = 0; i < 20; i++) {
                try {
                    RWLock.readLock().lock();
                    System.out.println(tName + " has acquired read lock.");
                    System.out.println(tName + " has read from blackBoard : " + blackBoard);
                    TimeUnit.MILLISECONDS.sleep(randomer.nextInt(500));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    RWLock.readLock().unlock();
                    System.out.println(tName + " has released read lock.");
                }
            }
        }
    }
}
