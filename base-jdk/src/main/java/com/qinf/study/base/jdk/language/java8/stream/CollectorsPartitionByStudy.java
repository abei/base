package com.qinf.study.base.jdk.language.java8.stream;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by qinf on 2017/11/28 PM10:44:35
 * <p>
 * Partition by is a special case of Group by.
 * The key of a result map from Partition by is just "true" or "false".
 * {
 * true : [v1, v2, v3],
 * false : [v4, v5],
 * }
 */
public class CollectorsPartitionByStudy {

    public static void main(String... strings) {
        Stream<Employee> employees = Stream.of(
                new Employee("ana", "girl", 15),
                new Employee("fen", "man", 20),
                new Employee("lil", "girl", 25),
                new Employee("tom", "man", 30),
                new Employee("daf", "man", 40)
        );

        // 1. Partition by.
        Map<Boolean, List<Employee>> employeesPartitionByGender = employees
                .collect(Collectors.partitioningBy(e -> e.getGender().equals("man")));
        for (Entry<?, ?> m : employeesPartitionByGender.entrySet())
            System.out.println(m.getKey() + " : " + m.getValue().toString());
    }

    static class Employee {
        private String name;
        private String gender;
        private int age;

        public Employee(String name, String gender, int age) {
            this.name = name;
            this.gender = gender;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public String getGender() {
            return gender;
        }

        public int getAge() {
            return age;
        }
    }
}
