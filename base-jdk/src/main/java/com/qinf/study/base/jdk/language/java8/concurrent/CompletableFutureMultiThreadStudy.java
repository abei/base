package com.qinf.study.base.jdk.language.java8.concurrent;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

/**
 * Created by qinf on 2020-08-07.
 */
public class CompletableFutureMultiThreadStudy {

    static void print(String flag) {
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName() + " handle: " + flag + "-" + i);
            try {
                TimeUnit.MILLISECONDS.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String... strings) {
        /**
         * The multiple thread ability of CompletableFuture and async is both used,
         * because you supplied multiple tasks to the thread pool almost at the same time.
         */
        Stream.of("good", "bad", "middle")
                .forEach(flag -> CompletableFuture.runAsync(() -> print(flag), Executors.newFixedThreadPool(2))
                        .handle((res, ex) -> res));

        /**
         * Because just one task is submitted, so just one thread of PorkJoinCommonPool handle
         * the task in async mode. In fact, the multiple thread ability is not used radically.
         */
        /*CompletableFuture.runAsync(() -> System.out.println(Thread.currentThread().getName() + " handle process."))
                .handle((res, ex) -> {
                    System.out.println(Thread.currentThread().getName() + " get last result.");
                    return res;
                });*/
        CompletableFuture.runAsync(() -> System.out.println(Thread.currentThread().getName() + " handle process."))
                .thenRun(() -> {
                    System.out.println(Thread.currentThread().getName() + " get last result.");
                });

    }
}
