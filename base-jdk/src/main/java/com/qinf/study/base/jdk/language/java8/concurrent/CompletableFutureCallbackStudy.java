package com.qinf.study.base.jdk.language.java8.concurrent;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.stream.Stream;

/**
 * Created by qinf on 2017/12/10 PM5:09:17
 * <p>
 * To trigger callback by response completion event of CompletableFuture.
 * https://stackoverflow.com/questions/34877380/callback-with-completablefuture
 * http://colobu.com/2016/02/29/Java-CompletableFuture/
 * <p>
 * In Java7, here is a callback example in kafka API :
 * producer.send(myRecord,
 * new Callback() {
 * public void onCompletion(RecordMetadata metadata, Exception e) {
 * if(e != null) {
 * e.printStackTrace();
 * } else {
 * System.out.println("The offset of the record we just sent is: " + metadata.offset());
 * }
 * }
 * });
 * <p>
 * In CompletableFuture, you can image callback is CompletableFuture.thenAccept() [or CompletableFuture.thenApply()??]
 */
public class CompletableFutureCallbackStudy {
    private static final Random randomDelayer = new Random();

    private static void randomDelay() {
        long delay = 500 + randomDelayer.nextInt(2_000);
        try {
            TimeUnit.MILLISECONDS.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Flow : for instance, there are 4 shops [TMall, Amazon, JD, WalMar]
     * Not block invoker, because not wait to get the last result, we just return a mediate stream.
     */
    public static Stream<CompletableFuture<String>> findDiscountedPricesInStream(List<Shop> allShops, String productName, Executor powerExecutor1) {
        final Executor powerExecutor2 = Executors.newFixedThreadPool(20, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            }
        });
        Stream<CompletableFuture<String>> discountedPricesInStream = allShops.stream()
                /** The first task. */
                .map(shop -> CompletableFuture.supplyAsync(() -> shop.getProductPriceInfo(productName), powerExecutor1))
                .map(complFuture_priceInfoInString -> complFuture_priceInfoInString.thenApply(DTO::parsePriceInfo))
                .map(complFuture_priceInfoInDTO -> complFuture_priceInfoInDTO.thenCompose(
                        /** The second task depends on result of first task. */
                        dto -> CompletableFuture.supplyAsync(() -> DiscountService.applyDiscount(dto), powerExecutor2)));
        //return cmplFutureConvertedPrices.stream().map(CompletableFuture::join).collect(Collectors.toList());
        return discountedPricesInStream;
    }

    public static void main(String... strings) {
        List<Shop> allShops = Arrays
                .asList(new Shop[]{new Shop("TMall"), new Shop("Amazon"), new Shop("JD"), new Shop("WalMar")});
        final Executor executor = Executors.newFixedThreadPool(Math.min(allShops.size(), 50), new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            }
        });

        long StartTime = System.currentTimeMillis();
        CompletableFuture[] futures = findDiscountedPricesInStream(allShops, "IPhone1000", executor)
                .map(cmplFuture -> cmplFuture.thenAccept(priceInfo -> System.out.println(
                        priceInfo + " [done in " + ((System.currentTimeMillis() - StartTime) / 1_000) + " seconds]")))
                .toArray(size -> new CompletableFuture[size]);
        /** Wait all the CompletableFuture in array futures completed. */
        CompletableFuture.allOf(futures).join();
        /**
         * Wait any one of the CompletableFuture in array futures completed.
         * That is to say, once one CompletableFuture completed, the whole task
         * is completed.
         */
        //CompletableFuture.anyOf(futures).join();

        /** Run result[differ from each time you run] :
         * Amazon price is : -59512276086395935 [done in 2 seconds]
         * JD price is : 29958999704316957 [done in 2 seconds]
         * TMall price is : 92233608502504278 [done in 3 seconds]
         * WalMar price is : -27241261960898055 [done in 3 seconds]
         */
    }

    static class DTO {
        private final String shopName;
        private final long productPrice;

        public DTO(String shopName, long productPrice) {
            this.shopName = shopName;
            this.productPrice = productPrice;
        }

        public static DTO parsePriceInfo(String priceInfo) {
            String[] splits = (String[]) Stream.of(priceInfo.split(":"))
                    .map(String::trim)
                    .toArray(String[]::new);
            return new DTO(splits[0],
                    Long.parseLong(splits[1]));
        }

        public String getShopName() {
            return shopName;
        }

        public long getProductPrice() {
            return productPrice;
        }
    }

    static class Shop {
        private String shopName;

        Shop(String shopName) {
            this.shopName = shopName;
        }

        public String getShopName() {
            return this.shopName;
        }

        /**
         * consume time is random
         */
        public String getProductPriceInfo(String productName) {
            randomDelay();
            long productPrice = new Random().nextLong() * productName.charAt(0) + productName.charAt(1);
            return shopName + ":" + productPrice;
        }
    }

    static class DiscountService {
        /**
         * consume time is random
         */
        public static String applyDiscount(DTO dto) {
            randomDelay();
            return dto.getShopName() + " price is : " + String.valueOf(dto.getProductPrice() * 50 / 100);
        }
    }
}
