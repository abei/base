package com.qinf.study.base.jdk.cpu.threadlocal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by qinf on 2017/10/31 PM9:50:33
 */
public class ThreadLocalStudy {

    public static void main(String... args) throws InterruptedException {
        Map<String, List<String>> context = (Map<String, List<String>>) MyContext.getContext();
        context.get("context.qin.key").add("transactionId01");
        context.get("context.qin.key").add("transactionId02");

        Thread.sleep(1000);

        for (String txnid : ((Map<String, List<String>>) MyContext.getContext()).get("context.qin.key"))
            System.out.println(txnid);

        Map<String, List<String>> context2 = (Map<String, List<String>>) MyContext.getContext();

        List<Integer> secondList = new ArrayList<Integer>();
        secondList.add(new Integer(1));
        secondList.add(new Integer(2));
        Map secondMap = new HashMap<String, List<?>>();
        secondMap.put("context.nan.key", secondList);
        MyContext.setContext(secondMap);

        Map<String, List<Object>> context3 = (Map<String, List<Object>>) MyContext.getContext();
    }
}
