package com.qinf.study.base.jdk.language.reflectproxy.single;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by qinf on 2020-10-05.
 */
public class SingleProxyFactory implements InvocationHandler {

    private Object sourceObj;

    private Method[] methods;

    public Object getProxyObj(Object sourceObj) {
        this.sourceObj = sourceObj;
        /*for(Method method: sourceObj.getClass().getDeclaredMethods()) {
            this.methods.put(method.getName(), method);
        }*/
        methods = sourceObj.getClass().getDeclaredMethods();
        Object proxyObj = Proxy.newProxyInstance(
                sourceObj.getClass().getClassLoader(),
                sourceObj.getClass().getInterfaces(),
                this); // this is mean the instance of InvocationHandler
        System.out.println("proxy class has generated : " + proxyObj.getClass().getName());
        return proxyObj;
    }

    /**
     * The Object proxy : proxy class : is the generated jdk dynamic proxy class instance(a instance of $Proxy0.class).
     * The Method method : invoked method : the method declared in interface Subject.class.
     * The Object[] args : method args : the arguments passed in for method of implementation class.
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("Before perform business logic.");
        Object result = null;
        //Object result = method.invoke(sourceObj, args);
        for (Method m : methods) {
            result = method.invoke(sourceObj, args);
            System.out.println("Reuslt: " + result);
        }
        //Object result = methods.get(method.getName()).invoke(sourceObj, args);
        System.out.println("After perform business logic.");
        return result;
    }
}
