package com.qinf.study.base.jdk.designpattern.processorchain_engine;

/**
 * Created by qinf on 2021-10-02.
 */
@FunctionalInterface
public interface Callback<T, U, P, R> {

    /**
     * Applies this function to the given arguments.
     *
     * @param t the first function argument
     * @param u the second function argument
     * @param p the third function argument
     * @return the function result
     */
    R callback(T t, U u, P p);
}
