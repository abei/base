package com.qinf.study.base.jdk.language.java8.stream;

/**
 * Created by qinf on 2017/11/28 PM12:25:31
 * <p>
 * Collector Interface used to add each element into a mutable container.
 * It includes 4 abstract methods :
 * 1. supplier() : create a new result container.
 * 2. accumulator() : add a element into the new result container.
 * 3. combiner() : combine two new result containers.
 * 4. finisher() : complete final modification for result container.
 * <p>
 * In fact, to simplify collecting reduction operation, there is a overloaded
 * Stream.collect() method can receive of Collector instance as argument.
 * <R, A> R collect(Collector<? super T, A, R> collector);
 * <p>
 * To further simplify collecting reduction operation, JDK supplied a static
 * factory class - Collectors, this class supplies many methods to create different
 * functional collector(collector is instance of Collector Interface).
 * <pre>
 * {@code public static Collector<CharSequence, ?, String> joining(CharSequence delimiter) {
 * 	return joining(delimiter, "", "");
 * }</pre>
 */
public class CollectorsConceptInStreamStudy {

}
