package com.qinf.study.base.jdk.cpu.juc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by qinf on 2017/5/31 PM9:33:18
 */
public class ConditionResourcePoolStudy {

    public static void main(String[] args) throws InterruptedException {
        ConditionResourcePoolStudy.ResourcePool pool = new ConditionResourcePoolStudy().new ResourcePool();

        for (int i = 0; i < 10; i++) {
            Thread putThread = new Thread(new PutThread(i, pool), "[put thread - " + i + "]");
            Thread takeThread = new Thread(new TakeThread(pool), "[take thread - " + i + "]");
            putThread.start();
            takeThread.start();
        }
    }

    static class PutThread implements Runnable {
        ResourcePool pool;
        int resource;

        public PutThread(int resource, ResourcePool pool) {
            this.resource = resource;
            this.pool = pool;
        }

        public void run() {
            try {
                Thread.sleep(500);
                pool.put(resource);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static class TakeThread implements Runnable {
        ResourcePool pool;

        public TakeThread(ResourcePool pool) {
            this.pool = pool;
        }

        @SuppressWarnings("unused")
        public void run() {
            try {
                Thread.sleep(500);
                Integer rscNum = (Integer) pool.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private class ResourcePool {
        private final Lock lock = new ReentrantLock();
        private final Condition full = lock.newCondition();
        private final Condition empty = lock.newCondition();
        private final Object[] resourcePool = new Object[5];
        int putCnt, takeCnt, size;

        public void put(Object o) throws InterruptedException {
            lock.lock();
            try {
                while (size == resourcePool.length)
                    full.await();

                resourcePool[putCnt++] = o;
                if (putCnt == resourcePool.length)
                    putCnt = 0;
                size++;

                empty.signal();

                System.out.println(Thread.currentThread().getName() +
                        " - put resource : " + (Integer) o);
            } finally {
                lock.unlock();
            }
        }

        public Object take() throws InterruptedException {
            lock.lock();
            try {
                while (size == 0)
                    empty.await();

                Object o = resourcePool[takeCnt++];
                if (takeCnt == resourcePool.length)
                    takeCnt = 0;
                size--;

                full.signal();

                System.out.println(Thread.currentThread().getName() +
                        " - take resource : " + (Integer) o);
                return o;
            } finally {
                lock.unlock();
            }
        }
    }
}
