package com.qinf.study.base.jdk.network.sock;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by qinf on 2021-09-14.
 */
public class SockInJdbcConnPoolStudy {

    private static final Set<Connection> connPool = new HashSet<>(2);

    public static void main(String... strings) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        /*try (Connection conn = DriverManager.getConnection("jdbc:mysql://xxx.0.xxx.xx:3306/db", "username", "password#");
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT version()");
             ) {
            rs.next();
            System.out.println(rs.getString("version()"));
        }*/

        Statement stmt = null;
        ResultSet rs = null;
        try {
            Connection conn1 = DriverManager.getConnection("jdbc:mysql://xxx.0.xxx.xx:3306/db", "username", "password");
            Connection conn2 = DriverManager.getConnection("jdbc:mysql://xxx.0.xxx.xx:3306/db", "username", "password");
            //TCP: 64181 → 3306 [RST, ACK] Seq=2079 Ack=3230 Win=0 Len=0
            connPool.add(conn1);
            //TCP: 64183 → 3306 [RST, ACK] Seq=2164 Ack=3347 Win=0 Len=0
            connPool.add(conn2);
            stmt = connPool.stream().findAny().get().createStatement();
            rs = stmt.executeQuery("SELECT version()");
            rs.next();
            System.out.println(rs.getString("version()"));
        } finally {
            stmt.close();
            rs.close();
        }
    }
}
