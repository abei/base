package com.qinf.study.base.jdk.language.reflectproxy.multiple;

/**
 * Created by qinf on 2020-10-05.
 */
public interface MultipleProcessor extends Comparable {

    void process(Integer orderId);
}
