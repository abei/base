package com.qinf.study.base.jdk.language.enum_;

public class EnumInnerClassStudy {
    public static void main(String... strings) {
        System.out.println(ResponseType.SUCCESS);
        System.out.println(ResponseType.SUCCESS.getCode());
        System.out.println(ResponseType.SUCCESS.getMessage());

        System.out.println(ResponseType.TICKET_COMPLETED);
        System.out.println(ResponseType.TICKET_COMPLETED.getCode());
        System.out.println(ResponseType.TICKET_COMPLETED.getMessage());
    }

    public enum ResponseType {
        SUCCESS("1000000", null),
        TICKET_COMPLETED("2000207", "response.message.ticket.completed");
        private String code;
        private String message;

        ResponseType(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }
    }
}
