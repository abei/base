package com.qinf.study.base.jdk.designpattern.processorchain_engine;

/**
 * Created by qinf on 2021-10-02.
 */
public interface Processor<Q, P, C> {

    void process(Q request, P response, C chain);

    Callback getCallback();
}
