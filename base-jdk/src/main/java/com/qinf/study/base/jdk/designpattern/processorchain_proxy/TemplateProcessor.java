package com.qinf.study.base.jdk.designpattern.processorchain_proxy;

import java.lang.reflect.Proxy;
import java.util.function.BiFunction;

/**
 * Created by qinf on 2020-10-06.
 */
public class TemplateProcessor implements Processor {

    private Integer priority;

    @Override
    public void process(Integer orderId) {
        System.out.println("Order: " + orderId + " is processing.");
    }

    @Override
    public void process(Integer orderId, BiFunction callback) {

    }

    @Override
    public int compareTo(Object obj) {
        if (obj instanceof Proxy) {
            TemplateProcessor sourceObj = (TemplateProcessor) ProxyFactory.getSourceObj(obj);
            return this.priority - sourceObj.getPriority();
        }
        TemplateProcessor thisObj = (TemplateProcessor) obj;
        return this.priority - thisObj.getPriority();
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
