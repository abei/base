package com.qinf.study.base.jdk.designpattern.observermechanism_callback;

import java.util.concurrent.TimeUnit;

/**
 * Created by qinf on 2017/12/28 PM15:31:58
 */
class NetworkSender {
    //private volatile boolean isSendSucc = false;
    //private AtomicReference operResult = new AtomicReference(new String());
    //private volatile String operResult;

    String doSend(String msg, Callback callback) {
        boolean isSendSucc = false;
        // Simulate send message to remote server.
        try {
            TimeUnit.SECONDS.sleep(2);
            isSendSucc = true;
            System.out.println(String.format("Have sent mesasge : {%s}.", msg));
            callback.onCompletion("Successfull", null);
        } catch (InterruptedException e) {
            callback.onCompletion(null, new Exception("Failed", e));
        }

        // If send successfully will return 1, or return 0.
        if (isSendSucc)
            return "1";
        else
            return "0";
    }
}
