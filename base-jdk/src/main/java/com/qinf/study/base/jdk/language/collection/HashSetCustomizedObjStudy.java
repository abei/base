package com.qinf.study.base.jdk.language.collection;

import lombok.EqualsAndHashCode;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by qinf on 2020-06-23.
 */
public class HashSetCustomizedObjStudy {

    /**
     * All elements inserted into the hash set must be override hashCode and equals method.
     * Because the elements, actually, are the keys of HashMap.
     */
    static class Order {
        private Long orderId;
        private String orderType;

        @Override
        public boolean equals(Object o) {
            if (o == this) {
                return true;
            } else if (!(o instanceof HashSetCustomizedObjStudy.Order)) {
                return false;
            } else {
                HashSetCustomizedObjStudy.Order other = (HashSetCustomizedObjStudy.Order)o;
                if (!other.canEqual(this)) {
                    return false;
                } else {
                    Object this$orderId = this.getOrderId();
                    Object other$orderId = other.getOrderId();
                    if (this$orderId == null) {
                        if (other$orderId != null) {
                            return false;
                        }
                    } else if (!this$orderId.equals(other$orderId)) {
                        return false;
                    }

                    Object this$orderType = this.getOrderType();
                    Object other$orderType = other.getOrderType();
                    if (this$orderType == null) {
                        if (other$orderType != null) {
                            return false;
                        }
                    } else if (!this$orderType.equals(other$orderType)) {
                        return false;
                    }

                    return true;
                }
            }
        }

        protected boolean canEqual(Object other) {
            return other instanceof HashSetCustomizedObjStudy.Order;
        }

        @Override
        public int hashCode() {
            int result = 1;
            Object $orderId = this.getOrderId();
            result = result * 59 + ($orderId == null ? 43 : $orderId.hashCode());
            Object $orderType = this.getOrderType();
            result = result * 59 + ($orderType == null ? 43 : $orderType.hashCode());
            return result;
        }

        public Long getOrderId() {
            return orderId;
        }

        public void setOrderId(Long orderId) {
            this.orderId = orderId;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        @Override
        public String toString() {
            return "Order{" +
                    "orderId=" + orderId +
                    ", orderType='" + orderType + '\'' +
                    '}';
        }
    }

    public static void main(String... strings) {
        Set<Order> orders = new HashSet();

        Order order1 = new Order();
        order1.setOrderId(1L);
        order1.setOrderType("electron");

        Order order2 = new Order();
        order2.setOrderId(2L);
        order2.setOrderType("food");

        Order order3 = new Order();
        order3.setOrderId(3L);
        order3.setOrderType("book");

        Order order4 = new Order();
        order4.setOrderId(3L);
        order4.setOrderType("book");

        orders.add(order3);
        orders.add(order1);
        orders.add(order2);
        orders.add(order4);

        orders.forEach(System.out::println);
    }
}
