package com.qinf.study.base.jdk.language.java8.concurrent;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * Created by qinf on 2017/12/3 PM4:02:40
 */
public class CompletableFutureStudy {

    public static List<String> findPricesFromAllShops(List<Shop> allShops, String productName, Executor powerExecutor) {
        // Use parallel stream.
        //return allShops.parallelStream().
        //		map(s -> s.getShopName() + " : " + s.getProductPrice(productName)).
        //		collect(Collectors.toList());

        // The result is String type, so the generic will be CompletableFuture<String>.
        List<CompletableFuture<String>> cmplFuturePrices = allShops.stream()
                .map(s -> CompletableFuture
                        .supplyAsync(() -> s.getShopName() + " : " + s.getProductPrice(productName),
                                powerExecutor))
                .collect(Collectors.toList());

        return cmplFuturePrices.stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());
    }

    public static void main(String... strings) {
        List<Shop> allShops = Arrays
                .asList(new Shop[]{new Shop("TMall"), new Shop("Amazon"), new Shop("JD"), new Shop("WalMar")});

        /**
         * The executor used for performing long time work [net io, db query, file io].
         */
        final Executor executor = Executors.newFixedThreadPool(Math.min(allShops.size(), 50), new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            }
        });
        List<String> allPrices = findPricesFromAllShops(allShops, "IPhone1000", executor);

        allPrices.stream().forEach(System.out::println);
    }

    static class Shop {
        private String shopName;

        Shop(String shopName) {
            this.shopName = shopName;
        }

        public String getShopName() {
            return this.shopName;
        }

        public long getProductPrice(String productName) {
            try {
                TimeUnit.SECONDS.sleep(1);
                //Thread.sleep(1_000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return new Random().nextLong() * productName.charAt(0) + productName.charAt(1);
        }
    }
}
