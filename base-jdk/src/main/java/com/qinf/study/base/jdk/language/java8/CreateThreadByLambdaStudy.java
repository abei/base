package com.qinf.study.base.jdk.language.java8;

/**
 * Created by qinf on 2017/11/19 PM9:09:23
 */
public class CreateThreadByLambdaStudy {

    public static void main(String... args) {
        new Thread() {
            public void run() {
                System.out.println("Create a thread by anonymous inner class.");
            }
        }.start();

        /**
         * The grammar of lambda expression in Java 8 is :
         * 1. (args) -> expression
         * 2. (args) -> expression
         * 3. (args) -> { expression }
         * NOTE : sometimes, the brackets () may not need.
         */
        new Thread(() -> System.out.println("Create a thread by lambda expression.")).start();
    }
}
