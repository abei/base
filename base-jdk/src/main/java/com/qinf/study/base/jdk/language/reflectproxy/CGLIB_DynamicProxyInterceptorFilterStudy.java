package com.qinf.study.base.jdk.language.reflectproxy;

import net.sf.cglib.proxy.*;

import java.lang.reflect.Method;

/**
 * Created by qinf on 2017/7/13 PM9:52:29
 * <p>
 * By interceptor filer, you can make different interceptors work for different
 * methods of a delegate class. So that, can realize more coarse-grained method
 * control.
 */
public class CGLIB_DynamicProxyInterceptorFilterStudy {

    private final static Enhancer proxyClsInstanceGenerator = new Enhancer();
    private static Object delegateClsInstance;

    public static Object getDynamicProxyClassInstance(Object realObj) {
        delegateClsInstance = realObj;
        proxyClsInstanceGenerator.setSuperclass(delegateClsInstance.getClass());
        /**
         * Here, we created two interceptors :
         * The one is AuthInterceptor, defined by customized.
         * Another is NoOp, it is a existed interceptor supplied by cglib, it means do nothing for agented method.
         * Callback[0] is AuthInterceptor
         * Callback[1] is NoOp
         */
        proxyClsInstanceGenerator.setCallbacks(new Callback[]{new AuthInterceptor(), NoOp.INSTANCE});
        proxyClsInstanceGenerator.setCallbackFilter(new InterceptorFilter());
        // the generated dynamic proxy class instance, the proxy class extended delegate class Delegate01Impl.class
        return proxyClsInstanceGenerator.create();
    }

    public static void main(String[] args) {
        CGLIB_Delegate02Impl proxyClsInstance = (CGLIB_Delegate02Impl) getDynamicProxyClassInstance(new CGLIB_Delegate02Impl());
        String resultOfGeneratingOrder = proxyClsInstance.generateOrder("1000");
        System.out.println("the result of performing business logic is : " + resultOfGeneratingOrder);

        System.out.println("**************************************************");

        String resultOfCancellingOrder = proxyClsInstance.cancelOrder("2000");
        System.out.println("the result of performing business logic is : " + resultOfCancellingOrder);
    }

    static final class AuthInterceptor implements MethodInterceptor {
        /**
         * The Object obj :  is the generated cglib dynamic proxy class instance(a instance of Delegate02Impl$$EnhancerByCGLIB$$2ddc8973.class).
         * The Method method : is the method declared in delegate Delegate02Impl.class.
         * The Object[] args : is the arguments passed in for method of delegate class.
         * The MethodProxy proxy : is a object can invoke CGLIB$cancelOrder$0() declared in proxy class.
         */
        @Override
        public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
            System.out.println("auth completed, you can cancel order : [" + args[0] + "]");
            Object performResult = proxy.invokeSuper(obj, args);
            System.out.println("order has been generated : [" + args[0] + "]");
            return performResult;
        }
    }

    /**  */
    static final class InterceptorFilter implements CallbackFilter {
        /**
         * The number 0 and 1 are the index of array Callback[]{new AuthInterceptor(), NoOp.INSTANCE}
         * "0" points to AuthInterceptor
         * "1" points to NoOp
         */
        @Override
        public int accept(Method method) {
            if ("cancelOrder".equalsIgnoreCase(method.getName()))
                return 0;
            else
                return 1;
        }
    }
}
