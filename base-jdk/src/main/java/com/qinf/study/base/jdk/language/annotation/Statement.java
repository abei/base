package com.qinf.study.base.jdk.language.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by qinf on 2018-1-30.
 * <p>
 * We can think annotation as tag that used to attach on package, class, field, method etc.
 * <p>
 * 1. We defined a new annotation "Statement" :
 * The annotation will be retained to Runtime, and it can just be attached on method.
 * 2. Annotation "Statement" holds 3 attributes :
 * We should assign values to the 3 attributes when use Statement.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Statement {

    // The attribute of annotation.
    String statementName();

    Type returnType() default Type.RESULT_SET;

    String[] returnParams();

    public enum Type {
        RESULT_SET,
        CURSOR
    }
}
