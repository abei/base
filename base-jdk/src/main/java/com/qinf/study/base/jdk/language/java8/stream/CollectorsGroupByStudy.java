package com.qinf.study.base.jdk.language.java8.stream;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by qinf on 2017/11/28 PM10:44:20
 * <p>
 * The result of group by is a map, and its value is a list :
 * {
 * key-i : [v1, v2, v3],
 * key-j : [v4, v5],
 * key-k : [v7]
 * .
 * .
 * }
 */
public class CollectorsGroupByStudy {

    public static void main(String... strings) {
        Stream<Employee> employees = Stream.of(
                new Employee("ana", "girl", 15),
                new Employee("fen", "man", 20),
                new Employee("lil", "girl", 25),
                new Employee("tom", "man", 30),
                new Employee("daf", "man", 40)
        );


        // 1. Group by.
        Map<String, List<Employee>> employeesGroupByGender = employees.collect(Collectors.groupingBy(Employee::getGender));
        for (Entry<?, ?> m : employeesGroupByGender.entrySet())
            System.out.println(m.getKey() + " : " + m.getValue().toString());


        // 2. Multiple level group by.
        /**
         * Multiple level group by, the result will from :
         * {
         * 	"man"  : [(fen,man,20), (tom,man,30)],
         * 	"girl" : [(ana,girl,15), (lil,girl,25)]
         * }
         * to
         * {
         * 	"man"  : [{"MATURE":[(tom,man,30), (daf,man,40)]}, {"YOUTH":[(fen,man,20)]}],
         * 	"girl" : [{"MATURE":[(lil,girl,25)]}, {"YOUTH":[(ana,girl,15)]}]
         * }
         *
         *
         * {
         * 	"man"  : [(fen,man,20), (tom,man,30)],  ====> will be grouped by again.
         * 	"girl" : [(ana,girl,15), (lil,girl,25)] ====> will be grouped by again.
         * }
         *
         */
        Map<String, Map<String, List<Employee>>> employeesGroupByGenderThenByAge = employees.collect(
                Collectors.groupingBy(Employee::getGender,
                        Collectors.groupingBy(e -> {
                            if (e.getAge() > 25)
                                return "MATURE";
                            else
                                return "YOUTH";
                        })
                ));


        // 3. Group by with other Collectors.
        /**
         * Group by cooperate with other Collectors.
         * Other Collectors worked for the subStream from the Group by.
         */
        Map<String, Set<String>> employeesGroupByWithOtherCollector = employees.collect(
                Collectors.groupingBy(
                        Employee::getGender, //param 1 : classifier
                        Collectors.mapping(  //param 2 : other collector
                                e -> {
                                    if (e.getAge() > 25) return "MATURE";
                                    else return "YOUTH";
                                },
                                Collectors.toCollection(HashSet::new))
                ));
    }

    static class Employee {
        private String name;
        private String gender;
        private int age;

        public Employee(String name, String gender, int age) {
            this.name = name;
            this.gender = gender;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public String getGender() {
            return gender;
        }

        public int getAge() {
            return age;
        }
    }
}
