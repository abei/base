package com.qinf.study.base.jdk.designpattern.singleton.hungrysingleton;

/**
 * Created by qinf on 2017/7/27 PM10:13:45
 */
public class HttpClientHungry {

    private static final HttpClientHungry httpClientInstance;

    /**
     * You can instantiate HttpClient in static{}, also can instantiate it
     * directly. By this way, HttpClient will be instantiated when load
     * HttpClient.class. JVM can guarantee thread safety, ensure
     * HttpClientHungry.class just be instantiated once.
     */
    static {
        httpClientInstance = new HttpClientHungry();
    }

    private HttpClientHungry() {
    }

    public static HttpClientHungry getHttpClientInstance() {
        return httpClientInstance;
    }

    public void executeHttpGetRequest() {
        System.out.println("Execute http get request.");
    }
}
