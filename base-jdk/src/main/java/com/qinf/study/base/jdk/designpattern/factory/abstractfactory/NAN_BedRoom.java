package com.qinf.study.base.jdk.designpattern.factory.abstractfactory;

/**
 * Created by qinf on 2017/7/25 PM10:01:52
 */
public class NAN_BedRoom extends Room {

    @Override
    public void live() {
        System.out.println("NAN company has created bed room.");
    }

}
