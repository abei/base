package com.qinf.study.base.jdk.designpattern.factory.simplefactory;

/**
 * Created by qinf on 2017/7/24 PM9:45:35
 */
public class BathRoom extends Room {

    @Override
    public void extralFunction() {
        System.out.println("The room can be used for washing.");
    }

}
