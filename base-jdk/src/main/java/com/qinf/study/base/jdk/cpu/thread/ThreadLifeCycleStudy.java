package com.qinf.study.base.jdk.cpu.thread;

import java.util.concurrent.TimeUnit;

/**
 * Created by qinf on Jul 6, 2018.
 * <p>
 * The state of a thread can be : NEW, RUNNABLE, BLOCKED, WAITING, TIMED_WAITING, TERMINATED.<br>
 * 1. BLOCKED: is about executed synchronized block/method, blocked in JVM Monitor lock<br>
 * 2. WAITING: is about executed Object.wait(), Thread.join(), LockSupport.park()
 * <p>
 * Above states are just representation of thread life in JVM level, not reflect any operating
 * system thread states, e.g., a thread in the runnable state is executing in the Java virtual
 * machine but it may be waiting for other resources from the operating system such as processor.
 */
public class ThreadLifeCycleStudy {

    static class Task implements Runnable {
        static synchronized void print() {
            for (int i = 0; i < 5; i++) {
                System.out.println(Thread.currentThread().getName() + " : " + Thread.currentThread().getState() + " : " + i);
                try {
                    TimeUnit.SECONDS.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void run() {
            print();
        }
    }

    public static void main(String... strings) {
        /**
         * Three threads to obtain JVM Monitor(synchronized block/method) at almost same time,
         * but just one thread can make it, then other threads will into BLOCKED state and enter a
         * Queue of JVM.
         * Once the running thread with Monitor completes its work, it will release the Monitor,
         * then one of the threads in JVM Queue will get a chance to get the Monitor and run its task.
         * The whole process don't need other running thread to notify the threads BLOCKED in Queue,
         * the all is implemented by JVM.
         */
        new Thread(new Task()).start();
        new Thread(new Task()).start();
        new Thread(new Task()).start();
    }
}
