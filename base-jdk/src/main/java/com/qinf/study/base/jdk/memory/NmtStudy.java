package com.qinf.study.base.jdk.memory;

import java.nio.ByteBuffer;

/**
 * Created by qinf on 2020-06-27.<p>
 *
 * NMT means native memory tracking, it is a tool for monitoring
 * the usage of virtual memory for jvm.<p>
 * Usage: java -XX:NativeMemoryTracking=detail -XX:+UnlockDiagnosticVMOptions -XX:+PrintNMTStatistics NMTStudy.class
 */
public class NmtStudy {

    public static void main(String... strings) {
        ByteBuffer.allocateDirect(200 * 1024 * 1024); // underlying is invoke -> unsafe.allocateMemory(size)
        ByteBuffer.allocateDirect(300 * 1024 * 1024);
        //Unsafe.getUnsafe().allocateMemory(100 * 1024 * 1024);
        System.out.println("Native memory has been allocated.");
    }

}
