package com.qinf.study.base.jdk.cpu.juc.ext_queue;

/**
 * Created by qinf on 2017/12/5 PM10:30:10
 * <p>
 * BlockingQueue is a Interface of all blocking queue implementation, there are 3 groups operation :
 * 1. add() | remove() | element() :
 * 1.1. add() throws exception when queue is full.
 * 1.2. remove throws exception when queue is empty.
 * <p>
 * 2. offer() | poll() | peek() :
 * 2.1. offer() just return false when queue is full.
 * 2.2. poll() just return null when queue is empty.
 * <p>
 * 3. put() | take() :
 * 3.1. put() will be blocked when queue is full.
 * 3.2. take() will be blocked when queue is empty.
 * <p>
 * NOTE : In the inner of blocking queue, to implement BLOCKING by using lock, make Thread-Safety.
 */
public class BLOCK_BlockingQueueStudy {

}
