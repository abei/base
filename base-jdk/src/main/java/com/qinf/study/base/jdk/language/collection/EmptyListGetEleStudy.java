package com.qinf.study.base.jdk.language.collection;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by qinf on 2018/08/14
 */
public class EmptyListGetEleStudy {

    private static List<String> getList() {
        return null;
    }

    public static void main(String... args) {
        /**
         * You can add a NULL to a list.
         */
        List list01 = new ArrayList();
        list01.add(null);
        System.out.println(list01.get(0));

        /**
         * You cannot pass NULL to Arrays.asList()
         * Exception will be threw.
         */
        List list02 = Arrays.asList(null);

        /**
         * Both of them will throw java.lang.IndexOutOfBoundsException: Index: 0
         * You cannot get element form empty list by get(index).
         */
        System.out.print(Collections.EMPTY_LIST.get(0));
        System.out.print(Collections.emptyList().get(0));

        /**
         * You can travers empty list safely.
         */
        List<String> list03 = Optional.ofNullable(getList())
                .orElse(Collections.emptyList())
                .stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        for (String ele : list03) {
            System.out.println(ele);
        }
        System.out.println(list03.stream().map(String::toString).collect(Collectors.joining(":")));
        System.out.println(list03.size());
    }
}
