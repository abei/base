package com.qinf.study.base.jdk.jvm.autobox;

/**
 * Created by qinf on 2018/5/29.
 */
public class IntegerStudy {

    public static void main(String... strings) {
        /**
         * Two instances to compare memory address.
         */
        Integer integer01 = 10; // auto box to Integer instance.
        Integer integer02 = new Integer(10);
        System.out.println(integer01 == integer02);

        /**
         * JDK, auto unbox and auto box.
         */
        int integer03 = 10;
        Integer integer04 = new Integer(10);
        System.out.println(integer03 == integer04);

        /**
         * The runtime constant pool cache the value of Integer from -128 to
         * 127.
         */
        Integer integer05 = 127;
        Integer integer06 = 127;
        System.out.println(integer05 == integer06);
        Integer integer07 = 128;
        Integer integer08 = 128;
        System.out.println(integer07 == integer08);
    }
}
