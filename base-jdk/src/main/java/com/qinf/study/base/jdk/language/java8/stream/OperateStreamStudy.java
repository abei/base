package com.qinf.study.base.jdk.language.java8.stream;

import java.util.*;
import java.util.stream.Stream;

/**
 * Created by qinf on 2017/11/26 PM10:05:50
 * <p>
 * 1. Stream Operation Category
 * Stream Operation can be divided into 2 categories :
 * 1.1. Intermediate Operation
 * {map (mapToInt, flatMap etc), filter, distinct, sorted, peek, limit, skip, parallel, sequential, unordered}
 * 1.2. Terminal Operation (include Short-Circuiting Operation)
 * {forEach, forEachOrdered,  toArray, [reduce, collect], min, max, count, anyMatch, allMatch, noneMatch, findFirst, findAny, iterator}
 * {anyMatch, allMatch, noneMatch, findFirst, findAny, limit - these are Short-Circuiting Operation}
 * NOTE : terminal operation - collect & reduce are important for collecting data, should rise focus.
 * <p>
 * 2. Stream Operation Mechanism
 * Stream operation is lazy trigger, the terminal operation will trigger all intermediate operations.
 * For short-circuiting operation, intermediate operation will stop immediately if the condition is satisfied.
 */
public class OperateStreamStudy {

    public static void main(String... strings) {
        Stream<String> stream = Arrays.asList(new String[]{"HELLO", "WORLD", "STREAM"}).stream();

        //List list = Arrays.asList(new String[]{"HELLO", "WORLD", "STREAM"});
        stream.forEach(e -> {
            if (Objects.equals(e, "WORLD")) {
                throw new NullPointerException();
                //return;
            }
            System.out.println(e);
        });
    }
}
