package com.qinf.study.base.jdk.cpu.juc.forkjoin;

/**
 * Created by qinf on 2018/1/7 PM3:32:46
 */
public class NumberOfCPUStudy {

    public static void main(String... args) {
        // the maximum number of processors available to the virtual machine; never smaller than one
        int numOfCups = Runtime.getRuntime().availableProcessors();
        System.out.println("The number of CPU of this JVM is ：" + numOfCups);
    }
}
