package com.qinf.study.base.jdk.algo_ds.string;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by qinf on 2020-11-13.
 *
 * Get a sub string of a specific string that its elements is different from each other.
 */
public class SearchLongestDiffCharsSubString {

    private static void getLongestDiffCharsSubStr(String string) {
        List<String> tmpSubChars = new ArrayList<>();
        Set<String> subStrings = new TreeSet<>();
        for (int i = 0; i < string.length(); i++) {
            String tmpChar = String.valueOf(string.charAt(i));
            if (tmpSubChars.contains(tmpChar)) {
                subStrings.add(tmpSubChars.stream().collect(Collectors.joining()));
                int fromIdx = tmpSubChars.indexOf(tmpChar) + 1;
                tmpSubChars = tmpSubChars.subList(fromIdx, tmpSubChars.size());
                tmpSubChars.add(tmpChar);
                continue;
            }
            tmpSubChars.add(tmpChar);
            if (i == string.length() - 1) {
                subStrings.add(tmpSubChars.stream().collect(Collectors.joining()));
            }
        }
        System.out.println(subStrings);
    }

    private static void getLongestDiffCharsSubStrLength(String string) {
        List<String> tmpSubChars = new ArrayList<>();
        int longestLength = 0;
        for (int i = 0; i < string.length(); i++) {
            String tmpChar = String.valueOf(string.charAt(i));
            if (tmpSubChars.contains(tmpChar)) {
                longestLength = Math.max(longestLength, tmpSubChars.size());
                int fromIdx = tmpSubChars.indexOf(tmpChar) + 1;
                tmpSubChars = tmpSubChars.subList(fromIdx, tmpSubChars.size());
                tmpSubChars.add(tmpChar);
                continue;
            }
            tmpSubChars.add(tmpChar);
            if (i == string.length() - 1) {
                longestLength = Math.max(longestLength, tmpSubChars.size());
            }
        }
        System.out.println(longestLength);
    }

    public static void main(String... strings) {
        getLongestDiffCharsSubStr("abcabcbbpojd");
        getLongestDiffCharsSubStrLength("abcabcbbpojd");
    }

}
