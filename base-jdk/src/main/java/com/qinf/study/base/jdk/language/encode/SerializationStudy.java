package com.qinf.study.base.jdk.language.encode;

import lombok.Data;

import java.io.*;

/**
 * Created by qinf on 2021-08-13.
 */
public class SerializationStudy {

    @Data
    static class Student implements Serializable {
        private static final long serialVersionUID = -8048095082739387288L;
        private String name;
    }

    public static void main(String... string) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("student.bin")));
             ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("student.bin")))) {
            // Serialize object.
            Student studentOut = new Student();
            studentOut.setName("QF");
            oos.writeObject(studentOut);
            System.out.println("The object student has been serialized and stored in file.");
            Student studentIn = (Student) ois.readObject();
            System.out.println("The object student has been deserialized: " + studentIn);

            // Serialize string.
            oos.writeObject("2021");
            System.out.println("The object string has been serialized and stored in file.");
            String strIn = (String) ois.readObject();
            System.out.println("The object string has been deserialized: " + strIn);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
