package com.qinf.study.base.jdk.language.java8.lambda.functiondesign;

/**
 * Created by qinf on 2018/4/1 PM3:32:46
 */
@FunctionalInterface
public interface UniFunction<A, R> {
    public R invoke(A a);
}
