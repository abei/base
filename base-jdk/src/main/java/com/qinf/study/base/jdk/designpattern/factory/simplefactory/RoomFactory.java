package com.qinf.study.base.jdk.designpattern.factory.simplefactory;

/**
 * Created by qinf on 2017/7/24 PM9:46:50
 */
public class RoomFactory {
    public static Room getRoom(String roomType) {
        Room concreteRoom = null;

        //if (room instanceof BathRoom) {
        //	concreteRoom = new BathRoom();
        //} else if (room instanceof BedRoom) {
        //	concreteRoom = new BedRoom();
        //}
        if ("BedRoom".equalsIgnoreCase(roomType)) {
            concreteRoom = new BedRoom();
        } else if ("BathRoom".equalsIgnoreCase(roomType)) {
            concreteRoom = new BathRoom();
        } else {
            concreteRoom = null;
        }
        return concreteRoom;
    }
}
