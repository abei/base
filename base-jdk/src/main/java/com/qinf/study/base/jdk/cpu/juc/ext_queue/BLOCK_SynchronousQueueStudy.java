package com.qinf.study.base.jdk.cpu.juc.ext_queue;

/**
 * Created by qinf on 2017/12/5 PM10:30:42
 * <p>
 * Used for :
 * 1. newCachedThreadPool() ===> ThreadPoolExecutor(0, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, new SynchronousQueue())
 * Why use SynchronousQueue in cached thread pool : there is no limit for the amount of generated threads, so the incoming
 * task not need block in queue, there are sufficient threads to handle them.
 * <p>
 * NOTE 1 : The capacity of SynchronousQueue is 0;
 * NOTE 2 : A thread invokes put() will be blocked when a thread invokes take();
 */
public class BLOCK_SynchronousQueueStudy {

}
