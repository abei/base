package com.qinf.study.base.jdk.designpattern.singleton.lazysingleton;

/**
 * Created by qinf on 2017/7/31 PM9:48:07
 * <p>
 * We use synchronized key word to realize lazy singleton design pattern.
 * But, by this way, we should know it is not a perfect implementation way.
 * Because it will cause some performance problem. It is not necessary to
 * modify entire method by key work synchronized.
 */
public class HttpClientLazySynchronized {

    private static HttpClientLazySynchronized httpClientInstance = null;

    private HttpClientLazySynchronized() {
    }

    public synchronized static HttpClientLazySynchronized getHttpClientSynchronizedInstance() {
        if (null == httpClientInstance)
            httpClientInstance = new HttpClientLazySynchronized();
        return httpClientInstance;
    }
}
