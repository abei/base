package com.qinf.study.base.jdk.designpattern.singleton.lazysingleton;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

/**
 * Created by qinf on 2017/7/31 PM10:28:07
 * <p>
 * It is a wrong completion of singleton, just for studying Unsafe class.
 */
@Deprecated
public class HttpClientLazyUnsafe {
    private static Unsafe unsafe = Unsafe.getUnsafe();
    private static volatile HttpClientLazyUnsafe httpClientInstance = new HttpClientLazyUnsafe();
    ;

    private HttpClientLazyUnsafe() {
    }

    public static HttpClientLazyUnsafe getHttpClientUnsafe() throws NoSuchFieldException, SecurityException {
        Field hci = HttpClientLazyUnsafe.class.getDeclaredField("httpClientInstance");
        // The offset of the httpClientInstance in memory, you can regard it as its position in memory.
        long fieldOffset = unsafe.objectFieldOffset(hci);
        unsafe.compareAndSwapObject(HttpClientLazyUnsafe.class, fieldOffset, httpClientInstance, new HttpClientLazyUnsafe());
        return httpClientInstance;
    }
}
