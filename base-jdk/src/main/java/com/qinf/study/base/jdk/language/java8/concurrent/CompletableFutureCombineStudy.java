package com.qinf.study.base.jdk.language.java8.concurrent;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by qinf on 2017/12/10 PM2:50:58
 */
public class CompletableFutureCombineStudy {
    /**
     * Flow : for instance, there are 4 shops [TMall, Amazon, JD, WalMar]
     * [ threadPool_1 ]                                                             [ threadPool_2 ]
     * thread-0[TMall]  : getPrice & parsePrice, use time 1s -->[combine result]<-- thread-6[TMall]  : getRate, use time 1s
     * thread-1[Amazon] : getPrice & parsePrice, use time 1s -->[combine result]<-- thread-4[Amazon] : getRate, use time 1s
     * thread-2[JD]     : getPrice & parsePrice, use time 1s -->[combine result]<-- thread-5[JD]     : getRate, use time 1s
     * thread-3[WalMar] : getPrice & parsePrice, use time 1s -->[combine result]<-- thread-7[WalMar] : getRate, use time 1s
     * <p>
     * Total use time is 1s.
     * <p>
     * NOTE 1 : The second CompletableFuture does not depend on the first CompletableFuture,
     * they run at the same time, so the total use time is 1s.
     * NOTE 2 : thread-0[TMall]  & thread-6[TMall]  : run at the same time
     * thread-1[Amazon] & thread-4[Amazon] : run at the same time
     * thread-2[JD]     & thread-5[JD]     : run at the same time
     * thread-3[WalMar] & thread-7[WalMar] : run at the same time
     */
    public static List<Double> findConvertExchangeRatePricesFromAllShops(List<Shop> allShops, String productName, Executor powerExecutor1) {
        final Executor powerExecutor2 = Executors.newFixedThreadPool(20, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            }
        });
        List<CompletableFuture<Double>> cmplFutureConvertedPrices = allShops.stream()
                /** The first independent task. */
                .map(shop -> CompletableFuture.supplyAsync(() -> shop.getProductPriceInfo(productName), powerExecutor1))
                .map(complFuture_priceInfoInString -> complFuture_priceInfoInString.thenApply(DTO::parsePriceInfo))
                .map(complFuture_priceInfoInDTO -> complFuture_priceInfoInDTO.thenCombine(
                        /** The second independent task. */
                        CompletableFuture.supplyAsync(() -> ExchangeRateService.getExchangeRate("USD", "RMB"), powerExecutor2),
                        /** NOTE : combine result happens in the same(second) CompletableFuture */
                        (priceInfo, exchangeRate) -> priceInfo.getProductPrice() * exchangeRate))
                .collect(Collectors.toList());
        /**
         * 1. CompletableFuture.join() will block thread until obtain the result, as like Future.get().
         * 2. The code will block invoker, because it will be blocked until get all results from CompletableFuture.
         * 	  That is to say invoker will be blocked until 4 CompletableFuture completed.
         */
        return cmplFutureConvertedPrices.stream().map(CompletableFuture::join).collect(Collectors.toList());
    }

    public static void main(String... strings) {
        List<Shop> allShops = Arrays
                .asList(new Shop[]{new Shop("TMall"), new Shop("Amazon"), new Shop("JD"), new Shop("WalMar")});
        final Executor executor = Executors.newFixedThreadPool(Math.min(allShops.size(), 50), new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            }
        });

        long StartTime = System.currentTimeMillis();
        List<Double> allPrices = findConvertExchangeRatePricesFromAllShops(allShops, "IPhone1000", executor);
        long EndTime = System.currentTimeMillis();

        System.out.println("Use time : " + (EndTime - StartTime) / 1_000 + " seconds");
        allPrices.stream().forEach(System.out::println);
    }

    static class DTO {
        private final String shopName;
        private final long productPrice;

        public DTO(String shopName, long productPrice) {
            this.shopName = shopName;
            this.productPrice = productPrice;
        }

        public static DTO parsePriceInfo(String priceInfo) {
            String[] splits = (String[]) Stream.of(priceInfo.split(":"))
                    .map(String::trim)
                    .toArray(String[]::new);
            return new DTO(splits[0],
                    Long.parseLong(splits[1]));
        }

        public String getShopName() {
            return shopName;
        }

        public long getProductPrice() {
            return productPrice;
        }
    }

    static class Shop {
        private String shopName;

        Shop(String shopName) {
            this.shopName = shopName;
        }

        public String getShopName() {
            return this.shopName;
        }

        /**
         * consume time 1s
         */
        public String getProductPriceInfo(String productName) {
            try {
                Thread.sleep(1_000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            long productPrice = new Random().nextLong() * productName.charAt(0) + productName.charAt(1);
            return shopName + ":" + productPrice;
        }
    }

    static class ExchangeRateService {
        /**
         * consume time 1s
         */
        public static double getExchangeRate(String fromCurrency, String toCurrency) {
            try {
                Thread.sleep(1_000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 0.5D;
        }
    }
}
