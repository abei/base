package com.qinf.study.base.jdk.language.avro;

import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;

import java.io.File;
import java.io.IOException;

/**
 * Created by qinf on 2018-6-3.
 */
public class DataStorageByAvroWithoutCodeGenerationStudy {

    /**
     * Use avro without code generation, here, the prerequisite is the avro schema file is existent.
     * 1. First, you should generate related Java object by corresponding avro schema file(*.avsc).
     * 2. Then, you can do Serialization and Deserialization by the generated Java object.
     *
     * @throws IOException
     */
    public static void main(String... strings) throws IOException {
        // 1. Obtain schema file.
        File schemaFile = new File("G:/study-code bak/study-myown/study/study-jdk/src/main/resources/config-avro/user.avsc");
        // 2. Obtain schema.
        Schema schema = new Schema.Parser().parse(schemaFile);

        // 3. Generate User object by specific avro schema file in runtime.(We don't generate Java object in advance.)
        GenericRecord user1 = new GenericData.Record(schema);
        user1.put("name", "fen-01");
        user1.put("age", 100);
        user1.put("email", "email-01");
        GenericRecord user2 = new GenericData.Record(schema);
        user2.put("name", "fen-02");
        user2.put("age", 200);
        user2.put("email", "email-02");

        // 4. Serialize user1 and user2 to disk, this time, schema is required.
        File file = new File(
                "G:/study-code bak/study-myown/study/study-jdk/src/main/java/com/qinf/study/avro/data-storage-users-no-code-generation.avro");
        DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<GenericRecord>(schema);
        DataFileWriter<GenericRecord> dataFileWriter = new DataFileWriter<GenericRecord>(datumWriter);
        dataFileWriter.create(schema, file);
        dataFileWriter.append(user1);
        dataFileWriter.append(user2);
        dataFileWriter.close();

        // 5. Deserialize users from disk.
        DatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(schema);
        DataFileReader<GenericRecord> dataFileReader = new DataFileReader<GenericRecord>(file, datumReader);
        GenericRecord user = null;
        while (dataFileReader.hasNext()) {
            user = dataFileReader.next(user);
            System.out.println(user);
        }
        dataFileReader.close();
    }

}
