package com.qinf.study.base.jdk.cpu.juc.threadpool;

import java.util.concurrent.*;

/**
 * Created by qinf on 2017/6/13 PM9:42:55
 * <p>
 * Executor(Interface) -> ExecutorService(Interface) -> ScheduledExecutorServcie(Interface) -> ScheduledThreadPoolExecutor(Implementation)
 * Executors(Factory class to supply all kinds of Implementation)
 */
public class ThreadPoolCreateStudy {

    @SuppressWarnings("unused")
    public static void main(String[] args) {
        /**
         * 1. Create non-scheduled thread pool by constructor.
         * There are 4 ways to create non-scheduled thread pool by passing different arguments.
         * way 1 : ThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue)
         * way 2 : ThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, RejectedExecutionHandler handler)
         * way 3 : ThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory)
         * way 4 : ThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler)
         */
        ExecutorService nonScheduledThreadPool_01 = new ThreadPoolExecutor(5, 10, 1_000L, TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(5));

        /**
         * 2. Create non-scheduled thread pool by static factory method - Executors.
         * For the following 3 ways creating thread pool, correspondingly, there are
         * also 3 ways to create the same thread pool by customized thread factory.
         * way 1 : static ExecutorService newCachedThreadPool(ThreadFactory threadFactory)
         * way 2 : static ExecutorService newFixedThreadPool(int nThreads, ThreadFactory threadFactory)
         * way 3 : static ExecutorService newSingleThreadExecutor(ThreadFactory threadFactory)
         */
        ExecutorService nonScheduledThreadPool_02 = Executors.newCachedThreadPool();
        ExecutorService nonScheduledThreadPool_03 = Executors.newFixedThreadPool(10);
        ExecutorService nonScheduledThreadPool_04 = Executors.newSingleThreadExecutor();

        /** Supplied in Java 8. */
        ExecutorService workStealThreadPool = Executors.newWorkStealingPool();

        /**
         * 3. Create scheduled thread pool by constructor.
         * There are 4 ways to create scheduled thread pool by passing different arguments.
         * way 1 : ScheduledThreadPoolExecutor(int corePoolSize)
         * way 2 : ScheduledThreadPoolExecutor(int corePoolSize, RejectedExecutionHandler handler)
         * way 3 : ScheduledThreadPoolExecutor(int corePoolSize, ThreadFactory threadFactory)
         * way 4 : ScheduledThreadPoolExecutor(int corePoolSize, ThreadFactory threadFactory, RejectedExecutionHandler handler)
         *
         * NOTE : so-called scheduled thread pool, is that you can make the submitted task delayed execution,
         * or periodic execution.
         */
        ScheduledExecutorService scheduledThreadPool_01 = new ScheduledThreadPoolExecutor(10);

        /**
         * 4. Create scheduled thread pool by static factory method - Executors.
         * For the following 2 ways creating thread pool, correspondingly, there are
         * also 2 ways to create the same thread pool by customized thread factory.
         * way 1 : static ScheduledExecutorService newScheduledThreadPool(int corePoolSize, ThreadFactory threadFactory)
         * way 2 : static ScheduledExecutorService newSingleThreadScheduledExecutor(ThreadFactory threadFactory)
         */
        ScheduledExecutorService scheduledThreadPool_02 = Executors.newScheduledThreadPool(10);
        ScheduledExecutorService scheduledThreadPool_03 = Executors.newSingleThreadScheduledExecutor();
    }
}
