package com.qinf.study.base.jdk.language.nestedclass;

/**
 * Created by qinf on 2021-06-26.
 */
public class AbstractStaticInnerClassStudy {

    /**
     * The converter operation is encapsulated in the Bank.
     */
    public static abstract class BankCurrencyConverter {

        public abstract String convertTo(String innerCurrency);

        public abstract String convertFrom(String outerCurrency);
    }

    private class BankCurrencyConverterImpl extends BankCurrencyConverter {

        @Override
        public String convertTo(String innerCurrency) {
            return null;
        }

        @Override
        public String convertFrom(String outerCurrency) {
            return null;
        }
    }
}
