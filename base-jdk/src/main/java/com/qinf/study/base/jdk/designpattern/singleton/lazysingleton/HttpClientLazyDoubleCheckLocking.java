package com.qinf.study.base.jdk.designpattern.singleton.lazysingleton;

/**
 * Created by qinf on 2017/7/31 PM10:00:15
 * <p>
 * So called double check locking is to check the instance is null twice.
 */
public class HttpClientLazyDoubleCheckLocking {
    private static volatile HttpClientLazyDoubleCheckLocking httpClientInstance = null;

    private HttpClientLazyDoubleCheckLocking() {
    }

    public static HttpClientLazyDoubleCheckLocking getHttpClientDoubleCheckLockingInstance() {
        // First to check the instance is null or not.
        if (null == httpClientInstance) {
            synchronized (HttpClientLazyDoubleCheckLocking.class) {
                // Second to check the instance is null or not.
                if (null == httpClientInstance)
                    httpClientInstance = new HttpClientLazyDoubleCheckLocking();
            }
        }
        return httpClientInstance;
    }
}
