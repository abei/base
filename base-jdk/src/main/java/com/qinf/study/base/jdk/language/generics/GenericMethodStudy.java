package com.qinf.study.base.jdk.language.generics;

public class GenericMethodStudy {

    /**
     * Declare generic method, is to implement constraint between multiple arguments.
     * <T, U> used to describe the type of arguments of method.
     * <p>
     * public static <T> Response<T> ok(int version, T data) {
     * return ok(version, System.currentTimeMillis(), data);
     * }
     */
    //static <T, U, E> T get(T t, U u) {
    //static <T, U, E> void get(T t, U u) {
    static <T, U> T get(T t, U u) {
        if (u != null)
            return t;
        else
            return null;
    }

    private static class GenericClass<T> {
        // NOTE : the generic type can not declare static member arguments
        private T value;

        public GenericClass(T value) {
            this.value = value;
        }

        /**
         * The method is not generic method, just used the generic type of class.
         */
        public T getValue() {
            return value;
        }

        public void setValue(T value) {
            this.value = value;
        }

        /**
         * It's a generic instance method declared in generic class. Here an attention
         * should be paid that the generic "T" in method has nothing to do with the
         * generic "T" declared in class. They can be identical from each other, or not.
         * Additionally, you can name "T" in generic method to other letter as you like.
         */
        public <T, K> K genericInstanceMethod(T t) {
            return (K) t.toString();
        }

        /**
         * The static method cannot use generic feature unless it is declared generic method.
         * Otherwise, exception that genericClass.this cannot be referenced from a static context
         * will be thrown.
         */
        /*public static void genericStaticMethod(T t) {
            t.toString();
        }*/

        /**
         * It's a generic static method declared in generic class. the generic "T" in method
         * also has nothing to do with the generic T" declared in class.
         */
        public static <T> void genericStaticMethod(T t) {
            System.out.println(t.toString());
        }
    }

    public static void main(String[] args) {
        String string = get("HELLO", "WORLD");
        Integer integer = get(1, "HELLO");
        //System.out.println(string);
        //System.out.println(integer);

        GenericClass<Integer> genericClass = new GenericClass(123);
        genericClass.genericStaticMethod("STATIC GENERIC METHOD");

        //GenericClass.<String>genericStaticMethod("STATIC GENERIC METHOD");
    }
}
