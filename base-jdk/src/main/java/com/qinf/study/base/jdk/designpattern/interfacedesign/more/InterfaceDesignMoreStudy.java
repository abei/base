package com.qinf.study.base.jdk.designpattern.interfacedesign.more;

/**
 * Created by qinf on 2018/4/1 PM3:32:46
 */
public class InterfaceDesignMoreStudy {

    public static void main(String... strings) {
        IRequest odrRequest = new RequestOfOdr();
        IRequest mbrRequest = new RequestOfMbr();

        new OdrServiceImpl().process(odrRequest);
        new MbrServiceImpl().process(mbrRequest);
    }
}
