package com.qinf.study.base.jdk.language.java8;

import java.util.*;
import java.util.stream.Stream;

/**
 * Created by qinf on 2017/11/19 PM9:09:23
 */
public class OptionalStudy {

    static Map getResult() {
        Map map = new HashMap();
        map.put("K1", null);
        map.put("K2", new Obj("QIN"));
        //return null;
        return map;
    }

    static List<String> getListResult() {
        return null;
    }

    public static void main(String... args) {
        /** If getResult() return NULL, all statement will return "ERROR" from orElse("ERROR") */
        Object result = Optional.ofNullable(getResult())
                /** If m.get("K1") return NULL, all statement will also return "ERROR" from orElse("ERROR") */
                .map(m -> m.get("K1"))
                .orElse("ERROR");
        /** NOTE : No matter which(getResult() or m.get("K1")) return NULL, statement will return "ERROR" */
        System.out.println(result);

        // Exception in thread "main" java.lang.NumberFormatException: For input string: ""
        Integer integer = Optional.ofNullable("").map(Integer::valueOf).orElse(-1);
        System.out.println(integer);

        String string = Optional.ofNullable(getListResult()).orElse(new ArrayList<String>())
                .stream().filter(Objects::nonNull).findFirst().orElseGet(() -> null);
        System.out.println(string);

    }

    final private static class Obj {
        private String name;

        public Obj(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }
    }
}
