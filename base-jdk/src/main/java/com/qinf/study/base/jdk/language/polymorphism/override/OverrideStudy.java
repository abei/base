package com.qinf.study.base.jdk.language.polymorphism.override;

public class OverrideStudy {

    public static void main(String[] args) {
        /** father01 can only invoke its own methods */
        Father father01 = new Father();
        System.out.println(father01.getStr2());

        /** father02 can invoke its own methods or the methods overrided in its sub class */
        Father father02 = new Son("###");
        System.out.println(father02.getStr2());

        /** son actually is new Son("###"), even through it is casted from father02 */
        Son son = (Son) father02;
        System.out.println(son.getStr3());
    }
}
