package com.qinf.study.base.jdk.language.nestedclass;

public class MemberInnerClassStudy {

    private String outerMemberVariable = "IT IS OUTER MEMBER VARIABLE";

    public static void main(String[] args) {
        /**
         * For member inner class, it is depend on outer class, so
         * should first to create object of outer class, then by the outer class
         * object to create a object of inner class.
         */
        MemberInnerClassStudy outerClass = new MemberInnerClassStudy();
        MemberInnerClassStudy.MemberInnerClass innerClass = outerClass.new MemberInnerClass();
        innerClass.innerMemberMethod();
    }

    private void outerMemberMethod() {
        System.out.println("IT IS OUTER MEMEBER METHOD");
    }

    /**
     * Qualifier for class can be : public, protected, private, static, final,
     * abstract or without any qualifier.
     * NOTE : outer class just public or without any qualifier.
     */
    class MemberInnerClass {
        String innerMemberVariable = "IT IS INNER MEMBER VARIABLE";

        private void innerMemberMethod() {
            System.out.println("IT IS INNER MEMBER METHOD");
            System.out.print("call outer member private method : ");
            outerMemberMethod();
            System.out.print("call outer member private variable : " + outerMemberVariable);
        }
    }
}
