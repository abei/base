package com.qinf.study.base.jdk.language.java8.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * Created by qinf on 2017/11/26 PM12:04:24 <br>
 * <br>
 * There are 4 categories method reference in Lambda Expression :
 * 1. Static method reference
 * (args) -> ClassName.staticMethod(args); equals following :
 * ClassName::staticMethod;
 * <br>
 * 2. Instantial method reference
 * (String str) -> str.toUpperCase(); equals following :
 * String::toUpperCase;
 * <br>
 * 3. Instantial method reference
 * Object obj = ...;
 * (String str) -> obj.instanceMethod(str); equals following :
 * obj::instanceMethed;
 * <br>
 * 4. Construction method reference
 * Function<String, Apple> realAppleLambdaExpre = (color) -> new Apple(color);
 * Apple realApple = realAppleLambdaExpre.apply("red"); equals following :
 * Function<String, Apple> realAppleLambdaExpre = Apple::new;
 * Apple realApple = realAppleLambdaExpre.apply("red");
 */
public class LambdaOperation_MethodReferenceStudy {

    private static List<Apple> map(List<String> colors, Function<String, Apple> realAppleLambdaExpre) {
        List<Apple> realApples = new ArrayList<Apple>();
        for (String color : colors)
            realApples.add(realAppleLambdaExpre.apply(color));
        return realApples;
    }

    public static void main(String... strings) {
        List<String> colorContainer = Arrays.asList("red", "yellow", "blue");
        //List<Apple> realApples = map(colorContainer, color -> new Apple(color));
        List<Apple> realApples = map(colorContainer, Apple::new);

        for (Apple apple : realApples)
            System.out.println(apple.getColor());
    }

    private static class Apple {
        private String color;

        public Apple(String color) {
            this.color = color;
        }

        public String getColor() {
            return color;
        }
    }
}
