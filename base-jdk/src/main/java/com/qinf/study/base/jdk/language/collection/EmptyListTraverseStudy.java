package com.qinf.study.base.jdk.language.collection;

import java.util.Collections;
import java.util.List;

/**
 * Created by qinf on 2018/08/08
 */
public class EmptyListTraverseStudy {
    public static void main(String... args) {
        List<String> list = Collections.emptyList();
        /**
         * No exception throws.
         */
        for (String e : list)
            System.out.println(e);
    }
}
