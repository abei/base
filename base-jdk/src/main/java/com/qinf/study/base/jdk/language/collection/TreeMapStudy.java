package com.qinf.study.base.jdk.language.collection;

import java.util.Map;
import java.util.TreeMap;

public class TreeMapStudy {
    //static Map<String, String> map = new HashMap<String, String>();

    // The map is sorted according to the Comparable natural ordering of its keys
    static Map<Object, Object> map = new TreeMap<Object, Object>();

    public static void main(String[] args) {
        // alhpabet[key] sorted when use tree map
        /*map.put("b", "HELLOWORLD");
        map.put("c", "HELLOWORLD");
		map.put("a", "HELLOWORLD");
		map.put("f", "HELLOWORLD");
		map.put("d", "HELLOWORLD");
		map.put("e", "HELLOWORLD");
		map.put("g", "HELLOWORLD");
		map.put("h", "HELLOWORLD");*/

        // number[key] sorted when use tree map
        /*map.put("5", "HELLOWORLD");
        map.put("2", "HELLOWORLD");
		map.put("3", "HELLOWORLD");
		map.put("1", "HELLOWORLD");
		map.put("4", "HELLOWORLD");
		map.put("6", "HELLOWORLD");
		map.put("8", "HELLOWORLD");
		map.put("7", "HELLOWORLD");*/

        // number[key] sorted when use tree map
        map.put(new Integer(5), "HELLOWORLD");
        map.put(new Integer(2), "HELLOWORLD");
        map.put(new Integer(3), "HELLOWORLD");
        map.put(new Integer(1), "HELLOWORLD");
        map.put(new Integer(4), "HELLOWORLD");
        map.put(new Integer(6), "HELLOWORLD");
        map.put(new Integer(8), "HELLOWORLD");
        map.put(new Integer(7), "HELLOWORLD");
		
		/*Iterator<?> iter = map.entrySet().iterator();
		while (iter.hasNext()) {
			Entry<?, ?> entry = (Entry<?, ?>) iter.next();
			System.out.println(entry.getKey() + " : " + entry.getValue());
		}*/

        for (Map.Entry<?, ?> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }

    }
}
