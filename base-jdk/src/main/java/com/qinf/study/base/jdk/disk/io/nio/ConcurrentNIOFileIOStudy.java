package com.qinf.study.base.jdk.disk.io.nio;

/**
 * Created by qinf on 2021-02-15.
 *
 * Multiple concurrent threads can use a same instance of FileChannel safely.
 * However, only one thread at a time is allowed an operation that involves
 * updating a channel's position or changing its file size. This blocks other
 * threads attempting a similar operation until the previous operation completes.
 */
public class ConcurrentNIOFileIOStudy {
}
