package com.qinf.study.base.jdk.language.java8.lambda;

/**
 * Created by qinf on 2017/11/26 AM11:24:04
 * <p>
 * 1. Lambda Expression can reference instance variable and static variable
 * without limit in its block.
 * 2. Lambda Expression can reference local variable, but if a local variable
 * invoked by lambda, and then this local variable can not modified any more.
 * because it will cause unsafe-thread issue if the local variable changed again.
 */
public class LambdaOperation_LocalVariableInvokeStudy {

    public static void main(String... strings) {
        int localVariable = 100;
        new Thread(() -> System.out.println(localVariable)).start();
        /**
         * You cannot assign to the localVariable again, because in lambda
         * a new thread may be changing its value, now, if you want to change
         * its value again in main thread will cause Thread Insecurity.
         */
        //localVariable = 200;
    }
}
