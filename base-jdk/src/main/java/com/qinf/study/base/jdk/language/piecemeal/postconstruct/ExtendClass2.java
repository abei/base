package com.qinf.study.base.jdk.language.piecemeal.postconstruct;

/**
 * Created by qinf on 2021-02-03.
 */
//@Component
public class ExtendClass2 extends AbstractClass {

    public ExtendClass2() {
        System.out.println(this.getClass().getSimpleName());
    }

    @Override
    protected String register() {
        return this.getClass().getSimpleName();
    }
}
