package com.qinf.study.base.jdk.cpu.threadlocal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by qinf on 2017/10/31 PM9:50:17
 */
public class MyContext {

    private static ThreadLocal<Map<?, ?>> threadLocalMap = new ThreadLocal<Map<?, ?>>() {
        @Override
        protected Map<?, ?> initialValue() {
            HashMap<String, List<?>> context = new HashMap<String, List<?>>();
            context.put("context.qin.key", new ArrayList<String>());
            context.put("context.fen.key", new ArrayList<String>());
            return context;
        }
    };

    public static Map<?, ?> getContext() {
        return threadLocalMap.get();
    }

    /**
     * A ThreadLocalMap of a specified thread, just can contain a object by the
     * threadlocal instance as key in thread local map. That is say the
     * ThreadLocalMap just alway keep the state is {threadlocal : object} along
     * as the life of current thread.
     * set(o) method can override the original value in ThreadLocalMap by the
     * threadlocal as key.
     */
    public static void setContext(Map<?, ?> subcxt) {
        threadLocalMap.set(subcxt);
    }

    public static void clearContext() {
        threadLocalMap.remove();
    }
}
