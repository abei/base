package com.qinf.study.base.jdk.cpu.thread;

/**
 * When a thread is blocked by calling Thead.sleep() / Object.wait() /
 * Thread.join() / LockSupport.park(), and then call its interrupt(), its
 * interrupt status will be TRUE. But because it is in blocking, its interrupt
 * status will become FALSE immediately, and at the same time throws a
 * InterruptedException.
 */
public class InterruptJoiningThreadStudy {

    public static void main(String[] args) {
        // step 1.create new thread
        Thread joinedThread = new Thread(new ThreadJoined(), "joined thread");
        System.out.println("[" + joinedThread.getName() + "] life status is : [" + joinedThread.getState()
                + "], interrupt status is : " + "[" + joinedThread.isInterrupted() + "]");

        // step 2.start the new thread
        joinedThread.start();
        System.out.println("[" + joinedThread.getName() + "] life status is : [" + joinedThread.getState()
                + "], interrupt status is : " + "[" + joinedThread.isInterrupted() + "]");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("[" + joinedThread.getName() + "] life status is : [" + joinedThread.getState()
                + "], interrupt status is : " + "[" + joinedThread.isInterrupted() + "]");

        // step 4.main thread perform interrupt instruction
        /**
         * explanation : when we call joinedThread.interrupt(), we have known
         * that joinedThread has came into blocked state
         */
        joinedThread.interrupt();
        System.out.println("[" + joinedThread.getName() + "] life status is : [" + joinedThread.getState()
                + "] and has been interrupted, interrupt status is : " + "[" + joinedThread.isInterrupted() + "]");

        // step 6.check the last result
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("[" + joinedThread.getName() + "] life status is : [" + joinedThread.getState()
                + "], interrupt status is : " + "[" + joinedThread.isInterrupted() + "]");
    }

    static class ThreadRunning implements Runnable {
        public void run() {
            int cnt = 0;
            for (; ; ) {
                System.out.println("{loop " + cnt++ + "} - [" + Thread.currentThread().getName()
                        + "] life status is : [" + Thread.currentThread().getState() + "]");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (cnt == 4)
                    break;
            }
        }
    }

    static class ThreadJoined implements Runnable {
        public void run() {
            Thread runningThread = new Thread(new ThreadRunning(), "running thread");
            runningThread.start();
            try {
                // step 3.current thread into blocking status by calling join()
                runningThread.join();
            } catch (InterruptedException e) {
                // step 5.blocking status is interrupted, throws exception
                System.out.println("{catch exception} - [" + Thread.currentThread().getName() + "] life status is : ["
                        + Thread.currentThread().getState() + "], interrupt status is : " + "["
                        + Thread.currentThread().isInterrupted() + "]");
            }
        }
    }
}

/**
 * [joined thread] life status is : [NEW], interrupt status is : [false]
 * [joined thread] life status is : [RUNNABLE], interrupt status is : [false]
 * {loop 0} - [running thread] life status is : [RUNNABLE]
 * {loop 1} - [running thread] life status is : [RUNNABLE]
 * [joined thread] life status is : [WAITING], interrupt status is : [false]
 * [joined thread] life status is : [WAITING] and has been interrupted, interrupt status is : [true]
 * {catch exception} - [joined thread] life status is : [RUNNABLE], interrupt status is : [false]
 * {loop 2} - [running thread] life status is : [RUNNABLE]
 * {loop 3} - [running thread] life status is : [RUNNABLE]
 * [joined thread] life status is : [TERMINATED], interrupt status is : [false]
 */
