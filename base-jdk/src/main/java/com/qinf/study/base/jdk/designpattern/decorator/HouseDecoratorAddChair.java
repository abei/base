package com.qinf.study.base.jdk.designpattern.decorator;

/**
 * Created by qinf on 2017/7/16 PM9:19:29
 */
public class HouseDecoratorAddChair extends AbstractHouseDecorator {

    public HouseDecoratorAddChair(House house) {
        // Because AbstractHouseDecorator.class is a abstract class,
        // so here, have to invoke its constructor.
        super(house);
    }

    private final void addChair() {
        System.out.println("Decorator - add chair");
    }

    public void live() {
        this.addChair();
        super.live();
    }
}
