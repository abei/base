package com.qinf.study.base.jdk.designpattern.factory.factorymethod;

/**
 * Created by qinf on 2017/7/25 PM8:58:26
 */
public class Factory4FileLogger implements LoggerFactory {

    ILogger logger;

    @Override
    public ILogger createLogger() {
        logger = new FileLogger();
        return logger;
    }

}
