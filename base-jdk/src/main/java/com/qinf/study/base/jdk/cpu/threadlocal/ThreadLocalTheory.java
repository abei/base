package com.qinf.study.base.jdk.cpu.threadlocal;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by qinf on 2017/11/2 PM10:52:06
 * <p>
 * 1. Each living thread contains a ThreadLocalMap instance;
 * 2. ThreadLocal instance is key of the ThreadLocalMap of a current thread;
 * 3. A ThreadLocalMap of a living thread can contain multiple keys,
 *    that is to say threadlocal01, threadlocal02... can be its keys;
 * 4. When you invoke set(o) or get() of a ThreadLocal instance in a thread, then
 *    the ThreadLocal instance will be a key of the ThreadLocalMap belongs to the
 *    thread.
 * <p>
 * NOTE : Generally, you better define ThreadLocal as private static final, this
 * can make sure there only one ThreadLocal instance in whole JVM.
 * Then multiple ThraedLocalMaps of multiple threads can use the same(only one)
 * ThreadLocal instance as their key.
 */

/**
 * Using ThreadLocal as the process of thread executing :
 * 1. Thread01 just started, at this moment : [ThreadLocalMap = NULL] -->
 * 2. This thread01 execute instantiate threadLocal01, and threadLocal02 : [ThreadLocalMap = NULL] -->
 * 3. This thread01 execute threadLocal01.get() and threadLocal02.get(), at this time,
 *    will instantiate ThreadLocalMap : ThreadLocalMap = {threadLocal01:NULL, threadLocal02:NULL} -->
 *    If you override the initialValue method and do some initialized work, the
 *    ThreadLocalMap = {threadLocal01:VALUE01, threadLocal02:VALUE02} -->
 * 4. If thread01 execute threadLocal01.get(), will get value is VALUE01, if execute threadLocal02.get(),
 *    will get value is VALUE02.
 */

public class ThreadLocalTheory {
    /**
     * In fact it is : private Map threadLocalMap = Thread.currentThread().threadLocals;
     */
    private Map threadLocalMap = Collections.synchronizedMap(new HashMap());

    public Object get() {
        Thread curThread = Thread.currentThread();
        Object vluBelongToCurrentThread = threadLocalMap.get(curThread);

        if (vluBelongToCurrentThread == null && !threadLocalMap.containsKey(curThread)) {
            vluBelongToCurrentThread = initialValue();
            threadLocalMap.put(curThread, vluBelongToCurrentThread);
        }
        return vluBelongToCurrentThread;
    }

    public void set(Object newVluBelongToCurrentThread) {
        threadLocalMap.put(Thread.currentThread(), newVluBelongToCurrentThread);
    }

    /**
     * This method is protected, is mean it should be overrided by subclass.
     */
    protected Object initialValue() {
        return null;
    }
}
