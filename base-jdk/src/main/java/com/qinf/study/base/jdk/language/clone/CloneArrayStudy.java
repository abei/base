package com.qinf.study.base.jdk.language.clone;

import java.util.stream.Stream;

/**
 * 1. When the clone method is invoked upon an array, it returns a reference
 * to a new array which contains (or references) the same elements as the source
 * array.
 * 2. This becomes slightly more complicated when the source array contains
 * objects. The clone method will return a reference to a new array, which
 * references the same objects as the source array.<p>
 *
 * Created by qinf on 2018-06-03.
 */
public class CloneArrayStudy {

    private static final String[] arr = {"HELLO", "WORLD"};

    public static final String[] cloneArr() {
        return arr.clone();
    }

    public static void main(String... strings) {
        String[] clonedArr = cloneArr();
        clonedArr[0] = "QIN";

        // Stream.of(clonedArr).forEach(ele -> System.out.println(ele));
        Stream.of(clonedArr).forEach(System.out::println);
        Stream.of(arr).forEach(System.out::println);
    }
}
