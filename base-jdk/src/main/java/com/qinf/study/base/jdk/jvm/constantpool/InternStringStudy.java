package com.qinf.study.base.jdk.jvm.constantpool;

/**
 * Created by qinf on 2020-06-14.
 * <p>
 * The values in run-time constant pool comes from either compile-time or run-time. Takes string as example:<p>
 * 1) String string = "hello world";, the variable string has an already known value in compile-time, so the string literal
 *    "hello world" will be stored in run-time constant pool. No matter the variable string is method local variable or
 *    class global variable.<br>
 * 2) string.intern() is a run-time method used to store a string value to run-time constant pool in run-time.
 */
public class InternStringStudy {

    static String string1 = "hello world";

    private static String scpAssignment() {
        String string2 = "hello world";
        String string3 = "hello world";
        System.out.println("The address of two strings assigned into scp is：" + (string2 == string3));
        return string2;
    }

    public static void main(String... strings) {
        String string4 = "hello world";
        System.out.println("The address of two strings assigned into scp in another method is：" + (string4 == scpAssignment()));

        System.out.println("The address of two strings assigned into scp in different declared context is：" + (string1 == string4));

        String string5 = "constant pool";
        String string6 = new String("constant pool");
        String string7 = string6.intern();
        System.out.println("The address of two strings with different assignment ways is：" + (string5 == string6));
        System.out.println("The address of two strings interned into scp is：" + (string5 == string7));
    }
}
