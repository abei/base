package com.qinf.study.base.jdk.disk.io.nio;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Created by qinf on 2021-02-16.
 * <p>
 * FileChannel support lock mechanism for a file or a section of a file
 * to eliminate race condition.
 * Multiple threads can get a shared lock of a file at same time to get file content
 * concurrently(i.e., shared lock can reentrant), however, the exclusive lock of the
 * file cannot be obtained at the same time (at the moment, writing is not allowed).
 * If a thread got a exclusive lock of a file, then other threads cannot obtain exclusive
 * or shared lock of the file until the exclusive lock released.
 *
 * Note: some operating system may not support shared lock, this time shared lock will
 *       automatically convert into exclusive lock.
 * Note: so called shared lock is for multiple threads of a jvm(process) can read concurrently,
 *       however, for other threads of other process(e.g., another jvm instance) is still exclusive.
 */
public class FileLockStudy {

    static String FILE_PATH = "E:/git/base/base-jdk/src/main/java/com/qinf/study/base/jdk/disk/io/nio/src.txt";

    public static void main(String... strings) {

        FileLock blockingExclusiveLock = null;

        FileLock blockingSharedLock = null;

        FileLock nonBlockingExclusiveLock = null;

        FileLock nonBlockingSharedLock = null;

        try (RandomAccessFile file = new RandomAccessFile(FILE_PATH, "rw");
             FileChannel channel = file.getChannel()) {
            /**
             * Thread will be blocked if the lock is acquired by other thread.
             */
            blockingExclusiveLock = channel.lock();
            ByteBuffer buffer=ByteBuffer.wrap((new Date()+" in office\n").getBytes());
            channel.write(buffer);
            TimeUnit.SECONDS.sleep(20);

            /**
             * Thread will be blocked if the lock is acquired by other thread.
             * 1) different threads from different jvm instances is still exclusive
             * 2) shared just for reading operation, not for writing
             */
            //blockingSharedLock = channel.lock(0, Long.MAX_VALUE, Boolean.TRUE);

            /**
             * Thread will not be blocked if the lock is acquired by other thread, but will throw exception.
             */
            //nonBlockingExclusiveLock = channel.tryLock();

            /**
             * As same as "nonBlockingExclusiveLock"
             */
            //nonBlockingSharedLock = channel.tryLock(6, 5, Boolean.TRUE);

            //do other operations...

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                blockingExclusiveLock.release();
                //blockingSharedLock.release();
                //nonBlockingExclusiveLock.release();
                //nonBlockingSharedLock.release();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
