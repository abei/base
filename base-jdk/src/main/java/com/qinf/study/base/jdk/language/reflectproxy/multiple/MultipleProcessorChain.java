package com.qinf.study.base.jdk.language.reflectproxy.multiple;


import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by qinf on 2020-10-05.
 */
public class MultipleProcessorChain {

    //static Set<EmptyProcessor> processorChain = new TreeSet<>(Comparator.comparing(EmptyProcessor::getPriority));
    static Set<MultipleProcessor> multipleProcessorChain = new TreeSet<>();

    public static void main(String... strings) throws IOException {
        MultipleEmptyProcessor multipleEmptyProcessor1 = new MultipleEmptyProcessor();
        multipleEmptyProcessor1.setPriority(1);
        MultipleEmptyProcessor multipleEmptyProcessor2 = new MultipleEmptyProcessor();
        multipleEmptyProcessor2.setPriority(2);
        MultipleEmptyProcessor multipleEmptyProcessor3 = new MultipleEmptyProcessor();
        multipleEmptyProcessor3.setPriority(3);
        MultipleProcessor multipleProcessor1 = (MultipleProcessor) new MultipleProxyFactory().getProxyObj(multipleEmptyProcessor1);
        MultipleProcessor multipleProcessor2 = (MultipleProcessor) new MultipleProxyFactory().getProxyObj(multipleEmptyProcessor2);
        MultipleProcessor multipleProcessor3 = (MultipleProcessor) new MultipleProxyFactory().getProxyObj(multipleEmptyProcessor3);

        multipleProcessorChain.add(multipleProcessor1);
        multipleProcessorChain.add(multipleProcessor2);
        multipleProcessorChain.add(multipleProcessor3);

        for (MultipleProcessor processor : multipleProcessorChain) {
            processor.process(100);
        }
    }

}
