package com.qinf.study.base.jdk.designpattern.factory.abstractfactory;

/**
 * Created by qinf on 2017/7/25 PM10:04:22
 */
public class QINRoomFactory implements RoomFactory {

    Room qin_BathRoom;
    Room qin_BedRoom;

    @Override
    public Room createBedRoom() {
        qin_BedRoom = new QIN_BedRoom();
        return qin_BedRoom;
    }

    @Override
    public Room createBathRoom() {
        qin_BathRoom = new QIN_BathRoom();
        return qin_BathRoom;
    }

}
