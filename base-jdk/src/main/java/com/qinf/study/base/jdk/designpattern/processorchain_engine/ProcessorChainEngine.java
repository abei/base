package com.qinf.study.base.jdk.designpattern.processorchain_engine;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Objects;

/**
 * Created by qinf on 2021-10-02.
 */
@Slf4j
public abstract class ProcessorChainEngine<Q, P> implements ProcessorChain<Q, P> {

    protected List<Processor> processors = null;

    @Override
    public void process(Q request, P response) {
        Pair<Request, Response> reqAndResArgs = this.constructProcessorArgs(request, response);
        Objects.requireNonNull(reqAndResArgs, "The constructed args is null!");
        this.doProcess(reqAndResArgs.getLeft(), reqAndResArgs.getRight());
    }

    @Override
    public void process(Q request, P response, Callback callback) {
        Objects.requireNonNull(callback, "Callback function is null!");
        this.process(request, response);
        callback.callback(request, response, null);
    }

    @Override
    public void process(Q request, P response, Callback callback, Object... args) {
        Objects.requireNonNull(callback, "Callback function is null!");
        this.process(request, response);
        callback.callback(request, response, args);
    }

    private Pair constructProcessorArgs(Q request, P response) {
        Objects.requireNonNull(request, "The request is null!");
        return this.doConstructProcessorArgs(request, response);
    }

    /**
     * The invoker can decide how to construct the arguments to be executed.
     *
     * @param request  the request to execute
     * @param response the response to check
     */
    public abstract Pair doConstructProcessorArgs(Q request, P response);

    /**
     * If you have particular processor logic to be executed, you can override the method in subclass.
     * E.g., protected void doProcess(final Request request, final Response response) or
     * public void doProcess(final Request request, final Response response) can work.
     *
     * @param request  the request to execute
     * @param response the response to check
     * @return
     */
    protected void doProcess(final Request request, final Response response) {
        if (Objects.isNull(processors) || processors.isEmpty()) {
            throw new RuntimeException("Processors used in chain engine are not yet initialized!");
        }
        for (Processor processor : processors) {
            try {
                processor.process(request, response, this);
            } catch (Exception e) {
                log.error("Processor chain engine executed exception! request: {}, response: {}, interrupted processor: {}，exception: ", request, response,
                        processor.getClass().getSimpleName(), e);
                throw new RuntimeException(e);
            }
            if (response.isExitInstantly()) {
                log.info("Processor chain engine executed successfully! request: {}, response：{}, terminal processor: {}", request, response, processor.getClass().getSimpleName());
                return;
            }
        }
    }

    /**
     * Get active processors of processor chain.
     */
    @Override
    public List<Processor> getProcessors() {
        return this.processors;
    }
}
