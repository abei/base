package com.qinf.study.base.jdk.cpu.juc.ext_synctool;

import java.util.concurrent.Semaphore;

/**
 * Created by qinf on 2020-11-19.
 */
public class SemaphoreRepeatRequireStudy {

    /**
     * A same thread cannot acquire the semaphore repeatedly.
     */
    private static Semaphore semaphore = new Semaphore(1);

    private static void firstAcquirePrint() throws InterruptedException {
        semaphore.acquire();
        System.out.println("Has acquired, but not released.");
        //semaphore.release();
    }

    private static void secondAcquirePrint() throws InterruptedException {
        semaphore.acquire();
        System.out.println("Want acquire again!");
    }

    public static void main(String... strings) throws InterruptedException {
        firstAcquirePrint();
        /**
         * The second acquire print method will be hanged.
         */
        secondAcquirePrint();
    }
}
