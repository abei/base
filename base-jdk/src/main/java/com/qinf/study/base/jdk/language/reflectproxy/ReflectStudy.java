package com.qinf.study.base.jdk.language.reflectproxy;

import java.io.Serializable;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by qinf on 2017/7/3 PM9:18:14
 */
public class ReflectStudy implements Serializable, Comparable<Object> {
    private static final long serialVersionUID = 1L;
    /**
     * First, we get know what the REFLECT can do.
     * 1. In runtime, to judge the object belong to which class.
     * 2. In runtime, to judge what member method and field are included in a class.
     * 3. In runtime, to create a object of arbitrary class.
     * 4. In runtime, to invoke instantial method and field of a object.
     * 5. In runtime, to create dynamic proxy.
     */
    private String studyDate;
    private String studyContent;
    @SuppressWarnings("unused")
    private String studySchedule;

    public ReflectStudy() {
        super();
    }

    public ReflectStudy(String studyDate) {
        super();
        this.studyDate = studyDate;
    }

    public ReflectStudy(String studyDate, String studyContent) {
        super();
        this.studyDate = studyDate;
        this.studyContent = studyContent;
    }

    public static void main(String[] args)
            throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException,
            InvocationTargetException, SecurityException, NoSuchMethodException, NoSuchFieldException {

        /** 1. instantiate a Class.java */
        Class<?> cls = Class.forName("com.qinf.study.base.jdk.language.reflectproxy.ReflectStudy");
        Class<?> cls01 = ReflectStudy.class;
        Class<?> cls02 = new ReflectStudy().getClass();

        /** 2. get its own parent class or it implemented interface */
        System.out.println("its own superclass name is : " + cls.getSuperclass().getName());
        Class<?> interfaces[] = cls.getInterfaces();
        for (Class<?> owninterface : interfaces)
            System.out.println("its own interface name is : " + owninterface.getName());

        /** 3. get its own constructors */
        Constructor<?> constructors[] = cls.getConstructors();
        for (Constructor<?> ownconstructor : constructors)
            System.out.println("its own constructor is : " + ownconstructor);

        /** 4. get its own or its super class member field info */
        Field[] ownFields = cls.getDeclaredFields();
        for (Field ownfield : ownFields) {
            String ownFieldModifier = Modifier.toString(ownfield.getModifiers());
            String ownFieldType = ownfield.getType().getName();
            System.out.println("its own field modifer and type is : " + ownFieldModifier + "\t" + ownFieldType);
        }
        Field[] superclsFields = cls.getFields();
        for (Field superclsfield : superclsFields) {
            String superclsFieldModifier = Modifier.toString(superclsfield.getModifiers());
            String superclsFieldType = superclsfield.getType().getName();
            System.out.println(
                    "its superclass field modifer and type is : " + superclsFieldModifier + "\t" + superclsFieldType);
        }

        /**
         * 5. get its own member method info
         * NOTE : It is not support autoboxing when get method object by reflect parameter type.
         */
        // Method[] ownMethods = cls.getMethods(); // get all methods, include the cls declared and extended.
        Method[] ownMethods = cls.getDeclaredMethods(); // get all methods declared in it.
        for (Method ownmethod : ownMethods) {
            String ownMethodName = ownmethod.getName();
            String ownMethodModifier = Modifier.toString(ownmethod.getModifiers());
            String ownMethodRtnType = ownmethod.getReturnType().getName();
            System.out.println("its own method name & modifier & return type is : " + ownMethodName + "\t"
                    + ownMethodModifier + "\t" + ownMethodRtnType);
        }

        /** 6. invoke instantial method of a object */
        ReflectStudy obj01 = (ReflectStudy) cls.getConstructors()[2].newInstance("2017-07-04", "JavaReflectStudy");
        System.out.println("invoke instantial method by instance : " + obj01.getStudyDate() + "\t" + obj01.getStudyContent());

        ReflectStudy obj02 = new ReflectStudy("2017-07-04", "JavaReflectStudy");
        obj02.setStudyContent("JavaReflectStudy");
        // Method reflectMothod01 = cls.getMethod("getStudyContent"); // this statement can also invoke method correctly
        Method reflectMothodNoParam = obj02.getClass().getMethod("getStudyContent");
        Object studyContent = reflectMothodNoParam.invoke(obj02);
        System.out.println("invoke instantial method by reflect without param : " + studyContent.toString());

        // when get method by reflect, you need specify its name and its parameter type
        Method reflectMethodWithParam = obj02.getClass().getMethod("getStudySchedule", String.class);
        Object studySchedule = reflectMethodWithParam.invoke(obj02, "StepByStep");
        System.out.println("invoke instantial method by reflect with param : " + studySchedule.toString());

        // get method from interface, invoke by its implementation
        //Method m = Class.forName("com.qinf.study.reflectproxy.SubjectImpl").getMethod("performBusinessLogic");
        Method m = Class.forName("com.qinf.study.reflectproxy.Subject").getMethod("performBusinessLogic");
        m.invoke(new JDK_Subject01Impl());

        /** 7. invoke instantial field of a object */
        Object obj03 = cls.newInstance();
        Field reflectField = cls.getDeclaredField("studySchedule");
        reflectField.setAccessible(true);
        reflectField.set(obj03, "FromToday");
        System.out.println("invoke instantial field by reflect : " + reflectField.get(obj03));

        /** a little tricky thing for generic list by reflect */
        List<Integer> list = new ArrayList<Integer>();
        Method addOfList = list.getClass().getMethod("add", Object.class);
        addOfList.invoke(list, "String");
        System.out.println("Integer generic list can add string by reflect : " + list.get(0));
    }

    public int compareTo(Object o) {
        return 0;
    }

    public String getStudyDate() {
        return this.studyDate;
    }

    public void setStudyDate(String studyDate) {
        this.studyDate = studyDate;
    }

    public String getStudyContent() {
        return this.studyContent;
    }

    public void setStudyContent(String studyContent) {
        this.studyContent = studyContent;
    }

    public String getStudySchedule(String studySchedule) {
        return studySchedule;
    }

    public void setStudySchedule(String studySchedule) {
        this.studySchedule = studySchedule;
    }
}
