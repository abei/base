package com.qinf.study.base.jdk.language.enum_;

/**
 * 1. Enum can be used as switch.
 */
public class EnumBaseStudy {

    public static void main(String... args) {
        System.out.println(EnumBaseType.RED);
        System.out.println(EnumBaseType.RED.getColor());
        System.out.println(EnumBaseType.BLUE);
        System.out.println(EnumBaseType.BLUE.getColor());
    }
}
