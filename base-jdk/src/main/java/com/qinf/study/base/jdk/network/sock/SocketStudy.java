package com.qinf.study.base.jdk.network.sock;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by qinf on 2021-09-08.
 *
 * Socket is a set APIs supplied by OS used to operate the TCP/IP ability, e.g., let OS listen to a specified port,
 * create TCP connection between two servers, transmit data by the TCP connection etc.
 *
 * A TCP connection composed by 5 elements that make it unique:
 *   1. IP of the server
 *   2. IP of the client
 *   3. Port of the server
 *   4. Port of the client (the core will assign an idle port to a connection)
 *   5. Protocol (UDP, TCP/IP, etc...)
 * Take TCP as instance:
 *   client: 172.0.1.100:61359 <--> server: 168.1.1.200:8080 => it's a TCP connection
 *   client: 172.0.1.100:61359 <--> server: 168.1.1.200:8081 => it's another TCP connection
 *   client: 172.0.1.100:61360 <--> server: 168.1.1.200:8081 => it's another TCP connection
 * Above 3 connections is different from each, meanwhile, you should note a client port can be used for different connections.
 *
 * Assume a process-1(might a jvm thread-1) is creating a client socket, the process as following:
 *   1). process-1 will open a file descriptor(fd:1900) by invoke native int socket0(boolean stream, boolean v6Only)
 *   2). then execute connect operation to remote server(ip2:7777) actually by a three-way handshake with the random
 *       idle port(61359)(range from 49152 to 65535, can be configured kernel) assigned by TCP stack
 *   3). next get the port(61359) by invoke native int localPort0(int fd) and assign the socket local port, means connection
 *       already complete
 *   4). now, process-1 can transmit data by the client socket instance(i.e., a established TCP connection) through net card
 *   5). the server endpoint process-2 handle the request and reply response to the client(ip1:61359) by the TCP connection
 *   6). back to client net card, the TCP stack will look at the client local port in the packet(response), and route it to
 *       process-1 was assigned that local port(61359)
 * Note: the 6) is the basic theory of connection pool.
 */
public class SocketStudy {

    public static void main(String... strings) throws IOException, InterruptedException {
        CompletableFuture
                .runAsync(() -> Server.launch(7777))
                .handle((res, e) -> e);
        TimeUnit.SECONDS.sleep(1);
        Client.createConn("127.0.0.1", 7777);
        String response = Client.sendMessage("hello server");
        System.out.println("Get response from server: " + response);
        Client.closeConn();
    }
}
