package com.qinf.study.base.jdk.designpattern.factory.abstractfactory;

/**
 * Created by qinf on 2017/7/25 PM9:55:02
 */
public interface RoomFactory {

    public Room createBedRoom();

    public Room createBathRoom();

}
