package com.qinf.study.base.jdk.jvm.classloader;

import java.net.URL;

/**
 * Created by qinf on 2017/11/25 AM11:27:46
 * <p>
 * When you use public JRE(JVM) run your class file, that is to say you
 * set "JRE System Library" as public JRE in Eclipse, you get following result :
 * 1. sun.misc.Launcher.getBootstrapClassPath().getURLs() :
 * file:/C:/Program%20Files/Java/jre1.8.0_151/lib/resources.jar
 * file:/C:/Program%20Files/Java/jre1.8.0_151/lib/rt.jar
 * file:/C:/Program%20Files/Java/jre1.8.0_151/lib/sunrsasign.jar
 * file:/C:/Program%20Files/Java/jre1.8.0_151/lib/jsse.jar
 * file:/C:/Program%20Files/Java/jre1.8.0_151/lib/jce.jar
 * file:/C:/Program%20Files/Java/jre1.8.0_151/lib/charsets.jar
 * file:/C:/Program%20Files/Java/jre1.8.0_151/lib/jfr.jar
 * file:/C:/Program%20Files/Java/jre1.8.0_151/classes
 * <p>
 * 2. bootstrap class path (for Bootstrap class loader) :
 * C:\Program Files\Java\jre1.8.0_151\lib\resources.jar; (the core Java API)
 * C:\Program Files\Java\jre1.8.0_151\lib\rt.jar; (the core Java API)
 * C:\Program Files\Java\jre1.8.0_151\lib\sunrsasign.jar;
 * C:\Program Files\Java\jre1.8.0_151\lib\jsse.jar;
 * C:\Program Files\Java\jre1.8.0_151\lib\jce.jar;
 * C:\Program Files\Java\jre1.8.0_151\lib\charsets.jar;
 * C:\Program Files\Java\jre1.8.0_151\lib\jfr.jar;
 * C:\Program Files\Java\jre1.8.0_151\classes
 * <p>
 * 3. extension class path (for Extension class loader):
 * C:\Program Files\Java\jre1.8.0_151\lib\ext;
 * C:\windows\Sun\Java\lib\ext
 * <p>
 * 4. app class path (for App(or call it System) class loader) :
 * G:\study-code bak\study\study-jdk\target\classes;
 * E:\program files\mvn repository\org\apache\commons\commons-collections4\4.1\commons-collections4-4.1.jar;
 * E:\program files\mvn repository\cglib\cglib\3.2.5\cglib-3.2.5.jar;
 * E:\program files\mvn repository\org\ow2\asm\asm\5.2\asm-5.2.jar;
 * E:\program files\mvn repository\org\apache\ant\ant\1.9.6\ant-1.9.6.jar;
 * E:\program files\mvn repository\org\apache\ant\ant-launcher\1.9.6\ant-launcher-1.9.6.jar;
 * E:\program files\mvn repository\asm\asm\3.3.1\asm-3.3.1.jar;
 * E:\program files\mvn repository\commons-lang\commons-lang\2.6\commons-lang-2.6.jar;
 * E:\program files\mvn repository\org\slf4j\slf4j-api\1.5.10\slf4j-api-1.5.10.jar;
 * E:\program files\mvn repository\org\slf4j\jcl-over-slf4j\1.5.10\jcl-over-slf4j-1.5.10.jar;
 * E:\program files\mvn repository\org\slf4j\slf4j-log4j12\1.5.10\slf4j-log4j12-1.5.10.jar;
 * E:\program files\mvn repository\log4j\log4j\1.2.15\log4j-1.2.15.jar
 * <p>
 * NOTE : If the class is run on public JRE(includes JVM),
 * the path of public JRE is : C:\Program Files\Java\jre1.8.0_151\
 * the String.class imported in your class file is : C:\Program Files\Java\jre1.8.0_151\lib\rt.jar!/java/lang/String.class
 * NOTE : Of cause, you can also run your class on private JRE(also includes another JVM),
 * this time the path of private JRE is : C:\Program Files\Java\jdk1.8.0_151\jre\
 * the String.class imported in your class file is : C:\Program Files\Java\jdk1.8.0_151\jre\lib\rt.jar!/java/lang/String.class
 */
public class BootstrapClassLoaderStudy {

    public static void main(String... args) {
        URL[] urls = sun.misc.Launcher.getBootstrapClassPath().getURLs();
        for (int i = 0; i < urls.length; i++)
            System.out.println(urls[i].toExternalForm());

        System.out.println(System.getProperty("sun.boot.class.path"));
        System.out.println(System.getProperty("java.ext.dirs"));
        // Get all the existing class path.
        System.out.println(System.getProperty("java.class.path"));
    }
}
