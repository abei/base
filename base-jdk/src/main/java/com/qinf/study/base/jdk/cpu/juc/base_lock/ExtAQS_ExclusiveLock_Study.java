package com.qinf.study.base.jdk.cpu.juc.base_lock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * Created by qinf on 2017/12/17 PM9:05:37
 * <p>
 * AQS supplies a framework to implement exclusive lock.
 * <p>
 * So you can implement your own exclusive lock by AQS, you need override 2
 * methods in AQS.
 */
public class ExtAQS_ExclusiveLock_Study {

    private final Sync sync = new Sync();

    public static void main(String... strings) throws InterruptedException {
        final ExtAQS_ExclusiveLock_Study exclusiveLock = new ExtAQS_ExclusiveLock_Study();
        MyThread t1 = new MyThread("Thread-01", exclusiveLock);
        MyThread t2 = new MyThread("Thread-02", exclusiveLock);
        MyThread t3 = new MyThread("Thread-03", exclusiveLock);

        t1.start();
        t2.start();
        t3.start();

        /**
         * NOTE : join() will make current thread blocked until sub-thread completed.
         * 1. if first : t1.start();t2.start();t3.start();
         * 	  and then : t1.join();t2.join();t3.join();
         * 	  that will make main-thread blocked until t1,t2,t3 all completed.
         * 2. if start and join thread alternatively, then all the threads will run parallelly.
         * 	  t1.start();t1.join(); t2.start();t2.join(); t3.start();t3.join();
         */
        t1.join();
        t2.join();
        t3.join();
    }

    public void exclusiveLock() {
        sync.acquire(1);
    }

    public void unlock() {
        sync.release(1);
    }

    private static class Sync extends AbstractQueuedSynchronizer {
        private static final long serialVersionUID = 1L;

        protected Sync() {
            super();
        }

        @Override
        protected boolean tryAcquire(int ignore) {
            /** The state is volatile. compareAndSetState() is atomic operation. */
            return compareAndSetState(0, 1);
        }

        @Override
        protected boolean tryRelease(int igore) {
            /** setState() is also atomic operation. */
            setState(0);
            return true;
        }
    }

    private static class MyThread extends Thread {
        private final String tName;
        private final ExtAQS_ExclusiveLock_Study myLock;

        private MyThread(String name, ExtAQS_ExclusiveLock_Study myLock) {
            this.tName = name;
            this.myLock = myLock;
        }

        public void run() {
            try {
                myLock.exclusiveLock();
                System.out.println(tName + " has acquired the exclusiveLock.");
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                myLock.unlock();
                System.out.println(tName + " has released the exclusiveLock.");
            }
        }
    }
}
