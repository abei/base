package com.qinf.study.base.jdk.language.piecemeal;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by qinf on 2018/5/15.
 */
public class DateTimeZoneStudy {

    public static void main(String... strings) {
        Date date = null;
        String time = "2018/5/14 9:00";
        System.out.println("Date in string : " + time);
        DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            date = df.parse(time);
            System.out.println("Date with timeZone : " + date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        time = df.format(date);
        System.out.println("Date format with old DF : " + time);
        time = new SimpleDateFormat("yyyy/MM/dd HH:mm").format(date);
        System.out.println("Date format with new DF : " + time);
    }
}
