package com.qinf.study.base.jdk.designpattern.template;

/**
 * Created by qinf on 2021-09-13.
 */
public class ToCOrderServiceImpl extends AbstractOrderService {

    @Override
    public Boolean upsertOrder(String orderEntity) {
        System.out.println("To C upsert order service: " + orderEntity);
        return Boolean.TRUE;
    }

    /*@Override
    public Boolean delegateUpsertOrder(String orderEntity, String extraOperation) {
        System.out.println("To C delegate upsert order service: " + orderEntity);
        this.upsertOrder(orderEntity);
        return Boolean.TRUE;
    }*/
}
