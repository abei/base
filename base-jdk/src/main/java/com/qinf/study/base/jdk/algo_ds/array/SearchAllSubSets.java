package com.qinf.study.base.jdk.algo_ds.array;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qinf on 2020-11-13.
 */
public class SearchAllSubSets {

    /**
     * width == 0: []
     * width == 1: [1], [2], [3], [4]
     * width == 2: [1,2], [1,3], [1,4], [2,3], [2,4], [3,4]
     * width == 3: [1,2,3], [1,2,4], [1,3,4], [2,3,4]
     * width == 4: [1,2,3,4]
     */
    private static void getAllSubSets(int[] nums, int width, List<Integer> subSet, List<List<Integer>> allSubSets) {
        allSubSets.add(new ArrayList<>(subSet));
        for (int i = width; i < nums.length; i++) {
            subSet.add(nums[i]);
            getAllSubSets(nums, i + 1, subSet, allSubSets);
            subSet.remove(subSet.size() - 1);
        }
    }

    public static void main(String... strings) {
        List<Integer> subSet = new ArrayList<>();
        List<List<Integer>> allSubSets = new ArrayList<>();
        getAllSubSets(new int[]{1, 2, 3, 4}, 0, subSet, allSubSets);
        System.out.print(allSubSets);
    }
}
