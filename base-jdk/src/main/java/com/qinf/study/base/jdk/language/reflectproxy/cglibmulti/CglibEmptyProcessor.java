package com.qinf.study.base.jdk.language.reflectproxy.cglibmulti;

/**
 * Created by qinf on 2020-10-05.
 */
public class CglibEmptyProcessor implements Comparable<CglibEmptyProcessor>, CglibProcessor {

    private Integer priority;

    @Override
    public int compareTo(CglibEmptyProcessor cep) {
        return this.priority - cep.getPriority();
    }

    public void process(Integer orderId) {
        System.out.println("Order: " + orderId + " is processing.");
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
