package com.qinf.study.base.jdk.designpattern.interfacedesign;

import java.util.HashMap;

public class InterfaceDesignStudy {

    public static void main(String... strings) {
        // Generally, will get bean from sprint context.
        //IService service = new TransferServiceImpl();
        IService service = new OrderServiceImpl();
        service.process(
                new HashMap<String, String>(),
                new HashMap<String, String[]>(),
                new HashMap<String, Object>(),
                new HashMap<String, Object>(),
                "INVOKE",
                new HashMap<String, Object>());
    }
}
