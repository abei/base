package com.qinf.study.base.jdk.designpattern.decorator;

/**
 * Created by qinf on 2017/7/16 PM9:00:14
 */
public class BathRoomImpl implements House {

    @Override
    public void live() {
        System.out.println("This is bathroom.");
    }
}
