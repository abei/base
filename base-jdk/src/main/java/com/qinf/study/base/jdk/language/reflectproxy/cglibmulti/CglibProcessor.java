package com.qinf.study.base.jdk.language.reflectproxy.cglibmulti;

/**
 * Created by qinf on 2020-10-05.
 */
public interface CglibProcessor {

    void process(Integer orderId);

}
