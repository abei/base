package com.qinf.study.base.jdk.language.java8.lambda;

/**
 * Created by qinf on 2017/12/2 PM4:04:03
 * <p>
 * Just only consume something.
 * Pass in a argument t, no return : void accept(T t);
 */
public class FunctionalInterface_Consumer {

}
