package com.qinf.study.base.jdk.designpattern.decorator;

/**
 * Created by qinf on 2017/7/16 PM9:18:21
 */
public class HouseDecoratorAddDesk extends AbstractHouseDecorator {

    public HouseDecoratorAddDesk(House house) {
        super(house);
    }

    private final void addDesk() {
        System.out.println("Decorator - add desk");
    }

    public void live() {
        this.addDesk();
        super.live();
    }
}
