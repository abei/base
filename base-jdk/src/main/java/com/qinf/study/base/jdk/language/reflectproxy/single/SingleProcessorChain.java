package com.qinf.study.base.jdk.language.reflectproxy.single;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qinf on 2020-10-05.
 */
public class SingleProcessorChain {

    static List<SingleProcessor> singleProcessorChain = new ArrayList<>(2);

    public static void main(String... strings) {
        SingleEmptyProcessor singleEmptyProcessor1 = new SingleEmptyProcessor();
        singleEmptyProcessor1.setPriority(1);
        SingleEmptyProcessor singleEmptyProcessor2 = new SingleEmptyProcessor();
        singleEmptyProcessor2.setPriority(2);
        SingleProcessor singleProcessor1 = (SingleProcessor) new SingleProxyFactory().getProxyObj(singleEmptyProcessor1);
        //SingleProcessor singleProcessor2 = (SingleProcessor) new SingleProxyFactory().getProxyObj(singleEmptyProcessor2);

        singleProcessorChain.add(singleProcessor1);
        //singleProcessorChain.add(singleProcessor2);
        singleProcessorChain.get(0).process(100);
    }

}
