package com.qinf.study.base.jdk.designpattern.template;

/**
 * Created by qinf on 2021-09-13.
 */
public class ZEntry {

    public static void main(String... strings) {
        OrderService orderServiceB = new ToBOrderServiceImpl();
        orderServiceB.delegateUpsertOrder("test to b", "");

        OrderService orderServiceC = new ToCOrderServiceImpl();
        orderServiceC.delegateUpsertOrder("test to c", "");
    }
}
