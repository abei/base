package com.qinf.study.base.jdk.language.java8.stream;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by qinf on 2017/11/28 PM4:37:05
 */
public class CollectorsToSetStudy {

    public static void main(String... strings) {
        Stream<String> stream = Arrays.asList(new String[]{"HELLO", "HELLO", "STREAM", null}).stream();
        Set<String> hashSet = stream.collect(Collectors.toSet());
        for (String e : hashSet)
            System.out.println(e);

        List<String> list = Arrays.asList(new String[]{"HELLO", "HELLO", "STREAM"});
        Set<String> treeSet = list.stream().collect(Collectors.toCollection(TreeSet::new));
        treeSet.forEach(System.out::println);
    }
}
