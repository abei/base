package com.qinf.study.base.jdk.language.java8;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by qinf on 2018/4/9.
 */
public class ListNestedListToListByStreamStudy {

    public static void main(String... strings) {
        Game game01 = new Game("g01-id", "g01-name");
        Game.Option Option01_01 = new Game.Option("Option01-id-01", "Option01-name-01");
        Game.Option Option01_02 = new Game.Option("Option01-id-02", "Option01-name-02");
        Game.Option Option01_03 = new Game.Option("Option01-id-03", "Option01-name-03");
        List<Game.Option> OptionList_01 = Arrays.asList(Option01_01, Option01_02, Option01_03);
        game01.setOption(OptionList_01);

        Game game02 = new Game("g02-id", "g02-name");
        Game.Option Option02_01 = new Game.Option("Option02-id-01", "Option02-name-01");
        Game.Option Option02_02 = new Game.Option("Option02-id-02", "Option02-name-02");
        Game.Option Option02_03 = new Game.Option("Option02-id-03", "Option02-name-03");
        List<Game.Option> OptionList_02 = Arrays.asList(Option02_01, Option02_02, Option02_03);
        game02.setOption(OptionList_02);

        List<Game> gameList = Arrays.asList(game01, game02);

        /** Convert Game into CmpGame(Option into Offer) */
        List<CmpGame> cmpGameList = gameList.stream().map(g -> {
            CmpGame cmpGame = new CmpGame();
            cmpGame.setCmpGameId(g.getGameId());
            cmpGame.setCmpGameName(g.getGameName());
            List<CmpGame.Offer> offers = g.getOption().stream().map(o -> {
                CmpGame.Offer offer = new CmpGame.Offer();
                offer.setOfferId(o.getOptionId());
                offer.setOfferName(o.getOptionName());
                return offer;
            }).collect(Collectors.toList());
            cmpGame.setOffers(offers);
            return cmpGame;
        }).collect(Collectors.toList());

        cmpGameList.forEach(c -> System.out.println(c.getCmpGameName()));
    }

    private static class Game {
        private String gameId;
        private String gameName;
        private List<Option> Option;

        public Game(String gameId, String gameName) {
            this.gameId = gameId;
            this.gameName = gameName;
        }

        public String getGameId() {
            return gameId;
        }

        @SuppressWarnings("unused")
        public void setGameId(String gameId) {
            this.gameId = gameId;
        }

        public String getGameName() {
            return gameName;
        }

        @SuppressWarnings("unused")
        public void setGameName(String gameName) {
            this.gameName = gameName;
        }

        public List<Option> getOption() {
            return Option;
        }

        public void setOption(List<Option> Option) {
            this.Option = Option;
        }

        static class Option {
            private String OptionId;
            private String OptionName;

            public Option(String OptionId, String OptionName) {
                this.OptionId = OptionId;
                this.OptionName = OptionName;
            }

            public String getOptionId() {
                return OptionId;
            }

            @SuppressWarnings("unused")
            public void setOptionId(String OptionId) {
                this.OptionId = OptionId;
            }

            public String getOptionName() {
                return OptionName;
            }

            @SuppressWarnings("unused")
            public void setOptionName(String OptionName) {
                this.OptionName = OptionName;
            }
        }
    }

    static class CmpGame {
        private String cmpGameId;
        private String cmpGameName;
        private List<Offer> offers;

        public String getCmpGameId() {
            return cmpGameId;
        }

        public void setCmpGameId(String cmpGameId) {
            this.cmpGameId = cmpGameId;
        }

        public String getCmpGameName() {
            return cmpGameName;
        }

        public void setCmpGameName(String cmpGameName) {
            this.cmpGameName = cmpGameName;
        }

        public List<Offer> getOffers() {
            return offers;
        }

        public void setOffers(List<Offer> offers) {
            this.offers = offers;
        }

        static class Offer {
            private String OfferId;
            private String OfferName;

            public String getOfferId() {
                return OfferId;
            }

            public void setOfferId(String offerId) {
                OfferId = offerId;
            }

            public String getOfferName() {
                return OfferName;
            }

            public void setOfferName(String offerName) {
                OfferName = offerName;
            }
        }
    }
}
