package com.qinf.study.base.jdk.designpattern.builder;

/**
 * Created by qinf on 2017/8/21 PM11:09:42
 *
 * The timing when using builder pattern is :
 * If a object is quite complicated, we can use builder pattern to create it.
 * The builder pattern can make the process of creation more clearer, and can
 * reduce creation complexity.
 */
public class BuilderPatternStudy {

    public static void main(String... args) {
        /** In inner class UserBuilder, these two parameters must be known. */
        User user = new User.UserBuilder("fen", "qin")
                .age(18)
                .phone("15117001124")
                .address("bj")
                .build();
        System.out.println(user.toString());

    }
}
