package com.qinf.study.base.jdk.designpattern.interfacedesign.more;

/**
 * Created by qinf on 2018/1/29 PM10:15:34
 * <p>
 * 1. Abstract class supply a template to encapsulate common functions. That can
 * make sure sub class extended it do not have to do some repeating work. E.g.,
 * database manager, service, logger, and set them as protected.
 * <p>
 * 2. Some common tool methods had better lay in abstract class for sub class
 * invoke. E.g., parseData, getLong, str2Int etc.
 * <p>
 * 3. Make common flow of processing business logic. That can force sub class
 * follows the flow.
 * <p>
 * NOTE : Abstract class do not have to implements the abstract method in
 * interface.
 */
public abstract class AService implements IService {

    /**
     * [1]. Non-abstract sub class has to implements this method to supply real process.
     */
    //protected abstract String doProcess(String params);
    protected abstract String doProcess(IRequest request);

    /**
     * [2]. The method is a skeleton of processing request. Each sub class will invoke it.
     */
    @Override
    /**
     * NOTE : here we use the interface "IRequest", it's a flexible way to
     * handle more type request without changing our original business logic.
     */
    //public String process(String params) {
    public String process(IRequest request) {
        String result = null;
        try {
            // Check params passed in.
            checkParams(request);
            // Check user login.
            checkLogin(request);
            // Do other business work.
            doOtherBusiness(request);
            result = doProcess(request);
        } catch (Exception e) {
            // Record exception info.
            // log(e);
        } finally {
            notifyResult(result);
        }
        return result;
    }

    /**
     * [3]. The following three methods are the common flow for processing business logic.
     */
    //private boolean checkParams(String params) {
    private boolean checkParams(IRequest request) {
        System.out.println("Params is ok.");
        return true;
    }

    //private boolean checkLogin(String params) {
    private boolean checkLogin(IRequest request) {
        System.out.println("Login is ok.");
        return true;
    }

    //private boolean doOtherBusiness(String params) {
    private boolean doOtherBusiness(IRequest request) {
        System.out.println("Other business completed.");
        return true;
    }

    private void notifyResult(String result) {
        System.out.println("Service is completed : " + result + "\n");
    }

    /**
     * [4]. The following two methods are common methods for sub class to use for extra purpose.
     */
    protected IRequest handleOdrTypeRequest(IRequest odrRequest) {
        System.out.println("Odr request handle is completed.");
        return odrRequest;
    }

    protected IRequest handleMbrTypeRequest(IRequest mbrRequest) {
        System.out.println("Mbr request handle is completed.");
        return mbrRequest;
    }
}
