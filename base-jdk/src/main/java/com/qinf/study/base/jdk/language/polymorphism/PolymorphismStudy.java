package com.qinf.study.base.jdk.language.polymorphism;

public class PolymorphismStudy {

    public static void main(String[] args) {
        /**
         * The f_subSonType and f_subsubSonType can only invoke the
         * method(iSay()) declared in interface(Father), meanwhile, the method
         * have been implemented by Son.java.
         */
        Father f_subSonType = new SubSon();
        f_subSonType.iSay();
        System.out.println("**********************************");
        Father f_subsubSonType = new SubSubSon();
        f_subsubSonType.iSay();
        System.out.println("**********************************");

        /**
         * Generally, the son type can cast up to father type, but the father
         * type can not cast down to son type.
         */
        // 1. son type can cast up to father type(can remember it as : son can be father)
        Father f = new SubSon();
        f.iSay();

        // 2. father type CANNOT cast down to son type(can remember it as : father cannot be son)
        // NOTE : even through, the compile not throws exception, but a runtime
        // exception ClassCastException, definitely, will be threw out.
        SubSubSon subsubSon = (SubSubSon) new SubSon();
        subsubSon.iSay();
        subsubSon.iDo();
        subsubSon.iThink();
        subsubSon.iOk();
        subsubSon.sonMethod();
    }
}
