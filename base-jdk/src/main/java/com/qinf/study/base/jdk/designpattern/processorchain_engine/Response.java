package com.qinf.study.base.jdk.designpattern.processorchain_engine;

import lombok.Getter;

/**
 * Created by qinf on 2021-10-02.
 */
public interface Response {

    boolean isExitInstantly();

    public enum Result {
        SUCC("SUCC"),
        FAIL("FAIL");

        @Getter
        String code;

        Result(String code) {
            this.code = code;
        }
    }
}
