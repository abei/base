package com.qinf.study.base.jdk.designpattern.processorchain_engine;

import java.util.List;

/**
 * Created by qinf on 2021-10-02.
 */
public interface ProcessorChain<Q, P> {

    public static final boolean EXIT = true;

    void process(Q request,P response);

    void process(Q request, P response, Callback callback);

    void process(Q request, P response, Callback callback, Object... args);

    List<Processor> getProcessors();
}
