package com.qinf.study.base.jdk.language.collection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qinf on 2017/6/26 PM10:27:35
 */
public class ArrayListStudy {

    public static void main(String[] args) {
        List<String> arrayList = new ArrayList<String>();
        arrayList.add("H");
        arrayList.add("E");
        arrayList.add("L");
        arrayList.add("L");
        arrayList.add("O");

        /**
         * ArrayList keep the order of inserted.
         * Inside of ArrayList, it's a dynamic array to implement it.
         * Method of add(E e) is implemented as array[size++] = e;.
         * That is why ArrayList can keep the inserted order.
         */
        System.out.println(arrayList);
    }
}
