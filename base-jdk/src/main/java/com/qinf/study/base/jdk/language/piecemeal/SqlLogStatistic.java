package com.qinf.study.base.jdk.language.piecemeal;

import com.csvreader.CsvReader;
import org.apache.commons.lang3.StringUtils;

import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * // Read a record.
 * //System.out.println(reader.getRawRecord());
 * // Read a field by field name or its index.
 * //System.out.println(reader.get(0));
 * //System.out.println(reader.get(1));
 */
public class SqlLogStatistic {
    private static final String delimiter = ":";
    //private static List<String> sqlHeaders = Arrays.asList("UPDATE", "DELETE", "SELECT", "INSERT");
    private static List<String> sqlHeaders = Arrays.asList("SELECT");
    private static List<String> comparatorCharacters = Arrays.asList("<=", ">=", "<", ">", "=");
    private static String performCntAsKey = "performCntAsKey";
    private static String performCntPercAsKey = "performCntPercAsKey";
    private static String performTimeAvgAsKey = "performTimeAvgAsKey";
    private static String sumCostTimeAsKey = "sumCostTimeAsKey";
    private static String maxCostTimeAsKey = "maxCostTimeAsKey";
    private static String minCostTimeAsKey = "minCostTimeAsKey";
    private static String costTimePercAsKey = "costTimePercAsKey";
    private static String less3MonthCntAsKey = "less3MonthCntAsKey";
    private static String more3MonthCntAsKey = "more3MonthCntAsKey";

    private static long generateTimePointPast3Months() {
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        // Set 3 months before now.
        calendar.add(Calendar.MONTH, -3);
        return calendar.getTime().getTime();
    }

    private static boolean isBeyond3Months(String dataStr, long timePointPast3Months) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = format.parse(dataStr);
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        if (date.getTime() < timePointPast3Months)
            return true;
        return false;
    }

    public static boolean isValidDate(String str) {
        boolean convertSuccess = true;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            format.setLenient(true);
            format.parse(str);
        } catch (ParseException e) {
            convertSuccess = false;
        }
        return convertSuccess;
    }

    private static void crmSqlLogStatistic(String fPath) {
        long timePointPast3Months = generateTimePointPast3Months();
        CsvReader reader = null;
        double allPerformSum = 0;
        double allCostTimeSum = 0;
        int less3MonthCnt = 0;
        int more3MonthCnt = 0;
        // Data structure, store the last result.
        Map<String, Map<String, String>> infoAllSql = new HashMap<String, Map<String, String>>();

        try {
            reader = new CsvReader(fPath, ' ', Charset.forName("GBK"));
            // Read table header, then next read will skip the table header.
            reader.readHeaders();
            // NOTE : record = sqlContentInfo(sqlSelectInfoArr + sqlWhereInfoArr) + sqlPerformInfo
            while (reader.readRecord()) {
                String[] sqlContentInfo = reader.get(0).split("where");
                if (sqlContentInfo.length < 2)
                    continue;
                String[] sqlSelectInfoArr = Arrays.stream(sqlContentInfo[0].split("\\s+")).map(StringUtils::trim).toArray(String[]::new);
                String[] sqlWhereInfoArr = Arrays.stream(sqlContentInfo[1].split("\\s+")).map(StringUtils::trim).toArray(String[]::new);

                // Filter select sql and invalid sql.
                if (sqlSelectInfoArr.length < 2 || sqlWhereInfoArr.length < 1 || !sqlHeaders.contains(sqlSelectInfoArr[0].toUpperCase()))
                    continue;

                String[] sqlPerformInfo = reader.get(1).split("\t");
                if (sqlPerformInfo.length < 7)
                    continue;
                if (StringUtils.isNumeric(sqlPerformInfo[4])) {
                    // Evaluate the sum of cost time of all perform sql.
                    allCostTimeSum += Double.valueOf(sqlPerformInfo[4]).doubleValue();
                    // Count the sum of all perform sql.
                    allPerformSum++;
                }

                // Generate unique sql.
                String sqlSelectInfo = Arrays.stream(sqlSelectInfoArr).collect(Collectors.joining(delimiter));
                String sqlWhereInfo = Arrays.asList(sqlWhereInfoArr)
                        .stream()
                        .map(StringUtils::trim)
                        .filter(StringUtils::isNotEmpty)
                        .filter(e -> !e.contains(":")) // Eliminate time in where condition, e.g, 00:00:00.
                        .map(e -> {
                            String field = e;
                            if (e.contains(">=")) {
                                String[] t = Arrays.stream(e.split(">=")).map(StringUtils::trim).toArray(String[]::new);
                                if (t.length == 2)
                                    field = t[0] + ">=";
                            } else if (e.contains("<=")) {
                                String[] t = Arrays.stream(e.split("<=")).map(StringUtils::trim).toArray(String[]::new);
                                if (t.length == 2)
                                    field = t[0] + "<=";
                            } else if (e.contains(">")) {
                                String[] t = Arrays.stream(e.split(">")).map(StringUtils::trim).toArray(String[]::new);
                                if (t.length == 2)
                                    field = t[0] + ">";
                            } else if (e.contains("<")) {
                                String[] t = Arrays.stream(e.split("<")).map(StringUtils::trim).toArray(String[]::new);
                                if (t.length == 2)
                                    field = t[0] + "<";
                            } else if (e.contains("=")) {
                                String[] t = Arrays.stream(e.split("=")).map(StringUtils::trim).toArray(String[]::new);
                                if (t.length == 2)
                                    field = t[0] + "=";
                            }
                            return field;
                        })
                        .collect(Collectors.joining(delimiter));
                String sqlAsKey = sqlSelectInfo + delimiter + "where" + delimiter + sqlWhereInfo;
                if (sqlAsKey.contains("LIMIT"))
                    sqlAsKey = sqlAsKey.split("LIMIT")[0] + "LIMIT";

                // Parse select condition time beyond 3 months from now time point.
                List<String> whereTimes = Arrays.asList(sqlContentInfo[1].split("and | or | order | group"))
                        .stream()
                        .map(StringUtils::trim)
                        .map(e -> {
                            String field = "";
                            if (e.contains(">=")) {
                                String[] t = Arrays.stream(e.split(">=")).map(StringUtils::trim).map(s -> s.replaceAll("\'", "")).toArray(String[]::new);
                                if (t.length == 2 && isValidDate(t[1]))
                                    field = t[1];
                            } else if (e.contains("<=")) {
                                String[] t = Arrays.stream(e.split("<=")).map(StringUtils::trim).map(s -> s.replaceAll("\'", "")).toArray(String[]::new);
                                if (t.length == 2 && isValidDate(t[1]))
                                    field = t[1];
                            } else if (e.contains(">")) {
                                String[] t = Arrays.stream(e.split(">")).map(StringUtils::trim).map(s -> s.replaceAll("\'", "")).toArray(String[]::new);
                                if (t.length == 2 && isValidDate(t[1]))
                                    field = t[1];
                            } else if (e.contains("<")) {
                                String[] t = Arrays.stream(e.split("<")).map(StringUtils::trim).map(s -> s.replaceAll("\'", "")).toArray(String[]::new);
                                if (t.length == 2 && isValidDate(t[1]))
                                    field = t[1];
                            } else if (e.contains("=")) {
                                String[] t = Arrays.stream(e.split("=")).map(StringUtils::trim).map(s -> s.replaceAll("\'", "")).toArray(String[]::new);
                                if (t.length == 2 && isValidDate(t[1]))
                                    field = t[1];
                            }
                            return field;
                        })
                        .filter(StringUtils::isNotEmpty)
                        .collect(Collectors.toList());
                if (whereTimes.size() >= 1) {
                    int cnt = 0;
                    for (String time : whereTimes) {
                        if (isBeyond3Months(time, timePointPast3Months)) {
                            more3MonthCnt++;
                            break;
                        } else {
                            cnt++;
                        }
                    }
                    if (cnt == whereTimes.size())
                        less3MonthCnt++;
                }

                Map<String, String> infoPerSql = infoAllSql.get(sqlAsKey);
                if (Objects.isNull(infoPerSql) || infoPerSql.isEmpty()) {
                    infoPerSql = new HashMap<String, String>();
                    infoPerSql.put(performCntAsKey, Double.toString(1));
                    infoPerSql.put(performCntPercAsKey, StringUtils.EMPTY); // At last, then to evaluate it.
                    infoPerSql.put(performTimeAvgAsKey, StringUtils.EMPTY); // At last, then to evaluate it.
                    infoPerSql.put(sumCostTimeAsKey, sqlPerformInfo[4]);
                    infoPerSql.put(maxCostTimeAsKey, sqlPerformInfo[4]);
                    infoPerSql.put(minCostTimeAsKey, sqlPerformInfo[4]);
                    infoPerSql.put(costTimePercAsKey, StringUtils.EMPTY); // At last, then to evaluate it.
                    infoPerSql.put(less3MonthCntAsKey, Integer.toString(less3MonthCnt));
                    infoPerSql.put(more3MonthCntAsKey, Integer.toString(more3MonthCnt));
                    infoAllSql.put(sqlAsKey, infoPerSql);
                } else {
                    double performCnt = Double.parseDouble(infoPerSql.get(performCntAsKey)) + 1;
                    infoPerSql.put(performCntAsKey, Double.toString(performCnt));
                    double sumCostTime = Double.parseDouble(infoPerSql.get(sumCostTimeAsKey)) + Double.parseDouble(sqlPerformInfo[4]);
                    infoPerSql.put(sumCostTimeAsKey, Double.toString(sumCostTime));
                    double maxCostTime = Double.parseDouble(infoPerSql.get(maxCostTimeAsKey));
                    if (Double.parseDouble(sqlPerformInfo[4]) > maxCostTime)
                        infoPerSql.put(maxCostTimeAsKey, sqlPerformInfo[4]);
                    double minCostTime = Double.parseDouble(infoPerSql.get(minCostTimeAsKey));
                    if (Double.parseDouble(sqlPerformInfo[4]) < minCostTime)
                        infoPerSql.put(minCostTimeAsKey, sqlPerformInfo[4]);
                    int less3MCnt = Integer.parseInt(infoPerSql.get(less3MonthCntAsKey)) + less3MonthCnt;
                    infoPerSql.put(less3MonthCntAsKey, Integer.toString(less3MCnt));
                    int more3MCnt = Integer.parseInt(infoPerSql.get(more3MonthCntAsKey)) + more3MonthCnt;
                    infoPerSql.put(more3MonthCntAsKey, Integer.toString(more3MCnt));
                }

                // Reset corresponding data.
                less3MonthCnt = 0;
                more3MonthCnt = 0;
            }

            // Evaluate percent and avg.
            for (Map.Entry<String, Map<String, String>> entry : infoAllSql.entrySet()) {
                Map<String, String> map = entry.getValue();
                map.put(performCntPercAsKey, Double.toString(Double.parseDouble(map.get(performCntAsKey)) / allPerformSum));
                map.put(performTimeAvgAsKey, Double.toString(Double.parseDouble(map.get(performCntAsKey)) / Double.parseDouble(map.get(sumCostTimeAsKey))));
                map.put(costTimePercAsKey, Double.toString(Double.parseDouble(map.get(sumCostTimeAsKey)) / allCostTimeSum));
            }

            System.out.println("Sql语句" + "\t" + "执行时间百分比" + "\t" + "最小执行时间" + "\t" + "平均执行时间" + "\t" + "最大执行时间" + "\t" + "时间范围3个/以上" + "\t" + "执行次数" + "\t" + "时间范围3个/以内" + "\t" + "次数百分比");
            infoAllSql.forEach((k, v) -> {
                final StringBuilder record = new StringBuilder();
                //System.out.print(k + "\t");
                record.append(k);
                v.forEach((k1, v1) -> {
                    if (!Objects.equals(k1, sumCostTimeAsKey))
                        //System.out.print(k1 + " : " + v1 + "\t");
                        //System.out.print(v1 + "\t");
                        record.append("\t" + v1);
                });
                //System.out.print("\n");
                record.append("\n");
                System.out.print(record.toString());
            });
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (null != reader) {
                reader.close();
            }
        }
    }

    public static void main(String... args) {
        crmSqlLogStatistic("D:\\work\\dev work\\分库分表 - sql statistic\\crm - example.txt");
        //crmSqlLogStatistic("D:\\work\\dev work\\分库分表 - sql statistic\\crm_api_07-30_sql_log.csv");
    }
}
