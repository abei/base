package com.qinf.study.base.jdk.designpattern.observermechanism_callback;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

/**
 * Created by qinf on 2017/12/28 PM12:31:58
 * <p>
 * For me, the async callback mechanism includes 3 parts :
 * 1. A callback interface.
 * 2. Supply(Implement) callback method in invoker.
 * 3. Run task by background thread, when task completed to invoke callback method.
 */
public class DataProducer {
    private static final ExecutorService threadPool;

    static {
        threadPool = Executors.newCachedThreadPool(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r, "BACKGROUND-IO-THREAD");
                return t;
            }
        });
    }

    private NetworkSender networkSender;

    public DataProducer(NetworkSender networkSender) {
        this.networkSender = networkSender;
    }

    public static void main(String... strings) {
        DataProducer dataProducer = new DataProducer(new NetworkSender());
        /** You can use anonymous inner class to implement callback interface. */
        /*dataProducer.sendData("HELLO WORLD", new Callback() {
            @Override
            public void onCompletion(String result, Exception exception) {
                if (Objects.nonNull(exception))
                    System.out.println("The request encountered exception is : " + exception.getMessage());
                else
                System.out.println("The request completed and result is : " + result);
            }
        });
        */

        System.out.println("The message is sending...");
        /** You can also use lambda expression to implement callback interface. */
        dataProducer.sendData("HELLO WORLD.", (result, exception) -> {
            if (Objects.nonNull(exception))
                System.out.println("The request encountered exception is : " + exception.getMessage());
            else
                System.out.println("The request completed and result is : " + result);
        });
    }

    /**
     * It's a async method, and contains a callback.
     */
    public Future<String> sendData(String data, Callback callback) {
        return (Future<String>) threadPool.submit(() -> {
            networkSender.doSend(data, callback);
        });
    }
}
