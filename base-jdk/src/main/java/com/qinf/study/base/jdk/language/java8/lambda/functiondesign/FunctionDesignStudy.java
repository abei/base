package com.qinf.study.base.jdk.language.java8.lambda.functiondesign;

/**
 * Created by qinf on 2018/4/1 PM8:32:46
 */
public class FunctionDesignStudy {

    public static void main(String... strings) {
        MyInvoker invoker = new MyInvoker(true);
        Integer result = invoker.invoke("invoke", (String s) -> s.length());
        System.out.println("The result of invoking is : " + result);
    }
}
