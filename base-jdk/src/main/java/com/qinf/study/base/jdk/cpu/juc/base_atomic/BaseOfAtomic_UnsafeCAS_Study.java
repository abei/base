package com.qinf.study.base.jdk.cpu.juc.base_atomic;

/**
 * Created by qinf on 2017/12/13 PM10:30:42
 * <p>
 * CAS is the base of implementation of non-block lock.
 * <p>
 * CAS will be translated into a CPU instruction "cmpxchg" finally.
 * This CPU instruction cannot be interrupted by other thread when it is running.
 */
public class BaseOfAtomic_UnsafeCAS_Study {

}
