package com.qinf.study.base.jdk.language.java8.concurrent;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by qinf on 2017/12/3 PM9:24:03
 */
public class CompletableFutureComposeStudy {

    /**
     * Flow : for instance, there are 4 shops [TMall, Amazon, JD, WalMar]
     * [ threadPool_1 ]                                              [ threadPool_2 ]
     * thread-0[TMall]  : getPrice & parsePrice, use time 1s ======> thread-6[TMall]  : discntPrice, use time 1s
     * first CompletableFuture            ======> second CompletableFuture
     * thread-1[Amazon] : getPrice & parsePrice, use time 1s ======> thread-4[Amazon] : discntPrice, use time 1s
     * first CompletableFuture            ======> second CompletableFuture
     * thread-2[JD]     : getPrice & parsePrice, use time 1s ======> thread-5[JD]     : discntPrice, use time 1s
     * first CompletableFuture            ======> second CompletableFuture
     * thread-3[WalMar] : getPrice & parsePrice, use time 1s ======> thread-7[WalMar] : discntPrice, use time 1s
     * first CompletableFuture            ======> second CompletableFuture
     * <p>
     * Total use time is 2s.
     * <p>
     * NOTE 1 : As the second CompletableFuture depends on the first CompletableFuture,
     * so if you want get the last result, thread will take 2s, thread-0[TMall]==>thread-6[TMall].
     * NOTE 2 : thread-0[TMall]  ==> thread-6[TMall]  : run at the same time
     * thread-1[Amazon] ==> thread-4[Amazon] : run at the same time
     * thread-2[JD]     ==> thread-5[JD]     : run at the same time
     * thread-3[WalMar] ==> thread-7[WalMar] : run at the same time
     */
    public static List<String> findDiscountedPricesFromAllShops(List<Shop> allShops, String productName, Executor powerExecutor1) {
        final Executor powerExecutor2 = Executors.newFixedThreadPool(20, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            }
        });
        /**
         * The result is String type, so the generic will be CompletableFuture<String>.
         * The code will not block invoker, because it submit tasks to thread pool.
         */
        List<CompletableFuture<String>> cmplFutureDiscountedPrices = allShops.stream()
                /** First time supply asynchronous thread to handle long-time work */
                .map(shop -> CompletableFuture.supplyAsync(() -> shop.getProductPriceInfo(productName), powerExecutor1))
                .map(complFuture_priceInfoInString -> complFuture_priceInfoInString.thenApply(DTO::parsePriceInfo)) // thenApplyAsync(fn)
                .map(complFuture_priceInfoInDTO -> complFuture_priceInfoInDTO.thenCompose( // thenCombineAsync(other, fn)
                        /** Second time supply asynchronous thread to handle long-time work, depends on first time result */
                        //dto -> CompletableFuture.supplyAsync(() -> DiscountService.applyDiscount(dto), powerExecutor1)))
                        dto -> CompletableFuture.supplyAsync(() -> DiscountService.applyDiscount(dto), powerExecutor2)))
                .collect(Collectors.toList());
        /**
         * 1. CompletableFuture.join() will block thread until obtain the result, as like Future.get().
         * 2. The code will block invoker, because it will be blocked until get all results from CompletableFuture.
         * 	  That is to say invoker will be blocked until 4 CompletableFuture completed.
         */
        return cmplFutureDiscountedPrices.stream().map(CompletableFuture::join).collect(Collectors.toList());
    }

    public static void main(String... strings) {
        List<Shop> allShops = Arrays
                .asList(new Shop[]{new Shop("TMall"), new Shop("Amazon"), new Shop("JD"), new Shop("WalMar")});
        final Executor executor = Executors.newFixedThreadPool(Math.min(allShops.size(), 50), new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            }
        });

        long StartTime = System.currentTimeMillis();
        List<String> allPrices = findDiscountedPricesFromAllShops(allShops, "IPhone1000", executor);
        long EndTime = System.currentTimeMillis();

        System.out.println("Use time : " + (EndTime - StartTime) / 1000 + " seconds");
        allPrices.stream().forEach(System.out::println);
    }

    private enum DiscountCode {
        NONE(0), SILVER(5), GOLD(10), PLATINUM(15), DIAMOND(20);
        final int percentage;

        DiscountCode(int percentage) {
            this.percentage = percentage;
        }
    }

    static class DTO {
        private final String shopName;
        private final long productPrice;
        private final DiscountCode discntCode;

        public DTO(String shopName, long productPrice, DiscountCode discntCode) {
            this.shopName = shopName;
            this.productPrice = productPrice;
            this.discntCode = discntCode;
        }

        public static DTO parsePriceInfo(String priceInfo) {
            String[] splits = (String[]) Stream.of(priceInfo.split(":"))
                    .map(String::trim)
                    .toArray(String[]::new);
            return new DTO(splits[0],
                    Long.parseLong(splits[1]),
                    DiscountCode.valueOf(splits[2]));
        }

        public String getShopName() {
            return shopName;
        }

        public long getProductPrice() {
            return productPrice;
        }

        public DiscountCode getDiscntCode() {
            return discntCode;
        }
    }

    static class Shop {
        private String shopName;

        Shop(String shopName) {
            this.shopName = shopName;
        }

        public String getShopName() {
            return this.shopName;
        }

        /**
         * consume time 1s
         */
        public String getProductPriceInfo(String productName) {
            try {
                Thread.sleep(1_000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            long productPrice = new Random().nextLong() * productName.charAt(0) + productName.charAt(1);
            DiscountCode discntCode = DiscountCode.values()[new Random().nextInt(DiscountCode.values().length)];
            //return String.format("s% : %s : %s", shopName, productPrice, discntCode);
            return shopName + ":" + productPrice + ":" + discntCode;
        }
    }

    static class DiscountService {
        /**
         * consume time 1s
         */
        public static String applyDiscount(DTO dto) {
            try {
                Thread.sleep(1_000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return String.valueOf(dto.getProductPrice() * (100 - dto.getDiscntCode().percentage) / 100);
        }
    }
}
