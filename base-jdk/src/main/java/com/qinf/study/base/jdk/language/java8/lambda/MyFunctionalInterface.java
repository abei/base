package com.qinf.study.base.jdk.language.java8.lambda;

import java.util.Map;

/**
 * Created by qinf on 2017/11/25 PM9:41:11
 * <p>
 * Functional interface definition :
 * 1. Functional Interface
 * If a interface want to be functional interface, it just only contains a abstract method.
 * <p>
 * 2. Method Descriptor
 * Functional Interface used to describe the signature of Lambda Expression by its abstract method.
 * For example, the type of (argument and return) of the abstract method should match Lambda.
 * <p>
 * In java8, under java.util.function there are several functional interface:
 * Predicate / Consumer / Function etc, these Functional Interface supply frequently-used and different
 * method descriptors for Lambda Expression.
 */
@FunctionalInterface
/*public interface MyFunctionalInterface<A, R> {
    R mapSize(A a) throws Exception;
}*/
public interface MyFunctionalInterface {
    int mapSize(Map<?, ?> map) throws Exception;

    default void printEachEle(Map<?, ?> map) {
        map.forEach((k, v) -> {System.out.println(k + ":" + v);});
    }

    /*default boolean equals(Object o) {
        o.getClass();
        return false;
    }*/

    boolean equals(Object o);
}
