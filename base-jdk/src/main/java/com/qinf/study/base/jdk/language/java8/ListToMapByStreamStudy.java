package com.qinf.study.base.jdk.language.java8;

import java.util.*;

/**
 * Created by qinf on 2018/4/6 PM9:09:23
 */
public class ListToMapByStreamStudy {

    public static void main(String... strings) {
        Vector<String> vector = new Vector<String>();
        vector.add("QIN");
        vector.add("FEN");
        vector.add("NAN");
        Enumeration<String> enume = vector.elements();

        /** NOTE : you cannot convert list to stream by Stream.of(list). */
        Map<String, String> map = Collections.list(enume).stream()
                .filter(e -> !Objects.equals(e, "QIN"))
                .collect(HashMap::new, (m, k) -> m.put(k, k + "-V"), HashMap::putAll);

        map.forEach((k, v) -> System.out.println(k + " : " + v));
    }
}
