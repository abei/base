package com.qinf.study.base.jdk.language.encode;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * Created by qinf on 2021-07-26.<br>
 *
 * Codepoint is a numeric can be represented in Decimal, Hex or Binary.
 * A codepoint represent a char in charset of a specific encoding specification.
 * In brief, you can consider charset as a map, char and codepoint can be a entry(k-v)
 * of the map.
 * And so called encoding is to encode the codepoint into Binary or other system.
 * E.g., in Unicode encoding specification, the binary codepoint of char "阿" is 1001 011000 111111,
 * then the binary codepoint can be encoded into 11101001 10011000 10111111(UTF-8 code) by UTF-8(algorithm).
 */
public class CodePointStudy {

    public static void main(String... strings) throws UnsupportedEncodingException {
        /**
         * The java file encoding is utf-8 set by idea(Editor -> File Encodings),
         * so the below string in the source file will encoded by utf-8.
         */
        String string = "阿贝";

        // console: Char: 阿 | 贝
        System.out.println("Char: " + string.charAt(0) + " | " + string.charAt(1));

        // console: Codepoint in dec: 38463 | 36125
        System.out.println("Codepoint in dec: " + string.codePointAt(0) + " | " + string.codePointAt(1));
        // console: Codepoint in hex: 963f | 8d1d
        System.out.println("Codepoint in hex: " + Integer.toHexString(string.codePointAt(0)) + " | " + Integer.toHexString(string.codePointAt(1)));
        // console: Codepoint in bin: 1001011000111111 | 1000110100011101
        System.out.println("Codepoint in bin: " + Integer.toBinaryString(string.codePointAt(0)) + " | " + Integer.toBinaryString(string.codePointAt(1)));
        System.out.println("UTF-8 fill template: " + "1110 xxxx 10 xxxxxx 10 xxxxxx");
        System.out.println("UTF-8 code in bin: " + "11101001 10011000 10111111 11101000 10110100 10011101");
        System.out.println("UTF-8 code in dec: " + "-23, -104, -65, -24, -76, -99");
        System.out.println("UTF-8 code in hex: " + "E9 98 BF E8 B4 9D" + "\n");

        /**
         * Encode char by specific encoding mode utf-8.
         * 1) console is: [-23, -104, -65, -24, -76, -99], note here a byte range in decimal is -128 ~ 127
         * 2) console is: ffffffe9 ffffff98 ffffffbf ffffffe8 ffffffb4 ffffff9d
         * 3) a chinese char will hold 3 bytes in utf-8
         */
        byte[] bytesByUtf_8 = string.getBytes("UTF-8");
        System.out.println("Encode chars to bytes(dec) by utf-8: " + Arrays.toString(bytesByUtf_8));
        System.out.println("Encode chars to bytes(hex) by utf-8: " + Integer.toHexString(-23) + " "
                + Integer.toHexString(-104) + " "
                + Integer.toHexString(-65) + " "
                + Integer.toHexString(-24) + " "
                + Integer.toHexString(-76) + " "
                + Integer.toHexString(-99));
        System.out.println("Decode bytes to chars by utf-8: " + new String(bytesByUtf_8, "UTF-8") + "\n");

        /**
         * Encode char by default encoding mode, the default encoding mode depends on your idea,
         * here is utf-8(Charset.defaultCharset()).
         * 1) console is: [-23, -104, -65, -24, -76, -99]
         */
        byte[] bytesByDefault = string.getBytes();
        System.out.println("Encode chars to bytes(hex) by default: " + Arrays.toString(bytesByDefault) + "\n");

        /**
         * Encode char by specific encoding mode gbk(ansi).
         * 1) console is: [-80, -94, -79, -76]
         * 2) a chinese char will hold 2 bytes in gbk
         */
        byte[] bytesByGbk = string.getBytes("GBK");
        System.out.println("Encode chars to bytes(hex) by gbk: " + Arrays.toString(bytesByGbk));
        System.out.println("Decode bytes to chars by gbk: " + new String(bytesByGbk, "GBK"));
        System.out.println("Decode bytes to chars by utf-8: " + new String(bytesByGbk, "UTF-8"));
    }
}
