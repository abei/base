package com.qinf.study.base.jdk.cpu.juc.ext_synctool;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * Created by qinf on 2017/12/5 PM9:50:05
 * <p>
 * Sync tool "CountDownLatch" implemented by shared lock.
 * <p>
 * Every thread wants to get the shared lock will be blocked until a other
 * thread invoked unlock.
 * CountDownLatch is one-off barrier, if you want block thread in a loop way,
 * CyclicBarrier is suitable.
 */
public class ExtSharedLock_CountDownLatchStudy {

    private final Sync sync = new Sync();

    public void sharedLock() { // equals to CountDownLatch.await();
        sync.acquireShared(1);
    }

    public void unlock() { // equals to CountDownLatch.countDown();
        sync.releaseShared(1);
    }

    /**
     * (1). The inner implementation of sync tool CountDownLatch
     */
    private static class Sync/*equals to CountDownLatch.class*/ extends AbstractQueuedSynchronizer {
        private static final long serialVersionUID = 1L;

        protected Sync() {
            /**
             * Here, set the state of shared lock is "1". That will block all
             * threads that try to acquire this shared lock. This is because if
             * state==1 means the shared lock has been acquired by other thread.
             */
            setState(1);
        }

        @Override
        protected int tryAcquireShared(int ignore) {
            /**
             * If return positive value, means the shared lock has been got by current thread.
             * If return 0, means the shared lock has not yet been got by current thread.
             * If return negative value, means the current thread failed to get the shared lock, it should be blocked.
             */
            return getState() == 0 ? 1 : -1;
        }

        @Override
        protected boolean tryReleaseShared(int ignore) {
            /**
             * The CAS state operation is included in for statement. That can
             * make sure all threads blocking on the shared lock wakes up.
             * This is so-called "non-blocked spin lock".
             */
            for (; ; ) {
                if (compareAndSetState(1, 0))
                    return true;
            }
        }
    }

    public static void main(String... strings) throws InterruptedException {
        /** (2). The example of sync tool CountDownLatch in JUC */
        ExecutorService threadPool = Executors.newCachedThreadPool();
        final CountDownLatch cntDownLatch = new CountDownLatch(3);

        System.out.println("Main-Thread blocked until other threads complete their tasks.\n");
        for (int i = 0; i < 3; i++)
            threadPool.execute(new Thread("Thread-" + i) {
                @Override
                public void run() {
                    try {
                        TimeUnit.MILLISECONDS.sleep(300);
                        System.out.println(Thread.currentThread().getName() + " is executing task.");
                        TimeUnit.SECONDS.sleep(5);
                        System.out.println(Thread.currentThread().getName() + " has completed task.");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    cntDownLatch.countDown();
                }
            });
        cntDownLatch.await();
        System.out.println("All threads have completed their tasks.");

        System.out.println("\nMain-Thread start execute its task, shutdown thread pool.");
        threadPool.shutdown();
        threadPool.awaitTermination(2, TimeUnit.SECONDS);
    }
}
