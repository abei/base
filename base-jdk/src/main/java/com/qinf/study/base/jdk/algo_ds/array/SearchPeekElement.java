package com.qinf.study.base.jdk.algo_ds.array;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qinf on 2020-11-17.
 */
public class SearchPeekElement {

    private static int getIndexOfPeekElement(int[] nums) {
        List<Integer> indices = new ArrayList<>(5);
        int h, p, t;
        h = 0;
        p = 0;
        t = 0;
        if (nums.length == 1) {
            return 0;
        } else if (nums.length == 2) {
            return nums[0] > nums[1] ? 0 : 1;
        }
        while (p < nums.length - 1) {
            if (nums[p] > nums[h] && nums[p] > nums[t]) {
                //System.out.println("The index of a peek element is: " + p);
                //return p;
                indices.add(p);
            }
            p++;
            h = p - 1;
            t = p + 1;
        }
        return indices.stream().findAny().get();
    }

    public static void main(String... strings) {
        //System.out.println(getIndexOfPeekElement(new int[]{1, 2, 1, 3, 5, 6, 4}));
        //System.out.println(getIndexOfPeekElement(new int[]{1, 2, 3, 1, 5, 3, 2, 6, 8, 44, 22, 45, 90}));
        //System.out.println(getIndexOfPeekElement(new int[]{1}));
        //System.out.println(getIndexOfPeekElement(new int[]{2, 1}));
        System.out.println(getIndexOfPeekElement(new int[]{3, 2, 1}));
    }
}
