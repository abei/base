package com.qinf.study.base.jdk.language.annotation;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by qinf on 2018-01-30.
 */
public class OrderDAO {

    @Statement(
            statementName = "getOrderInfo",
            returnParams = {
                    "orderId",
                    "orderDate",
                    "orderDetail"
            })
    public Map<?, ?> getOrderInfo(
            @StatementParam("orderName") String orderName,
            @StatementParam("orderType") String orderType) {
        // TODO database query.
        return new HashMap<String, String>();
    }
}
