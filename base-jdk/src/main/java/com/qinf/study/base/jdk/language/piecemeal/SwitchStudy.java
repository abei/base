package com.qinf.study.base.jdk.language.piecemeal;

/**
 * Created by qinf on 2018/09/04
 */
public class SwitchStudy {

    public static void main(String... args) {
        String type = "WORLD";

        switch (type) {
            case "HELLO":
                System.out.println("HELLO");
                break;
            case "WORLD":
                System.out.println("WORLD");
                break;
            default:
                System.out.println("DEFAULT");
        }
    }
}
