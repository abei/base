package com.qinf.study.base.jdk.designpattern.factory.abstractfactory;

/**
 * Created by qinf on 2017/7/25 PM10:04:22
 */
public class NANRoomFactory implements RoomFactory {

    Room nan_BathRoom;
    Room nan_BedRoom;

    @Override
    public Room createBedRoom() {
        nan_BedRoom = new NAN_BedRoom();
        return nan_BedRoom;
    }

    @Override
    public Room createBathRoom() {
        nan_BathRoom = new NAN_BathRoom();
        return nan_BathRoom;
    }

}
