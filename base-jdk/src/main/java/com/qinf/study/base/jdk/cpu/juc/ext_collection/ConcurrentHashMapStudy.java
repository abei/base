package com.qinf.study.base.jdk.cpu.juc.ext_collection;

/**
 * Created by qinf on 2018/1/7 PM3:20:43
 *
 * ConcurrentHashMap allows concurrent modification(insert or update) of the Map from
 * several threads without the need to block them, but cannot ensure consistency.
 *
 * ConcurrentHashMap can guarantee that there is no ConcurrentModificationException thrown
 * while one thread is updating the map and another thread is traversing the iterator
 * obtained from the map.
 *
 * ConcurrentHashMap can ensure thread always get the latest date. E.g., suppose there
 * are multiple threads, some of them are updating the Map and some of them are getting
 * data from that same map. So in this scenario when threads are trying to read, it is
 * guaranteed that they will get the latest data that has been updated since reader threads
 * does not have to hold locks.
 *
 * If there are many update operations and relative small amount of read operations, you
 * should choose ConcurrentHashMap
 */
public class ConcurrentHashMapStudy {

}
