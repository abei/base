package com.qinf.study.base.jdk.language.enum_;

public enum EnumBaseType {
    RED("I am real red."),
    BLUE("I am real blue.");

    private String color;

    EnumBaseType(String color) {
        this.color = color;
    }

    public String getColor() {
        return color; // this.color;
    }
}
