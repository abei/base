package com.qinf.study.base.jdk.language.java8;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by qinf on 2018/8/5 PM9:09:23
 */
public class TraversalEnumerationByStreamStudy {
    public static void main(String... args) {
        Hashtable hashTable = new Hashtable();
        hashTable.put("1", "One");
        hashTable.put("2", "Two");
        hashTable.put("3", "Three");
        Enumeration enumeration = hashTable.elements();

        Collections.list(enumeration).stream().forEach(System.out::println);
    }
}
