package com.qinf.study.base.jdk.cpu.juc.base_atomic;

/**
 * Created by qinf on 2017/12/17 PM1:09:46
 * <p>
 * Volatile variable can be visible to multiple threads, but can not guarantee atomicity
 * for modification by multiple threads.
 * <p>
 * volatile i ++ is not thread-safe.
 */
public class BaseOfAtomic_Volatile_Study {

}
