package com.qinf.study.base.jdk.cpu.juc.base_lock;

/**
 * Created by qinf on 2017/12/17 PM1:26:28
 * <p>
 * So-called spin lock as like following code :
 * for (; ;) or while {
 * if (T.compareAndSet(expect value, actual value)) {
 * do something;
 * return;
 * }
 * }
 */
public class SpinLockStudy {

}
