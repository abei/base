package com.qinf.study.base.jdk.cpu.juc.base_lock;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by qinf on 2017/12/14 PM9:42:25
 * <p>
 * 1. What is ABA problem?
 * A thread don't know a variable whether be modified by other thread just through
 * to judge its value.
 * ABA problem always occurs in concurrent Stack or concurrent List.
 * Deeply caused by spin lock under concurrent scenario.
 * <p>
 * 2. How to solve ABA problem?
 * You can solve ABA problem by AtomicStampedReference. AtomicStampedReference adds a
 * version to a specified variable.
 * When a thread wants to change the variable, it can check the variable's value and
 * variable's version, if both of them are not modified, can think the variable never
 * be changed by other thread.
 */
public class SpinLockOfABAIssueStudy {

    public static void main(String... strings) throws InterruptedException {
        MyStack<String> stack = new MyStack<String>();
        stack.push("B");
        stack.push("A");
        System.out.println("Initialized stack is : " + stack.toString());

        ExecutorService threadPool = Executors.newCachedThreadPool();
        threadPool.execute(() -> {
            System.out.println("Thread-01 is : " + Thread.currentThread().getState());
            Thread.currentThread().setName("Thread-01");
            Node<String> node = stack.pop();
            System.out.println("\nThread-01 pop a node : " + node.getValue());
            System.out.println("Operated by Thread-01 stack is : " + stack.toString());
        });

        threadPool.execute(() -> {
            System.out.println("Thread-02 is : " + Thread.currentThread().getState());
            Thread.currentThread().setName("Thread-02");
            Node<String> originalNodeA = stack.pop();
            stack.pop();
            stack.push("D");
            stack.push("C");
            /**
             * NOTE 2 : At this time, you should be also clear that originalA contains the reference to node B by its field "next".
             *
             * Now change node A back, to cause ABA problem occurred.
             */
            stack.push(originalNodeA);
            System.out.println("\nOperated by Thread-02 stack is  : " + stack.toString());
        });

        threadPool.shutdown();
        threadPool.awaitTermination(1, TimeUnit.MINUTES);
    }

    private static class Node<T> {
        private T value;
        private Node<T> next;

        public Node(T value) {
            this.value = value;
        }

        public Node<T> getNext() {
            return next;
        }

        public void setNext(Node<T> next) {
            this.next = next;
        }

        public T getValue() {
            return value;
        }
    }

    static class MyStack<U> {
        /**
         * Top always point to the latest node pushed into stack.
         * All operation[push & pop] for this stack is through top, so that can make sure atomic.
         */
        AtomicReference<Node<U>> top = new AtomicReference<>(null);

        public void push(U value) {
            Node<U> node = new Node<>(value);
            push(node);
        }

        void push(Node<U> node) {
            for (; ; ) {
                Node<U> tmpTop = top.get();
                if (top.compareAndSet(tmpTop, node)) {
                    node.setNext(tmpTop);
                    return;
                }
            }
        }

        public Node<U> pop() {
            for (; ; ) {
                Node<U> node = top.get();
                if (Objects.isNull(node))
                    return null;
                Node<U> nextNode = node.getNext();
                /**
                 * NOTE 1 : At this time, you should be clear that the nextNode is node B.
                 *
                 * Thread-01 sleep, can make sure before Thread-01 actually pop node A, other
                 * thread can get a chance to change node A and then change back. So Thread-01
                 * will think node A never be changed.
                 */
                if (Objects.equals(Thread.currentThread().getName(), "Thread-01"))
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                if (top.compareAndSet(node, nextNode))
                    return node;
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("[");
            Node<U> node = top.get();
            while (Objects.nonNull(node)) {
                sb.append(node.getValue());
                if (Objects.nonNull(node.getNext()))
                    sb.append(", ");
                node = node.getNext();
            }
            sb.append("]");
            return sb.toString();
        }
    }
}
