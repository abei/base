package com.qinf.study.base.jdk.language.reflectproxy;

/**
 * Created by qinf on 2017/7/6 PM9:14:09
 */
public class JDK_Subject01Impl implements JDK_Subject {

    public String performBusinessLogic() {
        System.out.println("We are handling orders......");
        return "Success";
    }
}
