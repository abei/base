package com.qinf.study.base.jdk.language.generics;

/**
 * Generic class or interface used to implement constraint on method signature.
 * <p>
 * TODO method-chaining to provide an easy way to configure the {@link AbstractBootstrap}
 */
public class GenericClassStudy {

    private static class GenericClass<T, U> {
        // NOTE : the generic type can not declare static member arguments
        private T vlu1;
        private U vlu2;

        public GenericClass(T vlu1, U vlu2) {
            this.vlu1 = vlu1;
            this.vlu2 = vlu2;
        }

        public T getVlu1() {
            return vlu1;
        }

        public void setVlu1(T vlu1) {
            this.vlu1 = vlu1;
        }

        public U getVlu2() {
            return vlu2;
        }

        public void setVlu2(U vlu2) {
            this.vlu2 = vlu2;
        }
    }

    public static void main(String[] args) {
        GenericClass<String, String> gClsString = new GenericClass<String, String>("VALUE1", "VALUE2");
        System.out.println(gClsString.getVlu1());
        System.out.println(gClsString.vlu1);

        GenericClass<Integer, Integer> gClsInteger = new GenericClass<Integer, Integer>(new Integer(1), new Integer(2));
        System.out.println(gClsInteger.getVlu2());

        // NOTE : can not use the primitive type
        //GenericClass<int, int> gClsInt = new GenericClass<int, int>(3, 4);
    }
}
