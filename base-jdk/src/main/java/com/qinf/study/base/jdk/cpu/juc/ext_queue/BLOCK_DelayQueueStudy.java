package com.qinf.study.base.jdk.cpu.juc.ext_queue;

/**
 * Created by qinf on 2017/12/5 PM10:30:42
 * <p>
 * NOTE 1 : Its elements will be ordered by Comparator(or natural).
 * NOTE 2 : There is no limit for its capacity, so put() wont be blocked, but take() will be blocked when queue is empty.
 * NOTE 3 : Internal implementation is by PriorityBlockingQueue.
 */
public class BLOCK_DelayQueueStudy {

}
