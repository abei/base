package com.qinf.study.base.jdk.jvm.constantpool;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qinf on 2021-01-23.
 */
public class StaticFieldOfClassSelfStudy {

    @Data
    static class InspectOrderSplitMessage {
        public final static InspectOrderSplitMessage empty = new InspectOrderSplitMessage();
        private String workOrderId;
        private Integer carId;
        private Integer inspectorId;
        private List<ChildInspectOrder> workOrder = new ArrayList<>(1);

        @Data
        static class ChildInspectOrder {
            private String childWorkOrderId;
            private Integer carId;
            private Integer inspectorId;
        }
    }

    public static void main(String... strings) {
        InspectOrderSplitMessage instance1 = new InspectOrderSplitMessage();
        InspectOrderSplitMessage innerEmpty1 = InspectOrderSplitMessage.empty;
        InspectOrderSplitMessage instance2 = new InspectOrderSplitMessage();
        InspectOrderSplitMessage innerEmpty2 = InspectOrderSplitMessage.empty;
        System.out.println("");
    }
}
