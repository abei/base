package com.qinf.study.base.jdk.language.java8.concurrent;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Created by qinf on 2020-06-23.
 */
public class CompletableFutureExceptionStudy {

    private static void byExceptionally() throws ExecutionException, InterruptedException {
        CompletableFuture<Void> future = CompletableFuture
                .runAsync(() -> {
                    System.out.println("[byExceptionally] - Task executed in: " + Thread.currentThread().getName());
                    try {
                        TimeUnit.SECONDS.sleep(2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    throw new RuntimeException("[byExceptionally] - Test exception!");
                })
                .exceptionally(ex -> {
                    if (Objects.nonNull(ex)) {
                        System.out.println("[byExceptionally] - Catch exception executed in: " + Thread.currentThread().getName());
                        System.out.println("[byExceptionally] - The caught exception is: " + ex.getMessage());
                    }
                    return null;
                });
        Void execResult = future.get();
        System.out.println("[byExceptionally] - The exec result is: " + execResult);
    }

    private static void byHandle(final String param) throws ExecutionException, InterruptedException {
        CompletableFuture<String> future = CompletableFuture
                .supplyAsync(() -> {
                    System.out.println("[byHandle] - Task executed in: " + Thread.currentThread().getName());
                    try {
                        TimeUnit.SECONDS.sleep(2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (param.isEmpty()) {
                        throw new RuntimeException("[byHandle] - Param can not be empty!");
                    }
                    return param;
                })/*.thenAccept(res -> {
                    System.out.println("[byHandle] - The last res is: " + res);
                })*/
                .handle((res, ex) -> {
                    if (Objects.nonNull(ex)) {
                        System.out.println("[byHandle] - Catch exception executed in: " + Thread.currentThread().getName());
                        System.out.println("[byHandle] - The caught exception is: " + ex.getMessage());
                    }
                    return res;
                });
        String execResult = future.get();
        System.out.println("[byHandle] - The exec result is: " + execResult);
    }

    public static void main(String... strings) throws ExecutionException, InterruptedException {
        byExceptionally();
        //byHandle("");
        byHandle("all done");
    }
}
