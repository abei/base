package com.qinf.study.base.jdk.designpattern.processorchain_proxy;

import java.util.function.BiFunction;

/**
 * Created by qinf on 2020-10-06.
 */
public interface Processor extends Comparable {

    void process(Integer orderId);

    void process(Integer orderId, BiFunction callback);
}
