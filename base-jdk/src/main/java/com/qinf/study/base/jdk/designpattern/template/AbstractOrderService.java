package com.qinf.study.base.jdk.designpattern.template;

/**
 * Created by qinf on 2021-09-13.
 */
public abstract class AbstractOrderService implements OrderService {

    //public abstract Boolean upsertOrder(String orderEntity);

    @Override
    public Boolean upsertOrder(String orderEntity) {
        System.out.println("Common upsert order service: " + orderEntity);
        return Boolean.TRUE;
    }

    @Override
    public Boolean delegateUpsertOrder(String orderEntity, String extraOperation) {
        System.out.println("Common delegate upsert order service: " + orderEntity);
        this.upsertOrder(orderEntity);
        return Boolean.TRUE;
    }
}
