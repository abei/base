package com.qinf.study.base.jdk.language.polymorphism.override;

public class Son extends Father {
    protected String str3;

    public Son(String str) {
        this.str3 = str;
    }

    public String getStr3() {
        return str3;
    }

    public void setStr3(String str) {
        this.str3 = str;
    }

    @Override
    public String getStr2() {
        return "override father's getStr2 method";
    }

}
