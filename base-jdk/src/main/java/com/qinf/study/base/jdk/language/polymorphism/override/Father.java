package com.qinf.study.base.jdk.language.polymorphism.override;

public class Father {
    protected String str1;
    protected String str2;

    public String getStr1() {
        return str1;
    }

    public void setStr1(String str) {
        this.str1 = str;
    }

    public String getStr2() {
        return str2;
    }

    public void setStr2(String str) {
        this.str2 = str;
    }
}
