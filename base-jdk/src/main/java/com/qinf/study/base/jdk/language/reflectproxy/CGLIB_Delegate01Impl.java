package com.qinf.study.base.jdk.language.reflectproxy;

/**
 * Created by qinf on 2017/7/12 PM9:46:50
 */
public class CGLIB_Delegate01Impl {

    public String performBusinessLogic() {
        System.out.println("We are handling orders......");
        return "Success";
    }
}
