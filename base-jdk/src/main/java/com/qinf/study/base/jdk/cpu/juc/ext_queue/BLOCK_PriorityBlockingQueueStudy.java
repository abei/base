package com.qinf.study.base.jdk.cpu.juc.ext_queue;

/**
 * Created by qinf on 2017/12/5 PM10:30:42
 * <p>
 * Used for recycling some timeout object.
 * <p>
 * 1. Some non-timeout objects are order in DelayQueue.
 * 2. take() will blocked until a object is timeout in the front of DelayQueue.
 * poll() just return null when there is not timeout object.
 */
public class BLOCK_PriorityBlockingQueueStudy {

}
