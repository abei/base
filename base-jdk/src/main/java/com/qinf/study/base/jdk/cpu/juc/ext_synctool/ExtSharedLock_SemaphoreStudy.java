package com.qinf.study.base.jdk.cpu.juc.ext_synctool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * Created by qinf on 2017/12/5 PM9:51:31
 */
public class ExtSharedLock_SemaphoreStudy {

    private final Sync sync;

    public ExtSharedLock_SemaphoreStudy(int permits) {
        sync = new Sync(permits);
    }

    public static void main(String... strings) throws InterruptedException {
        /** (2). The example of my own sync tool Semaphore - "sharedLock"
         * Thread-02 is acquiring permit of the sharedLock...
         * Thread-01 is acquiring permit of the sharedLock...
         * Thread-04 is acquiring permit of the sharedLock...
         * Thread-03 is acquiring permit of the sharedLock...
         * Thread-02 has acquired a permit and remaining permits is : 0 (IO is slower than acquire lock, so print remaining is 0)
         * Thread-01 has acquired a permit and remaining permits is : 0
         * Thread-02 has completed its work.
         * Thread-01 has completed its work.
         * Main-Thread released a permit of the sharedLock and remaining permits is : 0
         * Thread-04 has acquired a permit and remaining permits is : 0
         * Thread-04 has completed its work.
         *
         * NOTE : According to the above result, "Thread-03" will not be executed forever,
         * 		  this is because of not other thread release permit any more.
         */
        /* The shared lock contains 2 permits, means 2 threads can get the lock at the same time(each thread gets a permit). */
        //final ExtSharedLock_SemaphoreStudy sharedLock = new ExtSharedLock_SemaphoreStudy(2);
        //MyThread t1 = new MyThread("Thread-01", sharedLock);
        //MyThread t2 = new MyThread("Thread-02", sharedLock);
        //MyThread t3 = new MyThread("Thread-03", sharedLock);
        //MyThread t4 = new MyThread("Thread-04", sharedLock);
        //t1.start(); t2.start(); t3.start(); t4.start();
        //TimeUnit.SECONDS.sleep(5);
        //System.out.println("\nMain-Thread released a permit of the sharedLock and remaining permits is : " + sharedLock.getPermits());
        /* If a thread released a permit of the shared lock, then other thread can get the permit again. */
        //sharedLock.unlock();
        //t1.join(); t2.join(); t3.join(); t4.join();
        //System.out.println("Main-Thread exit.");

        /** (3). The example of sync tool Semaphore in JUC */
        ExecutorService threadPool = Executors.newCachedThreadPool();
        final Semaphore semaphore = new Semaphore(2);

        for (int i = 0; i < 5; i++)
            threadPool.execute(new Thread("Thread-" + i) {
                @Override
                public void run() {
                    try {
                        // NOTE : "TimeUnit.xx.sleep(n);" used in the following
                        // statement just for the convenience of observing the
                        // process of using semaphore.
                        TimeUnit.MILLISECONDS.sleep(300);
                        System.out.println(Thread.currentThread().getName() + " is waiting to acquire one permit.");
                        semaphore.acquire();
                        TimeUnit.SECONDS.sleep(3);
                        System.out.println(Thread.currentThread().getName() + " has acquired one permit.");
                        TimeUnit.SECONDS.sleep(2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        semaphore.release();
                        System.out.println(Thread.currentThread().getName() + " has released one permit.");
                    }
                }
            });
        threadPool.shutdown();
        threadPool.awaitTermination(2, TimeUnit.SECONDS);
    }

    public void sharedLock() { // equals to Semaphore.acquire();
        sync.acquireShared(1);
    }

    public void unlock() { // equals to Semaphore.release();
        sync.releaseShared(1);
    }

    public int getPermits() {
        return sync.getPermits();
    }

    /**
     * (1). The inner implementation of sync tool Semaphore
     */
    private static class Sync extends AbstractQueuedSynchronizer {
        private static final long serialVersionUID = 1L;

        protected Sync(int permits) {
            setState(permits);
        }

        final int getPermits() {
            return getState();
        }

        @Override
        protected int tryAcquireShared(int acquireds) {
            for (; ; ) { // This is so-called "non-blocked spin lock".
                int available = getState();
                int remaining = available - acquireds;
                if (remaining < 0 || compareAndSetState(available, remaining))
                    return remaining;
            }
        }

        @Override
        protected boolean tryReleaseShared(int releaseds) {
            for (; ; ) { // This is so-called "non-blocked spin lock".
                int available = getState();
                int remaining = available + releaseds;
                if (compareAndSetState(available, remaining)) {
                    return true;
                }
            }
        }
    }

    @SuppressWarnings("unused")
    private static class MyThread extends Thread {
        private final String tName;
        private final ExtSharedLock_SemaphoreStudy myLock;

        private MyThread(String tName, ExtSharedLock_SemaphoreStudy myLock) {
            this.tName = tName;
            this.myLock = myLock;
        }

        @Override
        public void run() {
            try {
                System.out.println(tName + " is acquiring permit of the sharedLock...");
                TimeUnit.MILLISECONDS.sleep(200L);
                myLock.sharedLock();
                System.out.println(tName + " has acquired a permit and remaining permits is : " + myLock.getPermits());
                TimeUnit.SECONDS.sleep(2);
                System.out.println(tName + " has completed its work.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } /*finally {
                myLock.unlock();
				System.out.println(tName + " released a permit and remaining permits is : " + myLock.getPermits());
			}*/
        }
    }
}
