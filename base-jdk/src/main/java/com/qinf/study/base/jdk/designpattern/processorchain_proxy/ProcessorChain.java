package com.qinf.study.base.jdk.designpattern.processorchain_proxy;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created by qinf on 2020-10-06.
 */
public class ProcessorChain {

    static Set<Processor> processorChain = new TreeSet<>();

    public static void main(String... strings) {
        TemplateProcessor templateProcessor1 = new TemplateProcessor();
        templateProcessor1.setPriority(1);
        TemplateProcessor templateProcessor2 = new TemplateProcessor();
        templateProcessor2.setPriority(2);
        TemplateProcessor templateProcessor3 = new TemplateProcessor();
        templateProcessor3.setPriority(3);
        Processor processor1 = (Processor) ProxyFactory.getProxyObj(templateProcessor1);
        Processor processor2 = (Processor) ProxyFactory.getProxyObj(templateProcessor2);
        Processor processor3 = (Processor) ProxyFactory.getProxyObj(templateProcessor3);

        processorChain.add(processor1);
        processorChain.add(processor2);
        processorChain.add(processor3);

        for (Processor processor : processorChain) {
            processor.process(100);
        }
    }
}
