package com.qinf.study.base.jdk.language.java8.lambda;

import java.util.function.Predicate;

/**
 * Created by qinf on 2017/12/2 PM4:03:11
 * <p>
 * Predicate == Assert
 * Pass in a argument t, return a boolean : boolean test(T t);
 */
public class FunctionalInterface_Predicate {

    public static <T> Predicate<T> nonNull() {
        return t -> t != null;
    }

    public static void main(String... args) {
        boolean isNull = nonNull().test("HELLO");
        System.out.println(isNull);
    }
}
