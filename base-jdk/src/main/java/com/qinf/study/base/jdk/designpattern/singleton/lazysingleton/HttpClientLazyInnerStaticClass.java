package com.qinf.study.base.jdk.designpattern.singleton.lazysingleton;

import java.util.Objects;

/**
 * Created by qinf on 2017/8/1 PM9:35:36
 * <p>
 * The way of lazy singleton realized by JVM load class mechanism.
 * JVM can guarantee thread safety, ensure
 * static inner class xxx just be instantiated once.
 */
public class HttpClientLazyInnerStaticClass {

    private HttpClientLazyInnerStaticClass() {
        if (Objects.nonNull(HolderClass.httpClientInstance))
            throw new RuntimeException("Can not instantiate this class by refect.");
    }

    public static HttpClientLazyInnerStaticClass getHttpClientInstance() {
        return HolderClass.httpClientInstance;
    }

    public void executeHttpGetRequest() {
        System.out.println("Execute http get request.");
    }

    private static class HolderClass {
        private final static HttpClientLazyInnerStaticClass httpClientInstance = new HttpClientLazyInnerStaticClass();
    }
}
