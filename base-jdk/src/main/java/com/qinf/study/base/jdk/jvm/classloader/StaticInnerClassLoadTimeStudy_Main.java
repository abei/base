package com.qinf.study.base.jdk.jvm.classloader;

import com.qinf.study.base.jdk.jvm.classloader.StaticInnerClassLoadTimeStudy.StaticInnerClass;

/**
 * Created by qinf on 2017/11/25 AM11:27:46
 */
public class StaticInnerClassLoadTimeStudy_Main {

    public static void main(String... strings) {
        System.out.println(StaticInnerClassLoadTimeStudy.outerClassMember);
        System.out.println(StaticInnerClassLoadTimeStudy.getInstance());
        System.out.println(StaticInnerClass.staticInnerClassMember);
    }
}
