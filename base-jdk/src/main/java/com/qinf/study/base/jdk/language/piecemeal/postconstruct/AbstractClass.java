package com.qinf.study.base.jdk.language.piecemeal.postconstruct;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by qinf on 2021-02-03.
 */
public abstract class AbstractClass {

    private List<String> list = new ArrayList<>(2);

    @PostConstruct
    private final void init() {
        list.add(this.register());
    }

    protected abstract String register();
}
