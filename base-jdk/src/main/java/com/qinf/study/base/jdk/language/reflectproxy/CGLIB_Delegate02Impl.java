package com.qinf.study.base.jdk.language.reflectproxy;

/**
 * Created by qinf on 2017/7/12 PM9:47:02
 */
public class CGLIB_Delegate02Impl {

    public String generateOrder(String orderId) {
        System.out.println("We are generating order [" + orderId + "]......");
        return "Generate order successfully!";
    }

    public String cancelOrder(String orderId) {
        System.out.println("We are cancelling order [" + orderId + "]......");
        return "Cancel order successfully!";
    }
}
