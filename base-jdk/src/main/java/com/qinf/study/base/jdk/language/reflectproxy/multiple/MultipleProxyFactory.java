package com.qinf.study.base.jdk.language.reflectproxy.multiple;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Objects;

/**
 * Created by qinf on 2020-10-05.
 */
public class MultipleProxyFactory implements InvocationHandler {

    private Object sourceObj;

    public Object getProxyObj(Object sourceObj) {
        this.sourceObj = sourceObj;
        Object proxyObj = Proxy.newProxyInstance(
                sourceObj.getClass().getClassLoader(),
                sourceObj.getClass().getInterfaces(), // means proxy all method of all interface implemented by target class
                //new Class[] { MultipleProcessor.class }, // here hard code just proxy method in Processor interface, no Comparable interface
                this); // this is mean the instance of InvocationHandler
        System.out.println("Proxy class has generated : " + proxyObj.getClass().getName());
        return proxyObj;
    }

    /**
     * The Object proxy : proxy class : is the generated jdk dynamic proxy class instance(a instance of $Proxy0.class).
     * The Method method : invoked method : the method declared in interface MultipleProcessor.class and Comparable.class.
     * The Object[] args : method args : the arguments passed in for method of implementation class.
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("Before perform business logic.");
        Object result = method.invoke(sourceObj, args);
        System.out.println("After perform business logic.");
        return result;
    }

    public static Object getSourceObj(Object proxyObj) {
        Objects.requireNonNull(proxyObj, "The proxy object cannot be null!");
        try {
            Field invoHandler;
            invoHandler = proxyObj.getClass().getSuperclass().getDeclaredField("h");
            invoHandler.setAccessible(true);
            MultipleProxyFactory proxyFactory = (MultipleProxyFactory) invoHandler.get(proxyObj);
            Field sourceObj = proxyFactory.getClass().getDeclaredField("sourceObj");
            sourceObj.setAccessible(true);
            return sourceObj.get(proxyFactory);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
