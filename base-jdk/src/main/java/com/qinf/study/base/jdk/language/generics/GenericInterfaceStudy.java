package com.qinf.study.base.jdk.language.generics;

/**
 * The usage is like generic class. Generic interface usually used as generator.
 */
public interface GenericInterfaceStudy<T, U> {
    void printContent(T t, U u);
}
