package com.qinf.study.base.jdk.designpattern.singleton.staticinnersingleton;

/**
 * Created by qinf on 2021-02-01.
 */
public class StaticInnerClassSingletonStudy {

    private StaticInnerClassSingletonStudy() {

    }

    private static final class InnerHolder {
        private static final Object instance = new Object();
    }

    public static final Object getInstance() {
        return InnerHolder.instance;
    }

    public static void main(String... strings) {
        System.out.println(StaticInnerClassSingletonStudy.getInstance());
    }

}
