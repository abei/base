package com.qinf.study.base.jdk.designpattern.singleton.lazysingleton;

/**
 * Created by qinf on 2017/7/27 PM10:52:23
 */
public class LazySingletonPatternStudy {

    public static void main(String[] args) {
        HttpClientLazyInnerStaticClass httpClient = HttpClientLazyInnerStaticClass.getHttpClientInstance();
        httpClient.executeHttpGetRequest();
    }
}
