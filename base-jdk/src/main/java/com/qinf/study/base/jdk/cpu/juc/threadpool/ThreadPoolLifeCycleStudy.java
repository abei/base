package com.qinf.study.base.jdk.cpu.juc.threadpool;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by qinf on 2017/6/13 PM10:50:33
 * <p>
 * ## executor config ##
 * thread.pool.executor.coreSize=10
 * thread.pool.executor.maxSize=20
 * thread.pool.executor.idleTime=60
 * # S, MS
 * thread.pool.executor.timeUnit=S
 * # SynchronousQueue, LinkedBlockingQueue, ArrayedBlockingQueue
 * thread.pool.executor.workQueue=LinkedBlockingQueue
 * # AbortPolicy, DiscardPolicy, DiscardOldestPolicy, CallerRunsPolicy
 * thread.pool.executor.rejectPolicy=CallerRunsPolicy
 */
public class ThreadPoolLifeCycleStudy {
    public static void main(String... strings) throws InterruptedException {
        /**
         * Create non-scheduled thread pool by constructor.
         * There are 4 ways to create non-scheduled thread pool by passing different arguments.
         * way 1 : ThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue)
         * way 2 : ThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, RejectedExecutionHandler handler)
         * way 3 : ThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory)
         * way 4 : ThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler)
         *
         * Description of inner implementation of ThreadPoolExecutor
         * WC is WorkerCountOf, means the sum of worker threads.
         * If WC <= coreSize, if there is a task(Object of Runnable), worker thread will handle it, if no, worker thread will WAITING on workQueue by queue.take();
         * If coreSize < WC < maxSize, when completed tasks, the redundant worker threads will WAITING on workQueue in keepAliveTime by queue.poll(keepAliveTime)
         * once time elapsed, these redundant worker threads will return from run() and died;
         */
        ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 5, 50, TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(1));
        // This configure can reclaim all idle threads and shutdown thread pool eventually when tasks have completed.
        //executor.allowCoreThreadTimeOut(true);

        for (int i = 0; i < 5; i++)
            executor.execute(new Task("mytask-" + i));
    }

    static class Task implements Runnable {
        String taskName;

        Task(String taskName) {
            this.taskName = taskName;
        }

        @Override
        public void run() {
            for (int i = 0; i < 5; i++) {
                System.out.println(Thread.currentThread().getName() + " : " + Thread.currentThread().getState() + " : run task " + taskName);
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
