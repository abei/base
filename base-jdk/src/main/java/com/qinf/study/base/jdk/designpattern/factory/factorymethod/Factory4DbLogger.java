package com.qinf.study.base.jdk.designpattern.factory.factorymethod;

/**
 * Created by qinf on 2017/7/25 PM8:53:10
 */
public class Factory4DbLogger implements LoggerFactory {

    ILogger logger;

    @Override
    public ILogger createLogger() {
        logger = new DatabaseLoggerImpl();
        return logger;
    }

}
