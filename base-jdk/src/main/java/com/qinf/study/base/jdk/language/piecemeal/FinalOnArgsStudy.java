package com.qinf.study.base.jdk.language.piecemeal;

/**
 * Created by qinf on 2017/11/19 PM8:53:14
 */
public class FinalOnArgsStudy {

    private static void changeArgs(final String arg) {
        /**
         * If you want to change a local variable delimited by final, you will
         * get following compilation exception :
         * "The final local variable arg cannot be assigned. It must be blank and
         * not using a compound assignment."
         */
        //arg = "NEW VALUE";
    }

    public static void main(String... args) {
        changeArgs("HELLO WORLD");
    }
}
