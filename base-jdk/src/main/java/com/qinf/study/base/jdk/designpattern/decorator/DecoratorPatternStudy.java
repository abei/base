package com.qinf.study.base.jdk.designpattern.decorator;

/**
 * Created by qinf on 2017/7/15 PM5:33:07
 * <p>
 * Decorator pattern used for extending the function of original(old) system.
 * The defect of this kind of decorator is for different requirement, you have
 * to add different decorator class, that will lead to too many decorator class.
 * We can solve this problem by AOP mechanism.
 */
public class DecoratorPatternStudy {

    public static void main(String[] args) {
        House bedRoomWithBed = new HouseDecoratorAddDesk(new BedRoomImpl());
        //bedRoomWithBed.live();
        House bedRoomWithBed$Chair = new HouseDecoratorAddChair(bedRoomWithBed);
        bedRoomWithBed$Chair.live();
    }
}
