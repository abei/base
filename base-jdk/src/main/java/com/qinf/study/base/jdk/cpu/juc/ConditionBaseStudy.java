package com.qinf.study.base.jdk.cpu.juc;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConditionBaseStudy {

    private static Lock lock = new ReentrantLock();
    private static Condition condition = lock.newCondition();

    public static void main(String[] args) {
        NonMainThread1 nonMainThread1 = new NonMainThread1("non-main thread-1");
        NonMainThread2 nonMainThread2 = new NonMainThread2("non-main thread-2");

        System.out.println("[" + Thread.currentThread().getName() + " thread]: acquire lock");
        lock.lock(); // acquire lock
        try {
            nonMainThread2.start();
            nonMainThread1.start();
            System.out.println("[" + Thread.currentThread().getName() + " thread]: start non-main thread");
            System.out.println("[" + Thread.currentThread().getName() + " thread]: block by condition");
            condition.await(); // wait
            System.out.println("[" + Thread.currentThread().getName() + " thread]: continue");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println("[" + Thread.currentThread().getName() + " thread]: release lock");
            lock.unlock(); // release lock
        }
    }

    static class NonMainThread1 extends Thread {
        public NonMainThread1(String name) {
            super(name);
        }

        public void run() {
            System.out.println("[" + Thread.currentThread().getName() + "]: acquire lock");
            lock.lock(); // acquire lock
            try {
                TimeUnit.SECONDS.sleep(10);
                System.out.println("[" + Thread.currentThread().getName() + "]: wake up main thread");
                condition.signal(); // wake up other thread on condition queue
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                System.out.println("[" + Thread.currentThread().getName() + "]: release lock");
                lock.unlock(); // release lock
            }
        }
    }

    static class NonMainThread2 extends Thread {
        public NonMainThread2(String name) {
            super(name);
        }

        public void run() {
            System.out.println("[" + Thread.currentThread().getName() + "]: acquire lock");
            lock.lock(); // acquire lock
            try {
                System.out.println("[" + Thread.currentThread().getName() + "]: wake up main thread");
                condition.signal(); // wake up other thread on condition queue
            } finally {
                System.out.println("[" + Thread.currentThread().getName() + "]: release lock");
                lock.unlock(); // release lock
            }
        }
    }
}

/**
 * the result of running application :
 * [main thread]: acquire lock
 * [main thread]: start non-main thread
 * [main thread]: block by condition
 * [non-main thread-2]: acquire lock
 * [non-main thread-2]: wake up main thread
 * [non-main thread-2]: release lock
 * [main thread]: continue
 * [main thread]: release lock
 * [non-main thread-1]: acquire lock
 * [non-main thread-1]: wake up main thread
 * [non-main thread-1]: release lock
 */