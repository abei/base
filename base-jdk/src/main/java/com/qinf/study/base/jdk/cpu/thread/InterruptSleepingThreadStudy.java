package com.qinf.study.base.jdk.cpu.thread;

public class InterruptSleepingThreadStudy {

    public static void main(String[] args) throws InterruptedException {
        // step 1.create new thread
        Thread sleepingThread = new Thread(new ThreadSleeping(), "sleeping thread");
        System.out.println("[" + sleepingThread.getName() + "] life status is : [" + sleepingThread.getState()
                + "], interrupt status is : " + "[" + sleepingThread.isInterrupted() + "]");

        // step 2.start the new thread
        sleepingThread.start();
        System.out.println("[" + sleepingThread.getName() + "] life status is : [" + sleepingThread.getState()
                + "], interrupt status is : " + "[" + sleepingThread.isInterrupted() + "]");

        Thread.sleep(1000);
        // step 3.main thread perform interrupt instruction, this time
        // sleepingThread is in blocking status
        sleepingThread.interrupt();
        System.out.println("[" + sleepingThread.getName() + "] life status is : [" + sleepingThread.getState()
                + "], interrupt status is : " + "[" + sleepingThread.isInterrupted() + "]");

        // step 4.check the last result
        Thread.sleep(2000);
        System.out.println("[" + sleepingThread.getName() + "] life status is : [" + sleepingThread.getState()
                + "], interrupt status is : " + "[" + sleepingThread.isInterrupted() + "]");
    }

    static class ThreadSleeping implements Runnable {
        public void run() {
            try {
                int cnt = 0;
                for (; ; ) {
                    Thread.sleep(400);
                    System.out.println("{loop " + cnt++ + "} - [" + Thread.currentThread().getName()
                            + "] life status is : [" + Thread.currentThread().getState() + "], interrupt status is : "
                            + "[" + Thread.currentThread().isInterrupted() + "]");
                }
            } catch (InterruptedException e) {
                System.out.println("{catch exception} - [" + Thread.currentThread().getName() + "] life status is : ["
                        + Thread.currentThread().getState() + "], interrupt status is : " + "["
                        + Thread.currentThread().isInterrupted() + "]");
            }
        }
    }
}

/**
 * [sleeping thread] life status is : [NEW], interrupt status is : [false]
 * [sleeping thread] life status is : [RUNNABLE], interrupt status is : [false]
 * {loop 0} - [sleeping thread] life status is : [RUNNABLE], interrupt status is : [false]
 * {loop 1} - [sleeping thread] life status is : [RUNNABLE], interrupt status is : [false]
 * [sleeping thread] life status is : [TIMED_WAITING], interrupt status is : [false]
 * {catch exception} - [sleeping thread] life status is : [RUNNABLE], interrupt status is : [false]
 * [sleeping thread] life status is : [TERMINATED], interrupt status is : [false]
 */
