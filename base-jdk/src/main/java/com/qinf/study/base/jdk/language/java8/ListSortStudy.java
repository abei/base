package com.qinf.study.base.jdk.language.java8;

import lombok.Builder;
import lombok.Data;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Created by qinf on 2019-07-19.
 */
public class ListSortStudy {
    @Data
    @Builder
    static class Dto {
        private String date;
        private String name;
        private Integer age;
    }

    public static void main(String... args) {
        final List<Dto> dtos = Arrays.asList(
                Dto.builder().date("2019-07-10").name("hello").age(10).build(),
                Dto.builder().date("2019-07-09").name("world").age(9).build(),
                Dto.builder().date("2019-06-25").name("bye").age(13).build(),
                Dto.builder().date("2019-07-12").name("nice").age(12).build(),
                Dto.builder().date("2019-07-11").name("day").age(11).build());
        dtos.sort(Comparator.comparing(Dto::getDate));
        List<Dto> tmp = dtos;
        tmp.forEach(System.out::println);
    }
}
