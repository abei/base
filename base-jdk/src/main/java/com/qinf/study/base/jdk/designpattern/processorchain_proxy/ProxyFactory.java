package com.qinf.study.base.jdk.designpattern.processorchain_proxy;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Objects;

/**
 * Created by qinf on 2020-10-06.
 */
public final class ProxyFactory {

    public static Object getProxyObj(Object sourceObj) {
        Object proxyObj = Proxy.newProxyInstance(
                sourceObj.getClass().getClassLoader(),
                sourceObj.getClass().getInterfaces(), // means proxy all method of all interface implemented by source class
                //new Class[] { Processor.class }, // here hard code just proxy method declared in Processor interface,
                                                   // no Comparable interface, so when invoke compareTo() by proxy object explicitly,
                                                   // exception java.lang.ClassCastException will be threw.
                //new Class[] { Comparable.class },
                new InvokeHandler(sourceObj)); // this means the instance of InvocationHandler, if the instance
                                               // is single then the proxy object always same
        System.out.println("Proxy object has generated: " + proxyObj.getClass().getName());
        // proxy object is an instance of com.sun.proxy.$Proxy0
        return proxyObj;
    }

    public static Object getProxyObj(Object sourceObj, InvocationHandler invokeHandler) {
        Object proxyObj = Proxy.newProxyInstance(
                sourceObj.getClass().getClassLoader(),
                sourceObj.getClass().getInterfaces(),
                invokeHandler);
        return proxyObj;
    }

    public static Object getSourceObj(Object proxyObj) {
        return getSourceObj(proxyObj, "sourceObj");
    }

    public static Object getSourceObj(Object proxyObj, String sourceObjName) {
        Objects.requireNonNull(proxyObj, "The proxy object cannot be null!");
        try {
            Field invokeHandlerF = proxyObj.getClass().getSuperclass().getDeclaredField("h");
            invokeHandlerF.setAccessible(true);
            InvocationHandler invokeHandler = (InvocationHandler) invokeHandlerF.get(proxyObj);
            Field sourceObjF = invokeHandler.getClass().getDeclaredField(sourceObjName);
            sourceObjF.setAccessible(true);
            return sourceObjF.get(invokeHandler);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    final static class InvokeHandler implements InvocationHandler {
        private Object sourceObj;

        public InvokeHandler(Object sourceObj) {
            this.sourceObj = sourceObj;
        }

        /**
         * proxy: the generated dynamic proxy object (a instance of $Proxy0.class).
         * method: all methods declared in interface of source class.
         * args: the arguments for method invocation on the proxy object.
         */
        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (!Objects.equals("process", method.getName())) {
                return method.invoke(sourceObj, args);
            }
            System.out.println(method.getName() + " start invoke");
            Object result = method.invoke(sourceObj, args);
            System.out.println( method.getName() + " end invoke");
            return result;
        }
    }

}
