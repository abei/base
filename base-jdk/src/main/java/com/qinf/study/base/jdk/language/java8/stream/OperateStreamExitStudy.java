package com.qinf.study.base.jdk.language.java8.stream;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Created by qinf on 2020-08-07.
 */
public class OperateStreamExitStudy {

    /**
     * 1. Throw exception will exit stream completely.
     * 2. Return statement will skip executing codes after return.
     * 3. Break will issue a compile error.
     */
    public static void main(String... strings) {
        Stream<String> stream = Arrays.asList("HELLO", "WORLD", "STREAM").stream();
        stream.forEach(e -> {
            if (Objects.equals(e, "WORLD")) {
                throw new NullPointerException();
                //return;
                //break;
            }
            System.out.println(e);
        });
    }
}
