package com.qinf.study.base.jdk.designpattern.factory.abstractfactory;

/**
 * Created by qinf on 2017/7/25 PM10:01:52
 */
public class QIN_BedRoom extends Room {

    @Override
    public void live() {
        System.out.println("QIN company has created bed room.");
    }

}
