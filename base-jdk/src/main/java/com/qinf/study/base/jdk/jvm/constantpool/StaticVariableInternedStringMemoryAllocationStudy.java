package com.qinf.study.base.jdk.jvm.constantpool;

import java.util.UUID;
import java.util.concurrent.locks.LockSupport;

/**
 * Created by qinf on 2020-06-25.<p>
 *
 * Firstly, should clarify that, for jvm implementation hotspot of jdk8, the
 * string literal value, interned string value and static field value is stored
 * in old gen of heap. Here, we verify whether the interned string value stored
 * in heap. The investigation steps as follows:<br>
 * 1. modify jvm options: -Xms50m -Xmx50m -Xmn48m -XX:MetaspaceSize=10m -XX:NativeMemoryTracking=detail -XX:+UnlockDiagnosticVMOptions -XX:+PrintNMTStatistics<br>
 * 2. observe the jdk tool, jVisualVm(check its plugin visual GC)<br>
 * 3. check the output of NMT<br>
 * 4. check the heap dump to find the interned string values<br>
 */
public class StaticVariableInternedStringMemoryAllocationStudy {

    private static int CONSTANT = 10;

    //public static volatile String lastString;

    public static void main(String... strings) {
        /**
         * The interned string values(instances of String.class) can be collected by GC
         * when full gc is triggered as old gen is full or called System.gc().
         */
        /*for (int iterations = 0; iterations < 40;) {
            String baseName = UUID.randomUUID().toString();
            for (int i = 0; i < 1_000_000; i++) {
                lastString = (baseName + i).intern();
            }
            if (++iterations % 10 == 0) {
                System.gc();
            }
            LockSupport.parkNanos(500_000_000);
        }*/

        String rdm = UUID.randomUUID().toString();
        for (long i = 0; i < 100_000_000; i++) {
            String string = (rdm + rdm + rdm + rdm + i).intern();
            //new String(rdm + rdm + rdm + rdm + i);
            if (i % 10000000 == 0) {
                //LockSupport.parkNanos(6000000);
                //System.gc();
            }
        }
    }
}
