package com.qinf.study.base.jdk.designpattern.factory.abstractfactory;

/**
 * Created by qinf on 2017/7/25 PM10:02:38
 */
public class QIN_BathRoom extends Room {

    @Override
    public void live() {
        System.out.println("QIN company has created bath room.");
    }

}
