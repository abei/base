package com.qinf.study.base.jdk.designpattern.processorchain_engine.entry;

import com.qinf.study.base.jdk.designpattern.processorchain_engine.ProcessorChainEngine;
import com.qinf.study.base.jdk.designpattern.processorchain_engine.Request;
import com.qinf.study.base.jdk.designpattern.processorchain_engine.Response;
import org.apache.commons.lang3.tuple.Pair;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by qinf on 2021-10-02.
 */
public class ConcreteProcessorChain extends ProcessorChainEngine<Request, Response> {

    /*@Autowired
    private ConcreteProcessor1 concreteProcessor1;

    @Autowired
    private ConcreteProcessor2 concreteProcessor2;*/

    @PostConstruct
    private void init() {
        this.processors = new ArrayList<>(10);
        this.processors.add(new ConcreteProcessor1());
        this.processors.add(new ConcreteProcessor2());
    }

    @Override
    public Pair doConstructProcessorArgs(Request request, Response response) {
        ConcreteRequest concreteRequest = (request instanceof Request)
                ? (ConcreteRequest) request : new ConcreteRequest();
        ConcreteResponse concreteResponse = (Objects.nonNull(response) && response instanceof Response)
                ? (ConcreteResponse) response : ConcreteResponse.initResponse();
        return Pair.of(concreteRequest, concreteResponse);
    }
}
