package com.qinf.study.base.jdk.disk.io.standard;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Created by qinf on 2019-03-10.<p>
 *
 * The based accessing file methods include in java.io, can read small file.
 */
public class StandardFileIOStudy {

    private static final String SRC_FILE_PATH = "E:/log.txt";

    private static final String DEST_FILE_PATH = "E:/result.txt";

    /**
     * Operate file by bytes.
     */
    private void operateFileByByte() {
        try (InputStream ins = new FileInputStream(SRC_FILE_PATH);
             OutputStream outs = new FileOutputStream(DEST_FILE_PATH)) {
            int data;
            while ((data = ins.read()) != -1) {
                outs.write(data);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Operate file by char.
     */
    private void operateFileByChar() {
        try (FileReader fileReader = new FileReader(SRC_FILE_PATH);
             FileWriter fileWriter = new FileWriter(DEST_FILE_PATH)) {
            int data;
            while ((data = fileReader.read()) != -1) {
                fileWriter.write((char) data);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Operate file by line.
     */
    private void operateFileByLine() {
        try (BufferedReader bufReader = new BufferedReader(new FileReader(SRC_FILE_PATH));
             BufferedWriter bufWriter = new BufferedWriter(new FileWriter(DEST_FILE_PATH))) {
            String data = null;
            while ((data = bufReader.readLine()) != null) {
                bufWriter.write(data + "\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Files.lines() add in jdk1.7, include in java.nio
     */
    private List<Map> salvageMissedClues(String filePath) {
        List<Map> missedClueModels = Collections.EMPTY_LIST;
        try (Stream<String> lines = Files.lines(Paths.get(filePath), Charset.forName("GBK"))) {
           /* missedClueModels = lines.filter(Objects::nonNull)
                    .map(line -> line.substring(1, line.length()-1))
                    .map(line -> JSON.parseObject(line, Map.class))
                    .collect(Collectors.toList());*/
        } catch (IOException e) {
            e.printStackTrace();
        }
        return missedClueModels;
    }
}
