package com.qinf.study.base.jdk.designpattern.decorator;

/**
 * Created by qinf on 2017/7/16 PM9:05:38
 */
public abstract class AbstractHouseDecorator implements House {
    // public class AbstractHouseDecorator implements House {
    private House house;

    public AbstractHouseDecorator(House house) {
        this.house = house;
    }

    /**
     * a common method
     */
    public void live() {
        house.live();
        System.out.println("Qinf in the house!!");
    }
}
