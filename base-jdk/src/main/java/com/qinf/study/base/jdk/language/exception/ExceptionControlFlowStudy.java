package com.qinf.study.base.jdk.language.exception;

public class ExceptionControlFlowStudy {

    public static void main(String[] args) throws Exception {
        int i = 0;
        for (; ; ) {
            i++;
            if (i == 3) {
                // 1.If throw a exception directly from here, the program flow will be interrupted.
                // The result of print is :
                //loop print i : 1
                //loop print i : 2
                //I throw a exception!!
                //Exception in thread "main" java.lang.Exception
                //	at study.qfen.exception.ExceptionControlFlow.main(ExceptionControlFlow.java:14)
                System.out.println("I throw a exception!!");
                throw new Exception();

                // 2. If you handled the exception, the program will go on.
                // The result of print is :
                //loop print i : 1
                //loop print i : 2
                //I get a exception!!
                //loop print i : 3
                //loop print i : 4
                //loop print i : 5
                /*try {
                    int j = i / 0;
				} catch (Exception e) {
					System.out.println("I get a exception!!");
				}*/
            }
            System.out.println("loop print i : " + i);

            if (i == 5)
                break;
        }
    }
}
