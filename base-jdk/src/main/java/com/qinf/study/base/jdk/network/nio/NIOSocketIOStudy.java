package com.qinf.study.base.jdk.network.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * Created by qinf on 2017-12-05.
 */
public class NIOSocketIOStudy {

    static class NIOServer {
        private static Selector sSelector;
        private static ServerSocketChannel serverSocketChannel;

        /**
         * 1. Get a client channel by sSelector.
         * 2. Register the client channel to sSelector, specify read event(because server need read data from the channel later).
         */
        private static void handleAcceptedConnectionFromClient(SelectionKey key) throws IOException {
            // NOTE : When server accepted a new connection from client, a new SocketChannel will be created.
            SocketChannel clientSocketChannel = ((ServerSocketChannel) key.channel()).accept();
            System.out.println(clientSocketChannel.hashCode());
            clientSocketChannel.configureBlocking(false);
            //clientSocketChannel.register(sSelector, SelectionKey.OP_READ, ByteBuffer.allocateDirect(1024));
            clientSocketChannel.register(sSelector, SelectionKey.OP_READ);
        }

        /**
         * 1. Get a client channel by sSelector.
         * 2. Read data into buffer from client channel.
         */
        private static void handleReadDataFromClient(SelectionKey key) throws IOException {
            SocketChannel clientSocketChannel = (SocketChannel) key.channel();
            //ByteBuffer inBuf = (ByteBuffer)key.attachment();
            ByteBuffer inBuf = ByteBuffer.allocate(1024);
            long bytesRead = clientSocketChannel.read(inBuf);
            while (bytesRead > 0) {
                inBuf.flip();
                System.out.print(Thread.currentThread().getName() + " is listening and has read data from client is : [");
                while (inBuf.hasRemaining())
                    System.out.print((char) inBuf.get());
                System.out.println("]");
                inBuf.clear();
                bytesRead = clientSocketChannel.read(inBuf);
            }
            if (bytesRead == -1)
                clientSocketChannel.close();
        }

        /**
         * 1. Get a client channel by sSelector.
         * 2. Write data into buffer of a client channel.
         */
        private static void handleWriteDataToClient(SelectionKey key) throws IOException {
            SocketChannel clientSocketChannel = (SocketChannel) key.channel();
            //ByteBuffer outBuf = (ByteBuffer)key.attachment();
            ByteBuffer outBuf = ByteBuffer.wrap(("(channel " + clientSocketChannel.hashCode() + ") - message from server.").getBytes());
            outBuf.flip();
            while (outBuf.hasRemaining())
                clientSocketChannel.write(outBuf);
            outBuf.compact();
        }

        /**
         * 1. Create server channel and binds a port.
         * 2. Register the server channel to sSelector, specify accept event(because server need listen connection from client).
         */
        static void initServer(int port) throws IOException {
            serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.configureBlocking(false);
            serverSocketChannel.socket().bind(new InetSocketAddress(port));
            sSelector = Selector.open();
            serverSocketChannel.register(sSelector, SelectionKey.OP_ACCEPT);
        }

        /**
         * 1. Listen means there is a thread to perform sSelector.select() in loop.
         * 2. Selector.select() is a blocking method.
         * 3. You can submit real IO task to a background thread(thread pool).
         */
        static void listenS() {
            try {
                for (; ; ) {
                    //Iterator<SelectionKey> iter = sSelector.selectedKeys().iterator();
                    /** NOTE 1 : select() will be blocked until at least one registered event of a channel is ready */
                    sSelector.select();
                    Iterator<SelectionKey> iter = sSelector.selectedKeys().iterator();
                    while (iter.hasNext()) {
                        SelectionKey key = (SelectionKey) iter.next();
                        /** A connection was accepted by a ServerSocketChannel. */
                        if (key.isAcceptable()) {
                            System.out.print("Server accepted a connection from client, and has created a new SocketChannel instance is  : ");
                            /** NOTE 2 : Can perform this method in other thread, just leave main-thread to listen ready events of registered channels. */
                            handleAcceptedConnectionFromClient(key);
                            /** A channel is ready for reading. */
                        } else if (key.isReadable()) {
                            //System.out.print("Server read data from client is : ");
                            handleReadDataFromClient(key);
                            /** A channel is ready for writing. */
                        } else if (key.isWritable() && key.isValid()) {
                            handleWriteDataToClient(key);
                            /** A connection was established with a remote server. */
                        } else if (key.isConnectable()) {
                            System.out.println("isConnectable = true");
                        }
                        iter.remove();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (Objects.nonNull(sSelector))
                        sSelector.close();
                    if (Objects.nonNull(serverSocketChannel))
                        serverSocketChannel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class NIOClient {
        private static Selector cSelector;

        static {
            try {
                cSelector = Selector.open();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        static void listenC() {
            SocketChannel clientSocketChannel = null;
            try {
                for (; ; ) {
                    cSelector.select();
                    Iterator<SelectionKey> iter = cSelector.selectedKeys().iterator();
                    while (iter.hasNext()) {
                        SelectionKey key = iter.next();
                        // Connection event is going on.
                        if (key.isConnectable()) {
                            clientSocketChannel = (SocketChannel) key.channel();
                            if (clientSocketChannel.isConnectionPending())
                                // Complete real connection.
                                clientSocketChannel.finishConnect();
                            clientSocketChannel.configureBlocking(false);
                            sendDataToServer(clientSocketChannel);
                            /** After we sent data, we should set event of the channel is OP_READ used for waiting read data from server. */
                            clientSocketChannel.register(cSelector, SelectionKey.OP_READ);
                        } else if (key.isReadable()) {
                            System.out.println("isReadable = true");
                        }
                        iter.remove();
                    }
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (Objects.nonNull(clientSocketChannel))
                        clientSocketChannel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        static void createConnToServer(String host, int port) throws IOException {
            SocketChannel clientSocketChannel = SocketChannel.open();
            clientSocketChannel.configureBlocking(false);
            System.out.println("Client is connecting to remote server...");
            clientSocketChannel.connect(new InetSocketAddress(host, port));
            clientSocketChannel.register(cSelector, SelectionKey.OP_CONNECT);
        }

        private static void sendDataToServer(SocketChannel clientSocketChannel) throws InterruptedException, IOException {
            ByteBuffer outBuf = ByteBuffer.allocate(1024);
            TimeUnit.MILLISECONDS.sleep(200);
            System.out.println("Client is sending data to remote server...");
            for (int i = 0; i < 3; i++) {
                TimeUnit.MILLISECONDS.sleep(300);
                outBuf.clear();
                outBuf.put(("(Client channel " + clientSocketChannel.hashCode() + ") - message from client.").getBytes());
                outBuf.flip();
                while (outBuf.hasRemaining())
                    clientSocketChannel.write(outBuf);
                outBuf.compact();
            }
        }

        /**
         * 1. Get a client channel by cSelector.
         * 2. Read data into buffer from channel.
         */
        @SuppressWarnings("unused")
        private static void receiveDataFromServer(SelectionKey key) throws IOException {
            SocketChannel clientSocketChannel = (SocketChannel) key.channel();
            //ByteBuffer inBuf = (ByteBuffer)key.attachment();
            ByteBuffer inBuf = ByteBuffer.allocate(1024);
            long bytesRead = clientSocketChannel.read(inBuf);
            while (bytesRead > 0) {
                inBuf.flip();
                System.out.print(Thread.currentThread().getName() + " is listening and has read data from server is : [");
                while (inBuf.hasRemaining())
                    System.out.print((char) inBuf.get());
                System.out.println("]");
                inBuf.clear();
                bytesRead = clientSocketChannel.read(inBuf);
            }
            if (bytesRead == -1)
                clientSocketChannel.close();
        }
    }

    public static void main(String... strings) throws IOException, InterruptedException {
        ExecutorService threadPool1 = Executors.newFixedThreadPool(1, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r, "LISTEN-SERVER");
                //t.setDaemon(true);
                return t;
            }
        });
        /**
         * 1. Initialize server in main-thread.
         * 2. Run listening work in other thread.
         */
        NIOServer.initServer(8080);
        threadPool1.execute(() -> {
            NIOServer.listenS();
        });

        TimeUnit.MILLISECONDS.sleep(1_000L);

        /** Create 2 client channels. */
        for (int i = 0; i < 2; i++) {
            NIOClient.createConnToServer("localhost", 8080);
        }
        ExecutorService threadPool2 = Executors.newFixedThreadPool(1, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r, "LISTEN-CLIENT");
                //t.setDaemon(true);
                return t;
            }
        });
        threadPool2.execute(() -> {
            NIOClient.listenC();
        });
    }
}
