package com.qinf.study.base.jdk.designpattern.factory.abstractfactory;

/**
 * Created by qinf on 2017/7/25 PM10:02:38
 */
public class NAN_BathRoom extends Room {

    @Override
    public void live() {
        System.out.println("NAN company has created bath room.");
    }

}
