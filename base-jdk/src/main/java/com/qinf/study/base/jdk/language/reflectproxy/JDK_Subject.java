package com.qinf.study.base.jdk.language.reflectproxy;

/**
 * Created by qinf on 2017/7/6 PM9:12:07
 */
public interface JDK_Subject {
    public String performBusinessLogic();
}
