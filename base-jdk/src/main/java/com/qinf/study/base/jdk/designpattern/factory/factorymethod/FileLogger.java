package com.qinf.study.base.jdk.designpattern.factory.factorymethod;

/**
 * Created by qinf on 2017/7/25 PM8:50:36
 */
public class FileLogger implements ILogger {

    @Override
    public void writeLog() {
        System.out.println("Log output to file.");
    }

}
