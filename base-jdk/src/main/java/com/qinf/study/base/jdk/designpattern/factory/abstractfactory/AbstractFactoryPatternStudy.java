package com.qinf.study.base.jdk.designpattern.factory.abstractfactory;

/**
 * Created by qinf on 2017/7/25 PM9:32:51
 */
public class AbstractFactoryPatternStudy {

    public static void main(String[] args)
            throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        RoomFactory roomFactory = (RoomFactory) Class
                .forName("com.qinf.study.base.jdk.designpattern.factory.abstractfactory.QINRoomFactory").newInstance();
        Room bedRoom = roomFactory.createBedRoom();
        Room bathRoom = roomFactory.createBathRoom();
        bedRoom.live();
        bathRoom.live();
    }
}
