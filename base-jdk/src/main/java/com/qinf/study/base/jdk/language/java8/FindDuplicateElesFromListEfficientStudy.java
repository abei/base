package com.qinf.study.base.jdk.language.java8;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by qinf on 2018/4/15.
 */
public class FindDuplicateElesFromListEfficientStudy {

    public static void main(String... strings) {
        List<String> largeList = Arrays.asList("H", "E", "L", "L", "O", ",", "W", "O", "R", "L", "D");
        Set<String> tmpUniquer = new HashSet<String>();

        Set<String> duplicates = largeList.stream()
                .filter(e -> !tmpUniquer.add(e)) // Set.add() returns false if the item was already in the set.
                .collect(Collectors.toSet());

        duplicates.forEach(System.out::println);
    }
}
