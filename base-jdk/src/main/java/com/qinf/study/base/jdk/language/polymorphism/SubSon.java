package com.qinf.study.base.jdk.language.polymorphism;

public class SubSon extends Son {

    public void iThink() {
        System.out.println("sub son think hello world");
    }

    @Override
    public void sonMethod() {
    }

}
