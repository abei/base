package com.qinf.study.base.jdk.memory;

import java.nio.ByteBuffer;
import java.util.Base64;

/**
 * Created by qinf on 2020-06-27.<p>
 *
 * 1. Space allocation<br>
 *    Native (byte) buffer is allocated in user space out of jvm heap. From jvm perspective, the user space has
 *    been allocated could be named as native memory.<p>
 * 2. Advantage of native buffer<br>
 *    The jvm will make a best effort to perform native I/O operations directly upon it. That is, it will attempt
 *    to avoid copying the buffer's content to (or from) an intermediate buffer(in heap) before (or after) each
 *    invocation of one of the underlying operating system's native I/O operations.<p>
 * 3. Use native buffer<br>
 *    However, the application still uses an object on the jvm heap to orchestrate I/O operations, but the buffer
 *    that holds the data is held in native memory, the jvm heap object only contains a reference to the native buffer.<br>
 *    Jdk has supplied some measures to operate the native buffer in java.nio package(allocate space, use it, free content).<p>
 * 4. GC management<br>
 *    1) The objects in native buffer cannot collected automatically, but that not means GC wouldn't manage it.<br>
 *    2) The objects(holds data) is stored in native buffer, but their references in stored in old gen, so the objects will
 *       be collected when full gc is triggered (olg gen is full or System.gc is invoked).<br>
 *    3) The pathological case would be that the native buffer becomes full and one or more direct ByteBuffers are eligible for
 *       GC (and could be freed to make some space on the native heap), but the jvm heap is mostly empty so GC doesn't occur.
 */
public class DirectBufferStudy {

    public static void main(String... strings) {
        //Unsafe.getUnsafe().allocateMemory(200 * 1024 * 1024);  // 200MB
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(1 * 1024 * 1024);
        String stringHasPadding = Base64.getEncoder().encodeToString("Hello".getBytes());
        //String stringNoPadding = Base64.getEncoder().withoutPadding().encodeToString("Hello World".getBytes());
        byte[] bytes = Base64.getDecoder().decode(stringHasPadding); // Hello occupies 5 bytes
        byteBuffer.put(bytes);
        byteBuffer.putInt(10); // int occupies 4 bytes
        System.out.println("The position is: " + byteBuffer.position());
        Byte byte0 = byteBuffer.get(0);

        /*CharBuffer charBuffer = byteBuffer.asCharBuffer();
        charBuffer.put("Native byte buffer testing.");
        charBuffer.flip();
        System.out.println("print: " + charBuffer.toString());*/
    }
}
