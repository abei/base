package com.qinf.study.base.jdk.cpu.juc.ext_queue;

/**
 * Created by qinf on 2017/12/5 PM10:30:42
 * <p>
 * Used for :
 * 1. newSingleThreadExecutor()    ===> ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue())
 * 2. newFixedThreadPool(nThreads) ===> ThreadPoolExecutor(nThreads, nThreads, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue())
 */
public class BLOCK_LinkedBlockingQueueStudy {

}
