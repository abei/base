package com.qinf.study.base.jdk.language.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by qinf on 2018-1-30.
 * <p>
 * The annotation will be retained to Runtime, and it can just be attached on field.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface StatementParam {

    String value();
}
