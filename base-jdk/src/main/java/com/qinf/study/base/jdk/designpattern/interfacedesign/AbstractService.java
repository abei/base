package com.qinf.study.base.jdk.designpattern.interfacedesign;

import java.util.Map;

/**
 * 1. Abstract class is unnecessary to have abstract method.
 * 2. If a class contains abstract method, it must be declared as abstract class.
 * 3. Abstract class can have construct method, but it cannot be instantiated.
 */
public abstract class AbstractService implements IService {

    /*private String serviceType;
    private String serviceStatus;
    protected AbstractService(String serviceType, String serviceStatus) {
        this.serviceType = serviceType;
        this.serviceStatus = serviceStatus;
    }*/

    protected AbstractService() {
    }

    @Override
    /**
     * Override method defined in interface.
     * Supplied usage for other class.
     */
    public final void process(
            Map<String, String> header,
            Map<String, String[]> parameters,
            Map<String, Object> attributes,
            Map<String, Object> session,
            String content,
            Map<String, Object> context) {
        process(header, parameters, attributes, session, context, content);
    }

    /**
     * Overload method process above by different sequence of params.
     * Can be overrided by subclass.
     */
    protected void process(
            Map<String, String> header,
            Map<String, String[]> parameters,
            Map<String, Object> attributes,
            Map<String, Object> session,
            Map<String, Object> context,
            String content) {
        process(header, parameters, attributes, session, context);
    }

    /**
     * Overload method process above by different number of params.
     * Can be overrided by subclass.
     */
    protected void process(
            Map<String, String> header,
            Map<String, String[]> parameters,
            Map<String, Object> attributes,
            Map<String, Object> session,
            Map<String, Object> context) {
    }
}
