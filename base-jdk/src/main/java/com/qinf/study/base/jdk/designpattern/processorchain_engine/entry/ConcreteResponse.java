package com.qinf.study.base.jdk.designpattern.processorchain_engine.entry;

import com.qinf.study.base.jdk.designpattern.processorchain_engine.Response;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by qinf on 2021-10-02.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConcreteResponse implements Response {

    private boolean exit;

    private Result result = Result.SUCC;

    private String message;

    public static ConcreteResponse initResponse() {
        return new ConcreteResponse();
    }

    @Override
    public boolean isExitInstantly() {
        return false;
    }
}
