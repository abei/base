package com.qinf.study.base.jdk.cpu.thread;

import java.util.concurrent.locks.LockSupport;

/**
 * Created by qinf on 2017/5/19 PM9:21:15
 */
public class InterruptParkingThreadStudy {

    public static void main(String[] args) throws InterruptedException {
        // step 1.create new thread
        Thread parkingThread = new Thread(new ThreadParking(), "parking thread");
        System.out.println("[" + parkingThread.getName() + "] life status is : [" + parkingThread.getState()
                + "], interrupt status is : " + "[" + parkingThread.isInterrupted() + "]");

        // step 2.start the new thread
        parkingThread.start();
        System.out.println("[" + parkingThread.getName() + "] life status is : [" + parkingThread.getState()
                + "], interrupt status is : " + "[" + parkingThread.isInterrupted() + "]");

        Thread.sleep(3000);
        System.out.println("[" + parkingThread.getName() + "] life status is : [" + parkingThread.getState()
                + "], interrupt status is : " + "[" + parkingThread.isInterrupted() + "]");
        // step 4.main thread perform interrupt instruction
        parkingThread.interrupt();

        // step 5.check the last result
        Thread.sleep(2000);
        System.out.println("[" + parkingThread.getName() + "] life status is : [" + parkingThread.getState()
                + "], interrupt status is : " + "[" + parkingThread.isInterrupted() + "]");
    }

    static class ThreadParking implements Runnable {
        public void run() {
            try {
                int cnt = 0;
                for (; ; ) {
                    Thread.sleep(500);
                    System.out.println("{loop " + cnt++ + "} - [" + Thread.currentThread().getName()
                            + "] life status is : [" + Thread.currentThread().getState() + "], interrupt status is : "
                            + "[" + Thread.currentThread().isInterrupted() + "]");
                    if (cnt == 2)
                        // step 3.current thread into blocking status by calling LockSupport.park()
                        LockSupport.park();
                }
            } catch (InterruptedException e) {
                System.out.println("{catch exception} - [" + Thread.currentThread().getName() + "] life status is : ["
                        + Thread.currentThread().getState() + "], interrupt status is : " + "["
                        + Thread.currentThread().isInterrupted() + "]");
            }
        }
    }
}
/**
 * [parking thread] life status is : [NEW], interrupt status is : [false]
 * [parking thread] life status is : [RUNNABLE], interrupt status is : [false]
 * {loop 0} - [parking thread] life status is : [RUNNABLE], interrupt status is : [false]
 * {loop 1} - [parking thread] life status is : [RUNNABLE], interrupt status is : [false]
 * [parking thread] life status is : [WAITING], interrupt status is : [false]
 * {catch exception} - [parking thread] life status is : [RUNNABLE], interrupt status is : [false]
 * [parking thread] life status is : [TERMINATED], interrupt status is : [false]
 */
