package com.qinf.study.base.jdk.language.java8.lambda.lambdabyte;

import java.io.IOException;

/**
 * Created by qinf on 2018-12-23.
 */
public class Calculator {

    public int binaryCalculate(int x, int y, ICalculate cal) throws IOException {
        return cal.operate(x, y);
    }
}
