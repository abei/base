package com.qinf.study.base.jdk.language.piecemeal;

/**
 * Created by qinf on 2017/5/16.
 */
public class FinalOnFieldStudy {
    final String[] finalArr;
    String[] tmpArr = new String[]{"Q", "F"};

    {
        finalArr = new String[]{"HELLO", "WORLD"};
    }

    public static void main(String[] args) {
        FinalOnFieldStudy obj = new FinalOnFieldStudy();
        obj.finalArr[0] = "EVIL";
        for (String ele : obj.finalArr) {
            System.out.println(ele);
        }
        /**
         * this will encounter a compile error, because cannot assign a value
         * to a final variable
         */
        //obj.finalArr = obj.tmpArr;
    }
}
