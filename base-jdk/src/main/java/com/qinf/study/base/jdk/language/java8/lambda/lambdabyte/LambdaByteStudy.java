package com.qinf.study.base.jdk.language.java8.lambda.lambdabyte;

import java.io.IOException;

/**
 * Created by qinf on 2018-12-23.
 */
public class LambdaByteStudy {
    /*private static final Optional<String> OPTIONAL_GREETING = Optional.of("Hello World!");

    static void useAnonymousClass() {
        OPTIONAL_GREETING.ifPresent(new Consumer<String>() {
            @Override
            public void accept(String greeting) {
                System.out.println();
            }
        });}*/

    public static void main(String... args) throws IOException {
        Calculator calculator = new Calculator();

        ICalculate calLambda = (x, y) -> x + y;
        calculator.binaryCalculate(5, 10, calLambda);
        calculator.binaryCalculate(10, 20, (x, y) -> x + y);
    }
}
