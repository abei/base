package com.qinf.study.base.jdk.cpu.juc.base_lock;

import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * Created by qinf on 2017/12/13 PM10:30:42
 * <p>
 * What we can do by Unsafe :
 * 1. Create an object without by constructor.
 * 2. Assign a value to a field by its memory address, even it is final.
 * 3. Operate array by its memory address.
 * 4. CAS operation.
 * 5. Block mechanism(Unsafe.park() & Unsafe.unpark()).
 */
public class BaseOfLock_Unsafe_Study {

    public static void main(String... strings) throws NoSuchFieldException, SecurityException, IllegalArgumentException,
            IllegalAccessException, InstantiationException {
        Unsafe unsafe = null;
        Field f = Unsafe.class.getDeclaredField("theUnsafe");
        f.setAccessible(true);
        unsafe = (Unsafe) f.get(null);

        /** 1. Create an object by Unsafe without constructor. */
        MyObject myobj = (MyObject) unsafe.allocateInstance(MyObject.class);
        System.out.println("myobj is : " + myobj.toString() + "\n");

        /** 2. Assign a value to a field through its memory address by Unsafe, even it is final. */
        long field_addr = unsafe.objectFieldOffset(MyObject.class.getDeclaredField("finalField"));
        System.out.println("Field address in memory is : " + field_addr);
        unsafe.getAndSetInt(myobj, field_addr, 100);
        System.out.println("The value assigned by Unsafe is : " + myobj.getVlu() + "\n");

        /** 3. Operate array through its memory address by Unsafe. */
        int[] arr = new int[5];
        System.out.println("Array content is : " + Arrays.toString(arr));
        //long arr_baseOffset = unsafe.arrayBaseOffset(Object[].class);
        long arr_baseOffset = unsafe.arrayBaseOffset(int[].class);
        System.out.println("Array address in memory is : " + arr_baseOffset);
        unsafe.putInt(arr, arr_baseOffset, 200);
        unsafe.putInt(arr, arr_baseOffset + 4L, 300);
        unsafe.putInt(arr, arr_baseOffset + 8L, 400);
        // NOTE : a int will occupy 4 bytes, so the next address of arr need plus 4.
        System.out.println("Array content is : " + Arrays.toString(arr) + "\n");

        /** 4. CAS operation. */
        MyObject myobj4CAS = (MyObject) unsafe.allocateInstance(MyObject.class);
        System.out.println("myobj4CAS is : " + myobj4CAS.toString());
        long field_arr_4cas = unsafe.objectFieldOffset(MyObject.class.getDeclaredField("finalField"));
        System.out.println("Field address in memory is : " + field_arr_4cas);
        unsafe.getAndSetInt(myobj4CAS, field_arr_4cas, 1000);
        unsafe.compareAndSwapInt(myobj4CAS, field_arr_4cas, 1000, 2000);
        System.out.println("The value compareAndSwapped by Unsafe is : " + myobj4CAS.getVlu() + "\n");
    }

    static class MyObject {
        private final int finalField;

        MyObject(int field) {
            this.finalField = field;
        }

        int getVlu() {
            return finalField;
        }
    }
}
