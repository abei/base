package com.qinf.study.base.jdk.language.java8.concurrent;

import java.util.Objects;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

/**
 * Created by qinf on 2017/12/10 PM8:40:23
 */
public class CompletableFutureThreadPoolStudy {
    private static final Random randomDelayer = new Random();
    private static final AtomicInteger threadNumber = new AtomicInteger(0);

    private static void randomDelay() {
        long delay = 500 + randomDelayer.nextInt(2_000);
        try {
            TimeUnit.MILLISECONDS.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String... strings) throws InterruptedException, ExecutionException {
        final ExecutorService threadPool = Executors.newCachedThreadPool(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r, "Executor-" + threadNumber.getAndIncrement());
                t.setDaemon(true);
                return t;
            }
        });

        /*final ExecutorService threadPool = Executors.newFixedThreadPool(5, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r, "Executor");
                t.setDaemon(true);
                return t;
            }
        });*/

        CompletableFuture<String> cmplFuture = CompletableFuture
                .supplyAsync(() -> {
                    System.out.println(Thread.currentThread().getName() + " run aync processing getPriceInfo.");
                    throw new RuntimeException("Exception handle test.");
                    /** 1. Thread-0 to handle getPriceInfo in async mode. */
                    //return new Shop("AppleMall").getProductPriceInfo("iphone");
                }, threadPool);

        //If the task is not long-time task, it's not necessary to run it in async mode again.
        cmplFuture
                .thenApply(cmplPrice -> {
                    /** 2. Thread-0 to handle parsePriceInfo in async mode. */
                    System.out.println(Thread.currentThread().getName() + " run aync processing parsePriceInfo.");
                    return DTO.parsePriceInfo(cmplPrice);})
                .handleAsync((res, ex) -> {
                    if (Objects.nonNull(ex)) {
                        System.out.println(Thread.currentThread().getName() + " : " + ex.getMessage());
                    }
                    return res;
                }, threadPool);
                /** 3. Thread-1 to handle displayPriceInfo in async mode. */
                //.thenAcceptAsync(dto -> System.out.println(Thread.currentThread().getName() + " run aync processing displayPriceInfo"), threadPool);

        //cmplFuture.get();

        /*threadPool.shutdownNow();
        threadPool.awaitTermination(9_000L, TimeUnit.MILLISECONDS);*/

        //cmplFuture.thenApply(cmplPrice -> {
        //    System.out.println(Thread.currentThread().getName() + " run aync processing parsePriceInfo again.");
            /**
             * 4. Because the thread pool was shutdown, so main thread will handle parsePriceInof again.
             * If you use cmplFuture.thenApplyAsync(), then ForkJoinThreadPool will handle this parsePriceInfo.
             */
        //    return DTO.parsePriceInfo(cmplPrice);
        //});

        /**
         * The result of running :
         * Thread-0 run aync processing getPriceInfo.
         * Thread-0 run aync processing parsePriceInfo.
         * Thread-1 run aync processing displayPriceInfo.
         * main run aync processing parsePriceInfo again.
         */
    }

    static class DTO {
        private final String shopName;
        private final long productPrice;

        public DTO(String shopName, long productPrice) {
            this.shopName = shopName;
            this.productPrice = productPrice;
        }

        public static DTO parsePriceInfo(String priceInfo) {
            String[] splits = (String[]) Stream.of(priceInfo.split(":"))
                    .map(String::trim)
                    .toArray(String[]::new);
            return new DTO(splits[0],
                    Long.parseLong(splits[1]));
        }

        public String getShopName() {
            return shopName;
        }

        public long getProductPrice() {
            return productPrice;
        }
    }

    static class Shop {
        private String shopName;

        Shop(String shopName) {
            this.shopName = shopName;
        }

        public String getShopName() {
            return this.shopName;
        }

        /**
         * consume time is random
         */
        public String getProductPriceInfo(String productName) {
            randomDelay();
            long productPrice = new Random().nextLong() * productName.charAt(0) + productName.charAt(1);
            return shopName + ":" + productPrice;
        }
    }
}
