package com.qinf.study.base.jdk.language.java8.stream;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.stream.Stream;

/**
 * Created by qinf on 2017/11/28 PM4:53:51
 */
public class OperateStreamCollectToMapStudy {

    @SuppressWarnings({"unused"})
    public static void main(String... strings) {
        Stream<Person> stream = Stream.of(
                new Person("1", "HELLO"),
                new Person(null, "WORLD"),
                new Person("3", null));
        /**
         * If you are not sure whether there is null key or null value when convert to map, you can use
         * Stream.collect(supplier, accumulator, combinator);, this method can handle the situation.
         */

        /**
         * 1. compile error : The method collect(Supplier, BiConsumer, BiConsumer) in the type Stream
         * is not applicable for the arguments (Supplier, (map, p) -> map.put(p.getId(), p.getName()), HashMap::putAll)
         * ???? WHY ????  But, when you use Enum type, it can work well. ???? WHY ????
         *
         * The answer is : generic problem, you should change "Stream stream" to "Stream<Person> stream".
         */
        /*final HashMap<String, String> asMapFromInnerClass = stream.collect(HashMap::new,
                (map, p) -> map.put(p.getId(), p.getName()),
				HashMap::putAll);*/
        final HashMap<String, String> asMapFromEnum = ConfigKey.stream().collect(
                HashMap::new,
                (map, e) -> map.put(e.getConfigKey(), new Integer(e.getConfigKey().length()).toString()),
                HashMap::putAll);
        final HashMap<String, String> asMapFromInnerClass = stream.collect(
                HashMap::new,
                (map, p) -> map.put(p.getId(), p.getName()),
                HashMap::putAll);

        /**
         * 2. can work
         */
        /*Map<String, String> asMap = (Map<String, String>) stream.collect(
				() -> new HashMap<String, String>(),
				(m, p) -> ((Map<String, String>) m).put(((Person) p).getId(), ((Person) p).getName()),
				(m1, m2) -> ((Map<String, String>) m1).putAll((Map<? extends String, ? extends String>) m2));*/

        /**
         * 3. can work
         */
		/*Map<String, String> asMap = (Map<String, String>) stream.collect(
				new Supplier<HashMap<String, String>>() {
					@Override
					public HashMap<String, String> get() {
						return new HashMap<String, String>();
					}
				},
				new BiConsumer<HashMap<String, String>, Person>() {
					@Override
					public void accept(HashMap<String, String> m, Person p) {
						m.put(p.getId(), p.getName());
					}
				},
				new BiConsumer<HashMap<String, String>, HashMap<String, String>>() {
					@Override
					public void accept(HashMap<String, String> m1, HashMap<String, String> m2) {
						m1.putAll(m2);
					}
				}
		);*/

        for (Entry<String, String> entry : asMapFromInnerClass.entrySet())
            System.out.println(entry.getKey() + " : " + entry.getValue());
    }

    private enum ConfigKey {
        PRODUCER_ENABLE("kafka.producer.client.enable"),
        FUSE_TRIGGER("kafka.producer.client.fuseTrigger"),
        BROKER_LIST("kafka.producer.client.kafkacluster.addr"),
        BROKER_TOPIC("kafka.producer.client.kafkacluster.topic"),
        SYNC_SEND("kafka.producer.client.conn.isSyncSend"),
        NUM_RETRY("kafka.producer.client.conn.retries");
        private String key;

        ConfigKey(String key) {
            this.key = key;
        }

        public static Stream<ConfigKey> stream() {
            return Stream.of(ConfigKey.values());
        }

        public String getConfigKey() {
            return key;
        }
    }

    static class Person {
        private String id;
        private String name;

        public Person(String id, String name) {
            this.id = id;
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }
}
