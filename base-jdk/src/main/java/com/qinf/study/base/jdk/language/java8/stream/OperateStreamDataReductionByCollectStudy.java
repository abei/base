package com.qinf.study.base.jdk.language.java8.stream;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created by qinf on 2017/11/26 PM10:32:10
 * <p>
 * Stream.collect() is also reduction method for collecting data after Intermediate Operation.
 * <p>
 * Its definition as following :
 * <R> R collect(Supplier<R> supplier, BiConsumer<R, ? super T> accumulator, BiConsumer<R, R> combiner);
 * Argument 1 : supplier : used to create a new result container.
 * Argument 2 : accumulator : add a element into new result container.
 * Argument 3 : combiner : combine two new generated result containers when in parallel processing.
 * <p>
 * NOTE : about the Argument 3, combiner, why it has to exist in Stream.collection()
 * 1. The API level should not differentiate between sequential and parallel streams.
 * For that reason, you should assume, that the combiner might be used.
 * 2. The combiner is used when your Stream is parallel, since in that case several threads
 * collect elements of the Stream into sub-lists of the final output ArrayList,
 * and these sub-lists have to be combined to produce the final ArrayList.
 */
public class OperateStreamDataReductionByCollectStudy {

    public static void main(String... strings) {
        Stream<String> stringStream = Stream.of("HELLO", "HELLO", "STREAM");
        Set<String> asSet = stringStream.collect(() -> new HashSet<String>(),
                HashSet::add,
                HashSet::addAll);
        for (String e : asSet)
            System.out.println(e);
    }
}
