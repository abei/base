package com.qinf.study.base.jdk.language.java8.lambda.functiondesign;

/**
 * Created by qinf on 2018/4/1 PM7:32:46
 */
public class MyInvoker extends UniFunctionInvoker {

    protected MyInvoker(boolean flag) {
        super(flag);
    }

    @Override
    protected void before() {
        System.out.println("Lock it.");
    }

    @Override
    protected void after() {
        System.out.println("Unlock it.");
    }
}
