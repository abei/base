package com.qinf.study.base.jdk.language.java8.lambda;

import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by qinf on 2017/11/26 PM4:58:32
 * <p>
 * Combine multiple Lambda Expressions into one wanted function.
 */
public class LambdaOperation_ComposeMultipleLambdasStudy {

    public static void main(String... strings) {
        // 1. Predicate compose
        Predicate<Apple> redApple = apple -> apple.getColor().equals("red");
        Predicate<Apple> redAndHeavyApple = redApple.and(apple -> apple.getWeight() > 500);

        // 2. Function compose
        Function<Integer, Integer> funcCount = x -> x + 1;
        Function<Integer, Integer> funcMultiply = x -> x * 2;

        // 2.1. Function compose - andThen - andThen == for x, first count 1 and then multiply 2.
        Function<Integer, Integer> funcOfAndThen = funcCount.andThen(funcMultiply);
        int resultOfAndThen = funcOfAndThen.apply(1);

        // 2.2. Function compose - compose - compose == for x, first multiply 2 and then count 1
        Function<Integer, Integer> funcOfCompose = funcCount.compose(funcMultiply);
        int resultOfCompose = funcOfCompose.apply(1);
    }

    private static class Apple {
        private String color;
        private int weight;

        public Apple(String color, int weight) {
            this.color = color;
            this.weight = weight;
        }

        public String getColor() {
            return this.color;
        }

        public int getWeight() {
            return this.weight;
        }
    }
}
