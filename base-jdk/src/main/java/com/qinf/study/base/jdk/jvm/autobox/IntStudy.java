package com.qinf.study.base.jdk.jvm.autobox;

/**
 * Created by qinf on 2018/5/29.
 */
public class IntStudy {
    // The primitive value will be stored different memory address by different instance.
    private int primitiveTypeField = 10;

    public static void main(String... strings) {
        IntStudy obj01 = new IntStudy();
        IntStudy obj02 = new IntStudy();

        obj01.primitiveTypeField = 20;
        System.out.println(obj01.primitiveTypeField);
        System.out.println(obj02.primitiveTypeField);
    }
}
