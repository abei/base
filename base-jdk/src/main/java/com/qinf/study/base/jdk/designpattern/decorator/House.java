package com.qinf.study.base.jdk.designpattern.decorator;

/**
 * Created by qinf on 2017/7/16 PM8:57:51
 */
public interface House {
    public abstract void live();
}
