package com.qinf.study.base.jdk.language.annotation;

import java.lang.reflect.Method;
import java.util.stream.Stream;

/**
 * Created by qinf on 2018-01-30.
 * <p>
 * How to use annotation(define annotation -> attach annotation -> obtain annotation -> process annotation) :
 * 1. define annotation
 * You can define your own annotation by JDK annotation grammar.
 * <p>
 * 2. attach annotation
 * Annotation is a kind of Tag to attach on method(field, class etc) to add additional info for the method.
 * You can get the additional info(tag/annotation) at an available time.
 * <p>
 * 3. obtain annotation
 * You can get the annotations attached on method by reflection mechanism.
 * <p>
 * 4. process annotation
 * When you got the annotation, you can print it or do some more work by it.
 */
public class AnnotationStudy {

    public static void main(String... strings) {
        Method[] allMethods = OrderDAO.class.getMethods();

        //Stream.of(allMethods).filter(m -> m.isAnnotationPresent(Statement.class))
        //        .map(m -> m.getAnnotation(Statement.class))
        //        .forEach(System.out::println);
        Stream.of(allMethods).filter(m -> m.isAnnotationPresent(Statement.class))
                .map(m -> m.getAnnotation(Statement.class))
                .forEach(annotation -> System.out.println(annotation.statementName()));
        Statement methodAnnotation = allMethods[0].getAnnotation(Statement.class);
        System.out.print(methodAnnotation);
    }
}
