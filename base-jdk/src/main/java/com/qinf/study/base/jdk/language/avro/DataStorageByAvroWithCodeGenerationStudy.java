package com.qinf.study.base.jdk.language.avro;

import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;

import java.io.File;
import java.io.IOException;

/**
 * Created by qinf on 2018-6-3.
 */
public class DataStorageByAvroWithCodeGenerationStudy {

    /**
     * Use avro by code generation means :
     * 1. First, you should generate related Java object by corresponding avro schema file(*.avsc).
     * 2. Then, you can do Serialization and Deserialization by the generated Java object.
     */
    public static void main(String... strings) throws IOException {
        // Construct via builder.
        User user01 = User.newBuilder()
                .setName("fen-01")
                .setAge(new Integer(100))
                .setEmail("email-01")
                .build();
        User user02 = User.newBuilder()
                .setName("fen-02")
                .setAge(new Integer(200))
                .setEmail("email-02")
                .build();

        // Serialize user1, user2 to disk.
        DatumWriter<User> userDatumWriter = new SpecificDatumWriter<User>(User.class);
        DataFileWriter<User> dataFileWriter = new DataFileWriter<User>(userDatumWriter);
        dataFileWriter.create(user01.getSchema(), new File(
                "G:/study-code bak/study-myown/study/study-jdk/src/main/java/com/qinf/study/avro/data-storage-users-by-code-generation.avro"));
        dataFileWriter.append(user01);
        dataFileWriter.append(user02);
        dataFileWriter.close();


        File file = new File(
                "G:/study-code bak/study-myown/study/study-jdk/src/main/java/com/qinf/study/avro/data-storage-users-by-code-generation.avro");
        // Deserialize Users from disk.
        DatumReader<User> userDatumReader = new SpecificDatumReader<User>(User.class);
        try (DataFileReader<User> dataFileReader = new DataFileReader<User>(file, userDatumReader)) {
            User user = null;
            while (dataFileReader.hasNext()) {
                /**
                 * Reuse user object by passing it to next(). This saves us from
                 * allocating and garbage collecting many objects for files with
                 * many items.
                 */
                user = dataFileReader.next(user);
                System.out.println(user);
            }
        }
    }
}
