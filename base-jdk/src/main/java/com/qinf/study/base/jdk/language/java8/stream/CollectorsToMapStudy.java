package com.qinf.study.base.jdk.language.java8.stream;

import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by qinf on 2017/11/28 PM4:49:40
 */
public class CollectorsToMapStudy {

    @SuppressWarnings({"unchecked", "rawtypes"})
    public static void main(String... strings) {
        Stream stream = Stream.of(
                new Person("1", "HELLO"),
                new Person("2", "WORLD"),
                new Person("3", "JAVA"));
        /**
         * NOTE : Collectors.toMap() dose not support null key and null value or null remappingFunction.
         * Otherwise NullpointerExcepton will be thrown.
         * If you are not sure whether there is null key or null value when convert to map, you can use
         * Stream.collect(supplier, accumulator, combinator);
         */
        Map<?, ?> asMap = (Map<?, ?>) stream.collect(Collectors.toMap(Person::getId, Person::getName));

        for (Entry<?, ?> entry : asMap.entrySet())
            System.out.println(entry.getKey() + " : " + entry.getValue());
    }

    static class Person {
        private String id;
        private String name;

        public Person(String id, String name) {
            this.id = id;
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }
}
