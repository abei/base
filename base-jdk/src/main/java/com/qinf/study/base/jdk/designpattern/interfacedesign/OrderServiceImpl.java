package com.qinf.study.base.jdk.designpattern.interfacedesign;

import java.util.Map;

public class OrderServiceImpl extends AbstractService {

    @Override
    protected void process(
            Map<String, String> header,
            Map<String, String[]> parameters,
            Map<String, Object> attributes,
            Map<String, Object> session,
            Map<String, Object> context) {
        System.out.println("Override process method in OrderServiceImpl.");
    }
}
