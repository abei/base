package com.qinf.study.base.jdk.jvm.classloader;

/**
 * Created by qinf on 2017/11/25 AM11:19:31
 * <p>
 * Class load time is : when you first use this class will trigger the class initialized.
 * <p>
 * 1. When you just only use the static inner class, the outer
 * class will not be initialized. That's what it means in fact the static inner
 * class is the top class as same as the outer class. Of course, when you just
 * use the outer class, the nested inner class will be also not initialized.
 * 2. When you invoke the static final member of a static inner class or outer
 * class, the class will not be initialized.
 */
public class StaticInnerClassLoadTimeStudy {

    //public static final String outerClassMember = "Outer class memeber.";
    public static String outerClassMember = "Outer class memeber.";

    static {
        System.out.println("Initialize Outer class static{}.");
    }

    public static String getInstance() {
        //return StaticInnerClassLoadTimeStudy.outerClassMember;
        return StaticInnerClass.staticInnerClassMember;
    }

    public static class StaticInnerClass {
        //public static final String staticInnerClassMember = "Static Inner class member.";
        public static String staticInnerClassMember = "Static Inner class member.";

        static {
            System.out.println("Initialize Inner class static{}.");
        }
    }
}
