package com.qinf.study.base.jdk.disk.io.nio;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

/**
 * Created by qinf on 2021-01-01.<br>
 * <p>
 * A channel for reading, writing, mapping, and manipulating a file.
 * FileChannel of NIO is suitable for reading large file, and support
 * multiple threads to read and write the same file concurrently, i.e.,
 * the read() and write() of FileChannel is thread-safe.
 * Furthermore, although as part of NIO, FileChannel operations are blocking
 * and do not have a non-blocking mode.
 */
public class NIOFileIOStudy {

    // String "HelloWorld" contained in the file.
    static String SRC_FILE_PATH = "E:/git/base/base-jdk/src/main/java/com/qinf/study/base/jdk/disk/io/file/src.txt";

    static String DEST_FILE_PATH = "E:/git/base/base-jdk/src/main/java/com/qinf/study/base/jdk/disk/io/file/dest.txt";

    private static void readFile() {
        /**
         * Get a file channel.
         */
        try (RandomAccessFile file = new RandomAccessFile(SRC_FILE_PATH, "r");
             FileChannel channel = file.getChannel()) {
            int bufferSize = channel.size() < 1024 ? (int) channel.size() : 1024;
            /**
             * Create memory buffer, the allocated buffer is essentially return new HeapByteBuffer(capacity, capacity)
             */
            ByteBuffer buffer = ByteBuffer.allocate(bufferSize); //
            /**
             * Read file content from specific position.
             */
            //channel.read(buffer, 5);
            //buffer.flip(); // Before read data from buffer, should switch buffer into read mode, can be considered as make buffer is readable
            //System.out.println(new String(buffer.array(), StandardCharsets.UTF_8));
            /**
             * Read all file content.
             */
            while (channel.read(buffer) > 0) {
                buffer.flip();
                while (buffer.hasRemaining()) {
                    System.out.println((char) buffer.get());
                }
                buffer.clear(); // Once has read all the data from buffer, should clear the buffer to make it writable again before write data into it
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeFile() {
        /**
         * Get a file channel.
         */
        try (RandomAccessFile file = new RandomAccessFile(DEST_FILE_PATH, "rw");
             FileChannel channel = file.getChannel()) {
            /**
             * Write data into buffer.
             */
            ByteBuffer buffer = ByteBuffer.wrap("HelloWorld".getBytes(StandardCharsets.UTF_8));
            //channel.write(buffer); // The writing operation is append mode
            channel.write(buffer, 5);
            /**
             * FileChannel.force() force the data write into local device(disk) from page cache to
             * avoid data lost when operating system crashed.
             * The underlying implement of FileChannel.force() is system call fdatasync() or fsync(),
             * can see FileChannelImpl.c:
             *   JNIEXPORT jint JNICALL
             *   Java_sun_nio_ch_FileChannelImpl_force0(JNIEnv *env, jobject this, jobject fdo, jboolean md)
             *   {
             *       jint fd = fdval(env, fdo);
             *       int result = 0;
             *       if (md == JNI_FALSE) {
             *           result = fdatasync(fd);
             *       } else {
             *           result = fsync(fd);
             *       }
             *       return handle(env, result, "Force failed");
             *   }
             */
            channel.force(true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String... strings) {
        //readFile();
        writeFile();
    }
}
