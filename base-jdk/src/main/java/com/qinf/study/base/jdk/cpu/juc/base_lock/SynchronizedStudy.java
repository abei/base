package com.qinf.study.base.jdk.cpu.juc.base_lock;

public class SynchronizedStudy {

    public static void main(String[] args) {
        SynchronizedStudy outerClass = new SynchronizedStudy();
        SynchronizedStudy.MonitorThread monitor = outerClass.new MonitorThread("non-main thread");

        synchronized (monitor) { // main thread can acquire lock of object monitor from here
            System.out.println("[" + Thread.currentThread().getName() + " thread] start non-main thread");
            monitor.start();

            System.out.println("[" + Thread.currentThread().getName() + " thread] call wait() - release lock of object monitor");
            try {
                /**
                 * NOTE : A thread calls wait() of a object is mean it release the lock of this object.
                 * Further more, the thread would blocking itself waiting other thread to wake it up.
                 */
                monitor.wait();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("[" + Thread.currentThread().getName() + " thread] is waked up and continue after blocked 5 seconds");
        }
    }

    class MonitorThread extends Thread {

        public MonitorThread(String threadName) {
            super(threadName);
        }

        public void run() {
            synchronized (this) { // non-main thread can acquire lock of object monitor from here
                System.out.println("[" + Thread.currentThread().getName() + "] call notify() - wake up blocking main thread");
                /**
                 * After non-main thread called notify(), but doesn't mean it has released the lock.
                 * So exactly, the main thread is waked up but haven't acquired the lock until the
                 * non-main thread release the lock, for example, non-main thread is dead or it also call wait()
                 * to release lock.
                 * That's why I use Thread.sleep(5000) to illustrate that main thread can re-run after blocked 5 seconds.
                 */
                notify();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

/**
 * The result of running application is :
 * [main thread] start non-main thread
 * [main thread] call wait() - release lock of object monitor
 * [non-main thread] call notify() - wake up blocking main thread
 * [main thread] is waked up and continue after blocked 5 seconds
 */
