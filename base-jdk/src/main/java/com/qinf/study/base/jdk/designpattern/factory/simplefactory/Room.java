package com.qinf.study.base.jdk.designpattern.factory.simplefactory;

/**
 * Created by qinf on 2017/7/24 PM9:38:21
 */
public abstract class Room {
    // The common business method
    public void live() {
        System.out.println("Qinf in the house!!");
    }

    public abstract void extralFunction();
}
