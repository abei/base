package com.qinf.study.base.jdk.designpattern.builder;

/**
 * Created by qinf on 2017/8/21 PM10:46:59
 */
public class User {

    private final String firstName;
    private final String lastName;
    private final int age;
    private final String phone;
    private final String address;

    private User(UserBuilder userBuilder) {
        /* this.firstName is means the field firstName of class User **/
        this.firstName = userBuilder.firstName;
        this.lastName = userBuilder.lastName;
        this.age = userBuilder.age;
        this.phone = userBuilder.phone;
        this.address = userBuilder.address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String toString() {
        return String.join(" | ", new String[]{this.getFirstName(), this.getLastName(), String.valueOf(this.getAge()),
                this.getPhone(), this.getAddress()});
    }

    public static class UserBuilder {
        private final String firstName; // the parameter must be specified
        private final String lastName; // the parameter must be specified
        private int age;
        private String phone;
        private String address;

        public UserBuilder(String firstName, String lastName) {
            /** this.firstName is means User.UserBuilder.firstName */
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public UserBuilder age(int age) {
            this.age = age;
            return this;
        }

        public UserBuilder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public UserBuilder address(String address) {
            this.address = address;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}
