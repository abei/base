package com.qinf.study.base.jdk.language.java8.parallel;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by qinf on 2017/12/3 PM3:23:48
 * <p>
 * When use Parallel Stream, there will be N threads to handle the
 * sub stream from the Parallel Stream at the same time.
 * The number[N] of the working thread equal the number of CPU core.
 */
public class OperateParallelStreamStudy {

    public static List<String> findPricesFromAllShops(List<Shop> allShops, String productName) {
        // use parallel stream
        return allShops.parallelStream().
                map(s -> s.getShopName() + " : " + s.getProductPrice(productName)).
                collect(Collectors.toList());
    }

    public static void main(String... strings) {
        List<Shop> allShops = Arrays
                .asList(new Shop[]{new Shop("TMall"), new Shop("Amazon"), new Shop("JD"), new Shop("WalMar")});
        List<String> allPrices = findPricesFromAllShops(allShops, "IPhone1000");

        allPrices.stream().forEach(System.out::println);
    }

    static class Shop {
        private String shopName;

        Shop(String shopName) {
            this.shopName = shopName;
        }

        public String getShopName() {
            return this.shopName;
        }

        public long getProductPrice(String productName) {
            try {
                Thread.sleep(1_000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return new Random().nextLong() * productName.charAt(0) + productName.charAt(1);
        }
    }
}
