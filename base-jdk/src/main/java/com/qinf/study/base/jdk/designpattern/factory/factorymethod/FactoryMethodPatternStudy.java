package com.qinf.study.base.jdk.designpattern.factory.factorymethod;

/**
 * Created by qinf on 2017/7/25 PM8:32:32
 * <p>
 * The factory method design pattern need supply different factory for
 * different product, use one factory to create one product. Not like
 * simple factory design pattern, it just supply one factory for all products.
 */

public class FactoryMethodPatternStudy {

    public static void main(String[] args)
            throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        LoggerFactory loggerFactory;
        ILogger logger;
        // By reflect mechanism, to instantiate different type logger factory.
        loggerFactory = (LoggerFactory) Class
                .forName("com.qinf.study.base.jdk.designpattern.factory.factorymethod.Factory4DbLogger").newInstance();
        logger = loggerFactory.createLogger();

        // loggerFactory = (LoggerFactory) Class.forName("Factory4FileLogger").newInstance();
        // logger = loggerFactory.createLogger();

        logger.writeLog();
    }
}
