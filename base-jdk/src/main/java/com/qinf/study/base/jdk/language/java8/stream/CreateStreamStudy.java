package com.qinf.study.base.jdk.language.java8.stream;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by qinf on 2017/11/26 PM8:47:05
 * <p>
 * As comparison to collection :
 * Collection : is used for data storage and access.
 * Stream : is used for data computation.
 */
public class CreateStreamStudy {

    public static void main(String... strings) {
        // 1. Create a empty stream.
        Stream<?> streamEmpty = Stream.empty();

        // 2. Create a stream by individual values.
        Stream<String> streamFromValues = Stream.of("HELLO", "WORLD", "STREAM");

        // 3. Create a stream by array.
        Stream<String> streamFromArray1 = Arrays.stream(new String[]{"HELLO", "WORLD", "STREAM"});
        Stream<String> streamFromArray2 = Stream.of(new String[]{"HELLO", "WORLD", "STREAM"});

        // 4. Create a stream by collection. [NOTE : Collection.stream()]
        Stream<String> streamFromCollection = Arrays.asList(new String[]{"HELLO", "WORLD", "STREAM"}).stream();
        Stream<String> parallelStreamFromCollection = Arrays.asList(new String[]{"HELLO", "WORLD", "STREAM"}).parallelStream();

        // 5. Create a stream by IO.
        try (Stream<String> streamFromIO = Files.lines(Paths.get("data.txt"));) {
            // You can use this streamFromIO here.
            // NOTE : Each element in stream corresponds to each line in the file.
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 6. Create a infinite stream by Stream API.
        Stream<Integer> streamFromAPIIterate = Stream.iterate(0, n -> n + 2).limit(10);
        IntStream streamFromAPIGenerate = IntStream.generate(() -> 1).limit(10);
    }
}
