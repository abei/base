package com.qinf.study.base.jdk.cpu.juc.threadpool;

/**
 * Created by qinf on 2018-12-15.<p>
 *
 * 1.threadPool.shutdown()
 *   JavaDoc says : "Initiates an orderly shutdown in which previously submitted
 *   tasks are executed, but no new tasks will be accepted. This method does not
 *   wait(block) for previously submitted tasks to complete execution."
 *   But now, you may be confused about the JavaDoc's saying, especially the second
 *   sentence, the meaning of this sentence is the invoker called shutdown() will
 *   return immediately, i.e. the thread that calls shutdown does not blocks.
 * <p>
 * 2.threadPool.awaitTermination(35, TimeUnit.SECONDS)
 *   Blocks until all tasks have completed execution after a shutdown request, or
 *   the timeout occurs, or the current thread is interrupted, whichever happens first.
 * <p>
 * 3.How to shutdown thread pool
 *   Invoke shutdown() first, then awaitTermination(timeout, timeunit).
 */
public class ThreadPoolShutDownStudy {
}
