package com.qinf.study.base.jdk.cpu.juc.threadpool;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

/**
 * Created by qinf on 2017/6/13 PM10:48:54
 */
public class ThreadPoolFutureTaskStudy {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        //ExecutorService threadPool = Executors.newCachedThreadPool();
        ExecutorService threadPool = Executors.newFixedThreadPool(2);

        //Future<Integer> rst01 = threadPool.submit(new Task("TASK-01"));
        //Future<Integer> rst02 = threadPool.submit(new Task("TASK-02"));
        //threadPool.shutdown();
        //Thread.sleep(3000);
        //System.out.println("TASK-01 : obtain result : " + rst01.get());
        //System.out.println("TASK-02 : obtain result : " + rst02.get());

        List<Future<String>> rstList = new LinkedList<Future<String>>();
        for (int i = 0; i < 5; i++) {
            Future<String> rst = threadPool.submit(new Task("TASK-0" + i));
            System.out.println("TASK-0" + i + " : result add into list begin");
            rstList.add(rst);
            System.out.println("TASK-0" + i + " : result add into list end");
        }
        threadPool.shutdown();

        /**
         * Even though the task submit and task run is divided asynchronously,
         * but the result obtain and the task run is not asynchronous, the
         * result obtain will be blocked before task completed. [Invoking get()
         * on this future will block until the associated request completes and
         * then return the result or throw any exception that occurred while
         * sending the record.]
         * The sequence of multiple tasks cannot be changed, so the sequence of
         * obtaining result cannot be changed too. For instance, If the task01
         * not completed, you cannot get the result of task06, even though
         * task06 is completed.
         * One word : the order of getting result is same as the order of
         * submitting task.
         */
        for (Future<String> rst : rstList)
            System.out.println(rst.get());
    }

    static class Task implements Callable<String> {
        private String taskName;

        public Task(String taskName) {
            this.taskName = taskName;
        }

        public String call() throws Exception {
            //System.out.println(taskName + " : START calculate");
            Thread.sleep(2000);
            int sum = new Random().nextInt(500);
            int rst = 0;
            for (int i = 0; i < sum; i++)
                rst += i;
            System.out.println(taskName + " : END calculate");
            return taskName + " : GET result : " + rst;
        }
    }
}
