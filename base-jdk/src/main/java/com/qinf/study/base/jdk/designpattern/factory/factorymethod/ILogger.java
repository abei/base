package com.qinf.study.base.jdk.designpattern.factory.factorymethod;

/**
 * Created by qinf on 2017/7/25 PM8:45:37
 */
public interface ILogger {

    public void writeLog();

}
