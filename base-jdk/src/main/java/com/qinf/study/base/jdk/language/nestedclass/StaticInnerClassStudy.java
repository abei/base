package com.qinf.study.base.jdk.language.nestedclass;

/**
 * WHY USE STATIC INNER CLASS
 * 1. When need combine a bunch of utility methods
 * together in a static inner class.
 * 2. When the static inner class just be used
 * by its outer class, and no need access its outer members. It's a cleaner
 * approach to create a static inner class, make it independent of its outer
 * class.
 * 3. From view of performance, reduce the cost of creating a instance of
 * outer class.
 * <p>
 * NOTE : we can regard static inner class as a class is declared by "final",
 * and with a private constructor. That means it cannot be extended by other
 * class, and cannot be instantiated by other client code.
 */
public class StaticInnerClassStudy {

    private static String outerStaticMemberVariable = "IT IS OUTER STATIC MEMBER VARIABLE";
    @SuppressWarnings("unused")
    private String outerInstantiatedMemberVariable = "IT IS OUTER INSTANTIATED MEMBER VARIABLE";

    private static void outerStaticMemberMethod() {
        System.out.println("IT IS OUTER STATIC MEMEBER METHOD");
    }

    public static void main(String[] args) {
        /**
         * For static inner class, it do not depend on outer class, so you can
         * use its static fields or static methods by its name directly.
         * Meanwhile, you can also use its static fields, static methods,
         * non-static fields or non-static methods by its instance.
         */
        StaticInnerClass.innerStaticMemberMethod();
        /**
         * compile error : Cannot make a static reference to the non-static
         * method innerInstantiatedMemberMethod() from the type
         * StaticInnerClassStudy.StaticInnerClass
         */
        //StaticInnerClass.innerInstantiatedMemberMethod();
        StaticInnerClass staitcInnerCls = new StaticInnerClass();
        staitcInnerCls.innerInstantiatedMemberMethod();
    }

    @SuppressWarnings("unused")
    private void outerInstantiatedMemberMethod() {
        System.out.println("IT IS OUTER INSTANTIATED MEMEBER METHOD");
    }

    static class StaticInnerClass {
        static String innerStaticMemberVariable = "IT IS INNER STATIC MEMBER VARIABLE";
        String innerInstantiatedMemberVariable = "IT IS INNER INSTANTIATED MEMBER VARIABLE";

        private static void innerStaticMemberMethod() {
            System.out.println("IT IS INNER STATIC MEMBER METHOD");
            /**
             * compile error : Cannot make a static reference to the non-static
             * method outerInstantiatedMemberMethod() from the type
             * StaticInnerClassStudy
             */
            // System.out.println("call outer instantiated member private method : ");
            // outerInstantiatedMemberMethod();
            /**
             * compile error : Cannot make a static reference to the non-static
             * field outerInstantiatedMemberVariable
             */
            // System.out.print("call outer instantiated member private variable : " + outerInstantiatedMemberVariable);
            System.out.println("call outer static member private method : ");
            outerStaticMemberMethod();
            System.out.print("call outer static member private variable : " + outerStaticMemberVariable);
        }

        private void innerInstantiatedMemberMethod() {
            System.out.println("IT IS INNER INSTANTIATED MEMBER METHOD");
        }
    }
}
