package com.qinf.study.base.jdk.language.polymorphism;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by qinf on 2020-11-23.
 */
public class TypeConversionStudy {

    public static void main(String... args) {
        Map<String, Object> params = new HashMap<>(1);
        params.put("k1", new Integer(1));
        params.put("k2", "v2");
        params.put("k4", "");
        /**
         * ClassCastException will be thrown. Accordingly, Integer type
         * from params<String, Object> cannot cast into String type.
         */
        //String convertedV1 = (String) params.get("k1");
        //String convertedV2 = (String) params.get("k2");

        String convertedV1 = String.valueOf(params.get("k1")); // 1
        String convertedV2 = String.valueOf(params.get("k2")); // v2
        String convertedV3 = String.valueOf(params.get("k3")); // null
        String convertedV4 = String.valueOf(params.get("k4")); // ""
        System.out.println(convertedV1);
    }
}
