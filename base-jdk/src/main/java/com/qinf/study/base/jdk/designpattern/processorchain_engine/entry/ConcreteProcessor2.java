package com.qinf.study.base.jdk.designpattern.processorchain_engine.entry;

import com.qinf.study.base.jdk.designpattern.processorchain_engine.TemplateProcessor;

/**
 * Created by qinf on 2021-10-02.
 */
public class ConcreteProcessor2 extends TemplateProcessor<ConcreteRequest, ConcreteResponse, ConcreteProcessorChain> {

    @Override
    public void doProcess(ConcreteRequest request, ConcreteResponse response, ConcreteProcessorChain chain) {
        // Your particular business logic
    }
}
