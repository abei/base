package com.qinf.study.base.jdk.language.collection;

import java.util.ArrayList;
import java.util.List;

/**
 * ArrayList can add NULL object. NULL is a member of every type.<p>
 * Created by qinf on 2017-11-2.
 */
public class ArrayListAddNULLStudy {

    public static void main(String... strings) {
        List<Object> list = new ArrayList<Object>();
        list.add("HELLO");
        list.add("WORLD");
        list.add(null);

        for (Object ele : list)
            System.out.println(ele);

    }
}
