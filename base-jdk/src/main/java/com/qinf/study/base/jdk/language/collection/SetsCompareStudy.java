package com.qinf.study.base.jdk.language.collection;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by qinf on 2020-06-19.
 */
public class SetsCompareStudy {

    public static void main(String... strings) {
        Set<String> kingPokerBox = new HashSet<>();
        kingPokerBox.add("J");
        kingPokerBox.add("Q");
        kingPokerBox.add("K");
        kingPokerBox.add("King");

        Set<String> queenPokerBox = new HashSet<>();
        queenPokerBox.add("Q");
        queenPokerBox.add("K");
        queenPokerBox.add("A");
        queenPokerBox.add("Queen");

        Set<String> duplicateCards = new HashSet<>(kingPokerBox);
        duplicateCards.retainAll(queenPokerBox);
        System.out.println("The duplicate cards is: " + duplicateCards);

        System.out.println("The king poker box remains before eliminating duplicate cards: " + kingPokerBox);
        kingPokerBox.removeIf(duplicateCards::contains);
        System.out.println("The king poker box remains after eliminating duplicate cards: " + kingPokerBox);

        System.out.println("The queen poker box remains before eliminating duplicate cards: " + queenPokerBox);
        queenPokerBox.removeIf(duplicateCards::contains);
        System.out.println("The queen poker box remains after eliminating duplicate cards: " + queenPokerBox);
    }
}
