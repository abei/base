package com.qinf.study.base.jdk.language.piecemeal;

import java.util.HashMap;
import java.util.Map;

public class TernaryOpeStudy {
    static int b;

    public static void main(String[] args) {
        //Person person = new Person("111", null);
        //Person person = null;
        Map<Object, Object> map = new HashMap<Object, Object>();
        map.put("id", "111");
        map.put("name", "qinf");
        //System.out.println(map.get(person.name));

        int a = 3;
        b = (1 < a && a < 3) ? a : 100;
        System.out.println(b);

        /**
         * Here, should guarantee consistent of return type of
         * ternary operation, otherwise, will throw a runtime
         * exception when the code is running.
         */
        int x = 0;
        //Integer x = 0;
        Integer y = x == 0 ? getId() : x;
        System.out.println(y);
    }

    private static Integer getId() {
        return null;
    }

    static class Person {
        public final String id;
        public final String name;

        public Person(String id, String name) {
            super();
            this.id = id;
            this.name = name;
        }
    }
}
