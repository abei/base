package com.qinf.study.base.jdk.language.reflectproxy.multiple;

import java.lang.reflect.Proxy;

/**
 * Created by qinf on 2020-10-05.
 */
public class MultipleEmptyProcessor implements MultipleProcessor {

    private Integer priority;

    @Override
    public void process(Integer orderId) {
        System.out.println("Order: " + orderId + " is processing.");
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Override
    public int compareTo(Object obj) {
        if (obj instanceof Proxy) {
            MultipleEmptyProcessor sourceObj = (MultipleEmptyProcessor) MultipleProxyFactory.getSourceObj(obj);
            return this.priority - sourceObj.getPriority();
        }
        MultipleEmptyProcessor mtp = (MultipleEmptyProcessor) obj;
        return this.priority - mtp.getPriority();
    }
}
