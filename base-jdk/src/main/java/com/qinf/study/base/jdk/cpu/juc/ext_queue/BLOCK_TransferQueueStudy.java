package com.qinf.study.base.jdk.cpu.juc.ext_queue;

import java.util.Random;
import java.util.concurrent.*;

/**
 * Created by qinf on 2017/12/12 PM9:43:21
 * <p>
 * By transfer(e) to insert elements into the TransferQueue.
 * The transfer(e) will be blocked until the inserted element e consumed by other thread.
 */
public class BLOCK_TransferQueueStudy {

    public static void main(String... strings) {
        /** There is only a implementation class LinkedTransferQueue of Interface TransferQueue */
        TransferQueue<String> queue = new LinkedTransferQueue<String>();
        ExecutorService threadPool = Executors.newCachedThreadPool();
        threadPool.execute(new Producer("PRODUCER", queue));
        threadPool.execute(new Consumer("CONSUMER-01", queue));
        threadPool.execute(new Consumer("CONSUMER-02", queue));
        threadPool.shutdown();
    }

    static class Producer implements Runnable {
        private final String producerName;
        private final TransferQueue<String> queue;

        Producer(String producerName, TransferQueue<String> queue) {
            this.producerName = producerName;
            this.queue = queue;
        }

        @Override
        public void run() {
            System.out.println(producerName + " : start to transfer elements.");
            for (int i = 0; i < 4; i++) {
                try {
                    /**
                     * If a thread is waiting to consume the queue, transfer(e) will give e directly and immediately to this waiting thread.
                     * Otherwise, transfer(e) will insert e into queue, and block until e is consumed by other thread.
                     */
                    queue.transfer("product" + i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(producerName + " : transfer [" + "product" + i + "]");
            }
            System.out.println(producerName + " : transfer elements over.");
        }
    }

    static class Consumer implements Runnable {
        private static Random rand = new Random(System.currentTimeMillis());
        private final String consumerName;
        private final TransferQueue<String> queue;

        Consumer(String consumerName, TransferQueue<String> queue) {
            this.consumerName = consumerName;
            this.queue = queue;
        }

        @Override
        public void run() {
            System.out.println(consumerName + " : start to consume elements.");
            for (int i = 0; i < 2; i++) {
                String product = null;
                try {
                    product = queue.take();
                    TimeUnit.SECONDS.sleep(rand.nextInt(5));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(consumerName + " : has consumed [" + product + "]");
            }
            System.out.println(consumerName + " : consume elements over.");
        }
    }
}
