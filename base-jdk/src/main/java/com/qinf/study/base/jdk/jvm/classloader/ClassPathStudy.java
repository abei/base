package com.qinf.study.base.jdk.jvm.classloader;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;

/**
 * Created by qinf on 2017/12/30 AM10:45:25
 * <p>
 * NOTE : If the class is run on public JRE(includes JVM),
 * the path of public JRE is : C:\Program Files\Java\jre1.8.0_151\
 * the String.class imported in your class file is : C:\Program Files\Java\jre1.8.0_151\lib\rt.jar!/java/lang/String.class
 * NOTE : Of cause, you can also run your class on private JRE(also includes another JVM),
 * this time the path of private JRE is : C:\Program Files\Java\jdk1.8.0_151\jre\
 * the String.class imported in your class file is : C:\Program Files\Java\jdk1.8.0_151\jre\lib\rt.jar!/java/lang/String.class
 */
public class ClassPathStudy {

    public static void main(String[] args) {
        // jar:file:/C:/Program%20Files/Java/jdk1.8.0_151/jre/lib/rt.jar!/java/lang/String.class
        System.out.println(ClassPathUtil.getAbsoluteURLOfClass(String.class));
        // jar:file:/C:/Program%20Files/Java/jdk1.8.0_151/jre/lib/rt.jar!/java/lang/String.class
        System.out.println(ClassLoader.getSystemResource("java/lang/String.class"));
    }

    static class ClassPathUtil {
        /**
         * Get a absolute location url of a class file.
         */
        @SuppressWarnings("rawtypes")
        private static URL getAbsoluteURLOfClass(final Class cls) {
            URL classUrl = null;
            /** java.lang.String -> java/lang/String.class */
            final String clsAsResource = cls.getName().replace('.', '/').concat(".class");
            final ProtectionDomain pd = cls.getProtectionDomain();

            if (pd != null) {
                final CodeSource cs = pd.getCodeSource();
                /** 'cs' can be null depending on the class loader behavior. */
                if (cs != null)
                    classUrl = cs.getLocation();
                if (classUrl != null) {
                    /**
                     * Convert a code source location into a full class file
                     * location for some common cases.
                     */
                    if ("file".equals(classUrl.getProtocol())) {
                        try {
                            if (classUrl.toExternalForm().endsWith(".jar") || classUrl.toExternalForm().endsWith(".zip"))
                                classUrl = new URL("jar:".concat(classUrl.toExternalForm()).concat("!/").concat(clsAsResource));
                            else if (new File(classUrl.getFile()).isDirectory())
                                classUrl = new URL(classUrl, clsAsResource);
                        } catch (MalformedURLException ignore) {
                        }
                    }
                }
            }

            if (classUrl == null) {
                /**
                 * Try to find 'cls' definition as a resource; this is not
                 * document to be legal, but Sun's implementations seem to allow
                 * this.
                 */
                final ClassLoader clsLoader = cls.getClassLoader();
                classUrl = clsLoader != null ? clsLoader.getResource(clsAsResource)
                        : ClassLoader.getSystemResource(clsAsResource);
            }
            return classUrl;
        }

        /**
         * Get a absolute path of a class file :
         * 1. The class file can be imported from JDK API(e.g., java.lang.String, etc ).
         * 2. The class file can be created by yourself.
         * 3. The class file can come from the third-part tool class.
         */
        @SuppressWarnings("rawtypes")
        public static String getAbsolutePathOfClass(Class cls) throws IOException {
            String classPath = null;
            URL url = getAbsoluteURLOfClass(cls);
            if (url != null) {
                classPath = url.getPath();
                if ("jar".equalsIgnoreCase(url.getProtocol())) {
                    try {
                        classPath = new URL(classPath).getPath();
                    } catch (MalformedURLException e) {
                    }
                    int location = classPath.indexOf("!/");
                    if (location != -1)
                        classPath = classPath.substring(0, location);
                }
                File file = new File(classPath);
                classPath = file.getCanonicalPath();
            }
            return classPath;
        }

        /**
         * Find a absolute path of a file by its relative path.
         * <p>
         * The implementation process as following :
         * A reference class to file "text.txt" is : "Test.class".
         * The relative path of "test.txt" is : "../../resource/test.txt".
         * Then you can get the absolute path of the file "text.txt".
         */
        @SuppressWarnings("rawtypes")
        public static String getAbsolutePathOfFile(String relativePath, Class referenceCls) throws IOException {
            String fileAbsolutePath = null;
            if (referenceCls == null) {
                throw new NullPointerException();
            }
            String referenceClsPath = getAbsolutePathOfClass(referenceCls);
            File clsFile = new File(referenceClsPath);
            String tmpPath = clsFile.getParent() + File.separator + relativePath;
            fileAbsolutePath = new File(tmpPath).getCanonicalPath();
            return fileAbsolutePath;
        }
    }
}
