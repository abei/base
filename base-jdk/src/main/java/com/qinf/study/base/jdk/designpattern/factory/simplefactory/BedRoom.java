package com.qinf.study.base.jdk.designpattern.factory.simplefactory;

/**
 * Created by qinf on 2017/7/24 PM9:43:34
 */
public class BedRoom extends Room {

    @Override
    public void extralFunction() {
        System.out.println("The room can be used for resting.");
    }

}
