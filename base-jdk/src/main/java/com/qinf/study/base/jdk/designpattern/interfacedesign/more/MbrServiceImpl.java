package com.qinf.study.base.jdk.designpattern.interfacedesign.more;

/**
 * Created by qinf on 2018/1/29 PM10:41:15
 */
public class MbrServiceImpl extends AService {

    @SuppressWarnings("unused")
    @Override
    protected String doProcess(IRequest request) {
        // TODO Auto-generated method stub
        System.out.println("Do process mbr reqeust service.");
        IRequest mbrRequest = this.handleMbrTypeRequest(request);
        return null;
    }

}
