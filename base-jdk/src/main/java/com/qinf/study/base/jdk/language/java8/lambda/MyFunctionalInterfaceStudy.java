package com.qinf.study.base.jdk.language.java8.lambda;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by qinf on 2017/11/25 PM9:48:42
 * <p>
 * 1. Lambda Expression just can only existed in Functional Interface context.
 * <p>
 * 2. Lambda Expression <-> Functional Interface
 * 2.1. Lambda Expression can be considered as a instance of a Implementation class
 * of a Functional Interface.<br>
 *
 * 2.2. Lambda Expression implemented the abstract method in a Functional Interface,
 * So its signature[descriptor] must match the abstract method.
 * For example, The abstract method in Functional Interface is as : public int method(Map map);
 * Then the lambda expression should be like : (map) -> int.<br>
 *
 * 2.3. Lambda Expression always used as a argument and passed into a method.
 */
public class MyFunctionalInterfaceStudy {

    public static int getMapSize(Map<?, ?> map, MyFunctionalInterface myFI) throws Exception {
        return myFI.mapSize(map);
    }

    public static void main(String... strings) throws Exception {
        Map<String, String> realMap = new HashMap<String, String>();
        realMap.put("k1", "v1");
        realMap.put("k2", "v2");

        int mapEntrySetSize = MyFunctionalInterfaceStudy.getMapSize(realMap,
                (Map<?, ?> map) -> map.entrySet().size());
        int mapKeySetSetSize = MyFunctionalInterfaceStudy.getMapSize(realMap,
                (Map<?, ?> map) -> map.keySet().size());
    }
}
