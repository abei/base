package com.qinf.study.base.jdk.language.java8.parallel;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Created by qinf on 2017/12/2 PM9:58:35
 * <p>
 * For now, I just know 2 ways to create parallel stream.
 * 1. by Stream.paralle(), convert a sequential stream to a parallel stream.
 * 2. by Collection.parallelStream(), get a parallel stream from collection source.
 * <p>
 * NOTE 1 : parallel stream can not guarantee the order of elements.
 * NOTE 2 : thread safety issues should rise your attentions.
 */
public class CreateParallelStreamStudy {

    @SuppressWarnings({"rawtypes", "unused"})
    public static void main(String... strings) {
        Stream parallelStream01 = Arrays.asList(new String[]{"HELLO", "WORLD", "FEN"}).parallelStream();
        Stream parallelStream02 = Stream.of("HELLO", "WORLD", "FEN").parallel();
    }
}
