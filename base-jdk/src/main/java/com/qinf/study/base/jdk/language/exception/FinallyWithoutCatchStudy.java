package com.qinf.study.base.jdk.language.exception;

/**
 * Created by qinf on 2019-07-23.
 */
public class FinallyWithoutCatchStudy {

    /**
     * Even throws ArithmeticException in try block, the finally block
     * still can be executed.
     */
    public static void main(String... strings) {
        try {
            int i = 10;
            int j = i / 0;
            System.out.println("Try statement.");
        } finally {
            System.out.println("Finally statement.");
        }
    }
}
