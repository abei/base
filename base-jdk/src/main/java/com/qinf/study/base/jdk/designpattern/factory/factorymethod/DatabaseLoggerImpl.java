package com.qinf.study.base.jdk.designpattern.factory.factorymethod;

/**
 * Created by qinf on 2017/7/25 PM8:46:27
 */
public class DatabaseLoggerImpl implements ILogger {

    @Override
    public void writeLog() {
        System.out.println("Log output to database.");
    }

}
