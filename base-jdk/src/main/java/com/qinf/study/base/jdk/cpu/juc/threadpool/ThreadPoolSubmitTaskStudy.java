package com.qinf.study.base.jdk.cpu.juc.threadpool;

import java.util.concurrent.*;

/**
 * Created by qinf on 2017/6/13 PM10:48:54
 */
public class ThreadPoolSubmitTaskStudy {

    @SuppressWarnings({"unchecked", "rawtypes"})
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService nonScheduledThreadPool = Executors.newFixedThreadPool(5);

        /** way 1 : submit task by execute() method */
        TaskRunnable taskRunnable = new TaskRunnable();
        nonScheduledThreadPool.execute(taskRunnable);

        /**
         * way 2 : submit task by submit() method
         * can submit Callable by submit(taskCallable),
         * can also submit Runnable by submit(taskRunnable)
         */
        TaskCallable taskCallable = new TaskCallable();
        Future future = nonScheduledThreadPool.submit(taskCallable);
        System.out.println((String) future.get());

        nonScheduledThreadPool.shutdown();

        /**
         * NOTE : the difference between way1 and way2 is :
         * 1. The way1 without result after task completed.
         * 2. The way1 cannot receive instance of Callable.
         */
    }

    private static class TaskRunnable implements Runnable {
        public void run() {
            System.out.println("[" + Thread.currentThread().getName() + "] runnable task start");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("[" + Thread.currentThread().getName() + "] runnable task end");
        }
    }

    @SuppressWarnings("rawtypes")
    private static class TaskCallable implements Callable {
        public Object call() throws Exception {
            System.out.println("[" + Thread.currentThread().getName() + "] callable task start");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("[" + Thread.currentThread().getName() + "] callable task end");
            return "calculate is over";
        }
    }
}
