package com.qinf.study.base.jdk.cpu.juc.base_lock;

/**
 * Created by qinf on 2017/12/17 PM10:03:50
 * <p>
 * About AQS, there are 3 keys need pay your attention.
 * 1. What is AQS [AbstractQueuedSynchronizer.class]
 * (1). AQS is the base of all kinds of locks and sync tools in JUC.
 * (2). More specifically, AQS supplies a framework of ExclusiveLock & SharedLock,
 * then next, ExclusiveLock and SharedLock can implement other more specific
 * locks and sync tools.
 * <p>
 * 2. The Theory of AQS
 * (1). The state and its modification of a lock[AQS]
 * You have to change the state in atomic way, so the volatile and CAS
 * is your better choice.
 * 1> If state is 0 means no thread acquired the lock[AQS].
 * 2> If state is not 0 means the lock has been acquired by other thread.
 * See the following code :
 * private volatile int state;
 * // It is atomic operation.
 * protected final int getState() {
 * return state;
 * }
 * // It is atomic operation.
 * protected final void setState(int newState) {
 * state = newState;
 * }
 * // It is atomic operation.
 * protected final boolean compareAndSetState(int expect, int update) {
 * return unsafe.compareAndSwapInt(this, stateOffset, expect, update);
 * }
 * (2). LockSupport in AQS
 * Block or unblock some thread in AQS.
 * (3). Sync Queue in AQS
 * The sync Queue is to hold blocking thread in Node format.
 * private transient volatile Node head;
 * private transient volatile Node tail;
 * +------+  prev +-----+       +-----+
 * head |      | <---- |     | <---- |     |  tail
 * +------+       +-----+       +-----+
 * <p>
 * 3. Use AQS
 * If you want implement AQS, you should override its 5 methods :
 * (1). protected boolean tryAcquire(int arg)
 * (2). protected boolean tryRelease(int arg)
 * (3). protected int tryAcquireShared(int arg)
 * (4). protected boolean tryReleaseShared(int arg)
 * (5). protected boolean isHeldExclusively()
 */
public class BaseOfLock_VolatileCASLockSupport2AQS_Study {

}
