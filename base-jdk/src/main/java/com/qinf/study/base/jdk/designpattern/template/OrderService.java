package com.qinf.study.base.jdk.designpattern.template;

/**
 * Created by qinf on 2021-09-13.
 */
public interface OrderService {

    Boolean upsertOrder(String orderEntity);

    Boolean delegateUpsertOrder(String orderEntity, String extraOperation);
}
