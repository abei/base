package com.qinf.study.base.jdk.designpattern.interfacedesign.more;

/**
 * Created by qinf on 2018/1/29 PM10:41:03
 * <p>
 * If a non-abstract class extends abstract class, it has to supply all method
 * implementations of the abstract class.
 * If a non-abstract class implements interface, it also has to supply all method
 * implementations of the interface.
 */
public class OdrServiceImpl extends AService {

    @SuppressWarnings("unused")
    @Override
    protected String doProcess(IRequest request) {
        // TODO Auto-generated method stub
        System.out.println("Do process odr reqeust service.");
        IRequest odrRequest = this.handleOdrTypeRequest(request);
        return null;
    }

}
