package com.qinf.study.base.jdk.language.java8.lambda.functiondesign;

/**
 * Created by qinf on 2018/4/1 PM3:32:46
 * <p>
 * The abstract class is still play template role :
 * 1. Encapsulate common function for usage of subclass.
 * 2. Regulate common flow of business logic.
 * <p>
 * NOTE : the abstract class not implements functional interface, that can be say
 * it's a special point of FunctionalInterface.
 */
public abstract class UniFunctionInvoker {
    protected final boolean flag;

    protected UniFunctionInvoker(boolean flag) {
        this.flag = flag;
    }

    protected abstract void before();

    protected abstract void after();

    public <A, R> R invoke(A a, UniFunction<A, R> f) {
        before();
        try {
            return flag ? _finally(a, f) : _invoke(a, f);
        } finally {
            after();
        }
    }

    private <A, R> R _finally(A a, UniFunction<A, R> f) {
        R r = f.invoke(a);
        return r;
        // or directly return f.invoke(a)
    }

    private <A, R> R _invoke(A a, UniFunction<A, R> f) {
        R r = f.invoke(a);
        return r;
    }
}
