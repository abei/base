package com.qinf.study.base.jdk.network.sock;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by qinf on 2021-09-08.
 */
public class Client {

    private static Socket clientSocket;
    private static PrintWriter out;
    private static BufferedReader in;

    static void createConn(String ip, int port) throws IOException {
        clientSocket = new Socket(ip, port);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }

    static String sendMessage(String msg) throws IOException {
        out.println(msg);
        String resp = in.readLine();
        return resp;
    }

    static void closeConn() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
    }
}
