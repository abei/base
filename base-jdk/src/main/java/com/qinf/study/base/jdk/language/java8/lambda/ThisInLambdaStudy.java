package com.qinf.study.base.jdk.language.java8.lambda;

import java.util.function.Function;

/**
 * Created by qinf on 2019-01-22.
 *
 * When use key "this" in lambda expression, it is among the current class context, means it
 * point to the current class. If the key "this" used in anonymous inner class, it will point
 * to the anonymous inner class.
 */
public class ThisInLambdaStudy {

    private String contentInCurrentClass = "DREAM ";

    private <T> void commonSpeak(T t, Function customizer) {
        System.out.println("HELLO " + customizer.apply(t));
    }

    public void doSpeak() {
        commonSpeak("WORLD", content -> content + "!");
        commonSpeak(null, content -> this.contentInCurrentClass.trim() + "!");
    }

    public static void main(String... strings) {
        ThisInLambdaStudy invoker = new ThisInLambdaStudy();
        invoker.doSpeak();
    }
}
