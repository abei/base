package com.qinf.study.base.jdk.language.piecemeal;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by qinf on 2020-08-07.
 */
public class CharacterStudy {

    /**
     * Primitive to wrapper of eight class types:
     * short   -> Short.class
     * int     -> Integer.class
     * long    -> Long.class
     * float   -> Float.class
     * double  -> Double.class
     * char    -> Character.class
     * byte    -> Byte.class
     * boolean -> Boolean.class
     */
    public static void main(String... strings) {
        char key1 = 'a';
        char key2 = 'b';
        Character key3 = 'b';
        Map<Character, Integer> map = new HashMap<>(2);
        map.put(key1, 10);
        map.put(key2, 20);
        map.put(key3, 30);
        map.entrySet().stream().forEach(System.out::println);
    }
}
