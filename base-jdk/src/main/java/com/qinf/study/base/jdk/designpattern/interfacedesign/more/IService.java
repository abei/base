package com.qinf.study.base.jdk.designpattern.interfacedesign.more;

/**
 * Created by qinf on 2018/1/29 PM10:13:48
 * <p>
 * Interface supply a constraint for implementation class. That is to say all
 * implementation class have to implement all the abstract method in the
 * interface.
 * <p>
 * NOTE : But, abstract is not required.
 */
public interface IService {
    //public abstract String processData(String params);
    public abstract String process(IRequest request);
}
