package com.qinf.study.base.jdk.language.reflectproxy.cglibmulti;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created by qinf on 2020-10-05.
 */
public class CglibProcessorChain {

    static Set<CglibEmptyProcessor> cglibProcessorChain = new TreeSet<>();

    public static void main(String... strings) {
        CglibProxyFactory cglibProxyFactory = new CglibProxyFactory();
        CglibEmptyProcessor cglibEmptyProcessor1 = new CglibEmptyProcessor();
        cglibEmptyProcessor1.setPriority(1);
        CglibEmptyProcessor cglibEmptyProcessor2 = new CglibEmptyProcessor();
        cglibEmptyProcessor2.setPriority(2);
        CglibEmptyProcessor cglibEmptyProcessor3 = new CglibEmptyProcessor();
        cglibEmptyProcessor3.setPriority(3);
        CglibEmptyProcessor cglibProcessor1 = (CglibEmptyProcessor) cglibProxyFactory.getProxyObj(cglibEmptyProcessor1);
        CglibEmptyProcessor cglibProcessor2 = (CglibEmptyProcessor) cglibProxyFactory.getProxyObj(cglibEmptyProcessor2);
        CglibEmptyProcessor cglibProcessor3 = (CglibEmptyProcessor) cglibProxyFactory.getProxyObj(cglibEmptyProcessor3);

        cglibProcessorChain.add(cglibProcessor1);
    }
}
