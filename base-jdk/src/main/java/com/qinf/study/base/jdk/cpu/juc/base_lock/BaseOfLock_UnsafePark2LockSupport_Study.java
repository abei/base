package com.qinf.study.base.jdk.cpu.juc.base_lock;

/**
 * Created by qinf on 2017/12/31 PM3:10:32
 * <p>
 * It is not convenient to use Unsafe.park() to block running thread directly,
 * so there is a tool class "LockSupport" in JUC to encapsulate the park() in
 * Unsafe.class.
 */
public class BaseOfLock_UnsafePark2LockSupport_Study {

}
