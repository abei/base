package com.qinf.study.base.jdk.cpu.thread;

import java.util.concurrent.TimeUnit;

/**
 * Created by qinf on Jul 5, 2018.
 * <p>
 * NOTE : As we know, the state of a thread can be : NEW, RUNNABLE, BLOCKED, WAITING, TIMED_WAITING, TERMINATED.
 * So, here, the "ThreadWailt" means BLOCKED and WAITING.
 */
public class InterruptWaitingThreadStudy {

    public static void main(String[] args) throws InterruptedException {
        // step 1.create new thread
        Thread waitingThread = new Thread(new ThreadWaiting(), "waiting thread");
        System.out.println("[" + waitingThread.getName() + "] life status is : [" + waitingThread.getState()
                + "], interrupt status is : " + "[" + waitingThread.isInterrupted() + "]");

        // step 2.start the new thread
        waitingThread.start();
        System.out.println("[" + waitingThread.getName() + "] life status is : [" + waitingThread.getState()
                + "], interrupt status is : " + "[" + waitingThread.isInterrupted() + "]");

        TimeUnit.SECONDS.sleep(2);
        // step 4.main thread perform interrupt instruction, this time waitingThread is in blocking status
        waitingThread.interrupt();
        System.out.println("[" + waitingThread.getName() + "] life status is : [" + waitingThread.getState()
                + "], interrupt status is : " + "[" + waitingThread.isInterrupted() + "]");
    }

    static class ThreadWaiting implements Runnable {
        public void run() {
            try {
                System.out.println("{before wait" + "} - [" + Thread.currentThread().getName()
                        + "] life status is : [" + Thread.currentThread().getState() + "], interrupt status is : "
                        + "[" + Thread.currentThread().isInterrupted() + "]");
                // step 3. invoke wait(), make current thread into WAITING state.
                synchronized (this) {
                    wait(); // NOTE : If you want invoke wait(), first you should hold a lock(monitor), that means you
                    // need to guarantee wait() included in synchronized block or method before you invoke it.
                }
                System.out.println("{after wait() " + "} - [" + Thread.currentThread().getName()
                        + "] life status is : [" + Thread.currentThread().getState() + "], interrupt status is : "
                        + "[" + Thread.currentThread().isInterrupted() + "]");
            } catch (InterruptedException e) {
                System.out.println("{catch exception} - [" + Thread.currentThread().getName() + "] life status is : ["
                        + Thread.currentThread().getState() + "], interrupt status is : " + "["
                        + Thread.currentThread().isInterrupted() + "]");
            }
        }
    }
}
