package com.qinf.study.base.jdk.cpu.juc.threadpool;

import java.util.Random;
import java.util.concurrent.*;

/**
 * Created by qinf on 2017/6/26 PM10:49:09
 */
public class ThreadPoolCompletionServiceStudy {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        //ExecutorService threadPool = Executors.newCachedThreadPool();
        ExecutorService threadPool = Executors.newFixedThreadPool(2);
        CompletionService<String> cmpService = new ExecutorCompletionService<String>(threadPool);

        for (int i = 0; i < 5; i++)
            cmpService.submit(new Task("TASK-0" + i));
        threadPool.shutdown();

        /**
         * When use CompletionService, the order of obtaining result is same as
         * the order of completion, it is nothing to do with the order of
         * submitting task.
         * Once the task is completed, just get its result. So, the main thread will not be blocked
         * until all tasks completed.
         */
        for (int i = 0; i < 5; i++)
            System.out.println(cmpService.take().get());
    }

    static class Task implements Callable<String> {
        private String taskName;

        public Task(String taskName) {
            this.taskName = taskName;
        }

        public String call() throws Exception {
            Thread.sleep(2000);
            int sum = new Random().nextInt(500);
            int rst = 0;
            for (int i = 0; i < sum; i++)
                rst += i;
            System.out.println(taskName + " : END calculate");
            return taskName + " : GET result : " + rst;
        }
    }
}
