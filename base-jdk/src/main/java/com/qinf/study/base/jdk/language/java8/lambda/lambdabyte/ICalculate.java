package com.qinf.study.base.jdk.language.java8.lambda.lambdabyte;

import java.io.IOException;

/**
 * Created by qinf on 2018-12-23.
 */
@FunctionalInterface
public interface ICalculate {

    int operate(int x, int y) throws IOException;
}
