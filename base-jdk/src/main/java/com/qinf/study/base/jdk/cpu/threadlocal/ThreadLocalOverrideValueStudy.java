package com.qinf.study.base.jdk.cpu.threadlocal;

import java.util.concurrent.TimeUnit;

/**
 * Created by qinf on 2019-01-29.
 */
public class ThreadLocalOverrideValueStudy {
    private static final ThreadLocal<String> TRANSIENT_VALUE_CACHE = new ThreadLocal<>();

    public static void main(String... args) {
        for (int i = 0; i < 1; i++) {
            new Thread(() -> {
                TRANSIENT_VALUE_CACHE.set("HELLO");
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                /**
                 * The second value will override the previous value.
                 */
                TRANSIENT_VALUE_CACHE.set("WORLD");
                System.out.println(TRANSIENT_VALUE_CACHE.get());
            }).start();
        }
    }
}
