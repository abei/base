package com.qinf.study.base.jdk.language.java8;

import java.util.Objects;

/**
 * Created by qinf on 2018/03/12 PM9:09:23
 */
public class ObjectsStudy {

    public static void main(String... strings) {
        /**
         * The difference between Objects.requireNonNull(obj, Supplier
         * <String> message) and Objects.requireNonNull(obj, String message) is :
         * For 1st method, the string message will not created until the checked obj is NULL.
         * For 2nd method, the string message will created directly, no matter the obj is NULL or not.
         * The performance of 1st method is better than the 2nd method.
         */
        String userInputMessage = Objects.requireNonNull("UserInputMessage", () -> {
            return "Your input is NULL, please check it again.";
        });
        System.out.println(userInputMessage);
    }
}
