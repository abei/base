package com.qinf.study.base.jdk.language.polymorphism;

public abstract class Son implements Father {

    public void iSay() {
        System.out.println("son say hello world");
    }

    public void iDo() {
        System.out.println("son do hello world");
        ;
    }

    /**
     * Even there is no abstract method is ok.
     */
    public abstract void sonMethod();

}
