package com.qinf.study.base.jdk.designpattern.factory.simplefactory;

/**
 * Created by qinf on 2017/7/24 PM9:55:38
 */
public class SimpleFactoryPatternStudy {

    public static void main(String[] args) {
        Room concreteRoom = RoomFactory.getRoom("BedRoom");
        concreteRoom.live();
        concreteRoom.extralFunction();
    }
}
