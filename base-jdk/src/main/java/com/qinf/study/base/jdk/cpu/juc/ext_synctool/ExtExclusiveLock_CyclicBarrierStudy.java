package com.qinf.study.base.jdk.cpu.juc.ext_synctool;

import java.util.Random;
import java.util.concurrent.*;

/**
 * Created by qinf on 2017/12/5 PM9:52:43
 * <p>
 * In fact, CyclicBarrier implemented by ReentrantLock and Condition.
 * So, say it extends the ExclusiveLock is okay.
 * CyclicBarrier is a loop way of CountDownLatch.
 */
public class ExtExclusiveLock_CyclicBarrierStudy {

    public static void main(String... strings) throws InterruptedException {
        ExecutorService threadPool = Executors.newFixedThreadPool(3);
        /**
         * NOTE 1 : new CyclicBarrier(3, runnable) :
         * 	3 means the barrier will block 3 threads before each of them completes its own task.
         * 	runnable means all of the 3 threads complete their tasks will trigger the runnable.
         */
        CyclicBarrier cycBarrier = new CyclicBarrier(2, () -> {
            System.out.println("All travelers have arrived a same city, wake up them all and go to next city.\n");
        });
        for (int i = 0; i < 3; i++)
            threadPool.submit(new Traveler("TRAVELER-" + i, cycBarrier));
        threadPool.shutdown();
        threadPool.awaitTermination(1L, TimeUnit.SECONDS);
    }

    private static class Traveler implements Runnable {
        private static Random randomer = new Random(10);
        private final String tName;
        private final CyclicBarrier cycBarrier;

        Traveler(String tName, CyclicBarrier cycBarrier) {
            this.tName = tName;
            this.cycBarrier = cycBarrier;
        }

        @Override
        public void run() {
            try {
                TimeUnit.SECONDS.sleep(randomer.nextInt(5));
                System.out.println(tName + " has arrived at BJ -> block right now.");
                /** NOTE 2 : The thread blocked voluntarily, and you should find there is no wake up method. */
                cycBarrier.await();
                TimeUnit.SECONDS.sleep(randomer.nextInt(5));
                System.out.println(tName + " has arrived at SH -> block right now.");
                cycBarrier.await(); // A same thread blocked twice, that is to say CyclicBarrier is in loop way.
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }
}
