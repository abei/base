package com.qinf.study.base.jdk.cpu.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * Created by qinf on 2017/12/26 PM10:31:58
 * <p>
 * NOTE: If all the user threads exited, the daemon thread will exited immediately,
 *       even through its task doesn't complete.
 * NOTE: If the a thread is generated from daemon thread, then the thread is also a daemon thread.
 */
public class DaemonThreadStudy {

    public static void main(String... strings) throws InterruptedException {
        ExecutorService threadPool = Executors.newCachedThreadPool(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                /**
                 * 1. The thread "t" is created by main-thread. Default, the thread "t"
                 *    is non-daemon thread.
                 * 2. If you want set "t" as daemon, you must set it by "t.setDaemon(true)"
                 *    explicitly, then, if main-thread exit, it will exit immediately.
                 */
                //Thread t = new Thread(r, "NON-DAEMON-THREAD");
                Thread t = new Thread(r, "DAEMON-THREAD");
                t.setDaemon(true);
                return t;
            }
        });
        threadPool.execute(() -> {
            for (int j = 0; j < 10; j++) {
                System.out.println(Thread.currentThread().getName() + " is print : " + j);
                try {
                    TimeUnit.MILLISECONDS.sleep(500);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        threadPool.shutdown();
        threadPool.awaitTermination(1L, TimeUnit.SECONDS);

        for (int i = 0; i < 5; i++) {
            System.out.println(Thread.currentThread().getName() + " is print : " + i);
            TimeUnit.MILLISECONDS.sleep(500);
        }
    }

    /**
     * The run result :
     * 1. If thread "t" is daemon thread, the result may be as follow(it can not print all the variables) :
     * DAEMON-THREAD is print : 0
     * DAEMON-THREAD is print : 1
     * main is print : 0
     * DAEMON-THREAD is print : 2
     * main is print : 1
     * DAEMON-THREAD is print : 3
     * main is print : 2
     * DAEMON-THREAD is print : 4
     * main is print : 3
     * DAEMON-THREAD is print : 5
     * main is print : 4
     * DAEMON-THREAD is print : 6
     *
     * 2. If thread "t" is not daemon thread, the result may be as follow(it will print all the variables) :
     * NON-DAEMON-THREAD is print : 0
     * NON-DAEMON-THREAD is print : 1
     * main is print : 0
     * NON-DAEMON-THREAD is print : 2
     * main is print : 1
     * NON-DAEMON-THREAD is print : 3
     * main is print : 2
     * NON-DAEMON-THREAD is print : 4
     * main is print : 3
     * NON-DAEMON-THREAD is print : 5
     * main is print : 4
     * NON-DAEMON-THREAD is print : 6
     * NON-DAEMON-THREAD is print : 7
     * NON-DAEMON-THREAD is print : 8
     * NON-DAEMON-THREAD is print : 9
     */
}
