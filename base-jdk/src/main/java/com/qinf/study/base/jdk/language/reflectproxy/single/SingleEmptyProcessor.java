package com.qinf.study.base.jdk.language.reflectproxy.single;

/**
 * Created by qinf on 2020-10-05.
 */
public class SingleEmptyProcessor implements SingleProcessor {

    private Integer priority;

    @Override
    public void process(Integer orderId) {
        System.out.println("Order: " + orderId + " is processing.");
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
