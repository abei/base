package com.qinf.study.base.jdk.designpattern.processorchain_engine;

import java.util.Objects;

/**
 * Created by qinf on 2021-10-02.
 */
public abstract class TemplateProcessor<Q, P, C> implements Processor<Q, P, C> {

    protected Callback callback;

    @Override
    public void process(Q request, P response, C chain) {
        // Your own common business logic, e.g, params validation, etc.
        this.doProcess(request, response, chain);
        if (Objects.nonNull(callback)) {
            callback.callback(request, response, null);
        }
    }

    public abstract void doProcess(Q request, P response, C chain);

    @Override
    public Callback getCallback() {
        return this.callback;
    }
}
