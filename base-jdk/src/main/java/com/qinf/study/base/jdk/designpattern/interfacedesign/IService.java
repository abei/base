package com.qinf.study.base.jdk.designpattern.interfacedesign;

import java.util.Map;

public interface IService {

    public void process(
            Map<String, String> header,
            Map<String, String[]> parameters,
            Map<String, Object> attributes,
            Map<String, Object> session,
            String content,
            Map<String, Object> context);
}
