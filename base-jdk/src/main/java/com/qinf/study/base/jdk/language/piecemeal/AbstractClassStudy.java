package com.qinf.study.base.jdk.language.piecemeal;

/**
 * Created by qinf on 2017/7/16 PM9:45:23
 * <p>
 * 1. Abstract class can contain a constructor(with args or not), but it can not be
 * instantiated directly.
 * 2. If a subclass of a abstract class(contains constructor) is non-abstract, it have
 * to invoke this constructor of its father class by method - super();
 * By this way, you can assign values to the variables of this abstract class.
 * 3. If a subclass of a abstract class is non-abstract class, it have to implement all
 * the abstract methods. But if the subclass is also a abstract class, it is not
 * mandatory.
 * 4. Abstract class can contain instantial method, and can contain abstract
 * method(even there is no abstract method is ok).
 */
public class AbstractClassStudy {

}
