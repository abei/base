package com.qinf.study.base.jdk.language.java8.stream;

import java.util.stream.Stream;

/**
 * Created by qinf on 2017/11/26 PM10:31:51
 * <p>
 * Data reduction is : Combine all elements from a stream into a definite result.
 * For example, combine/concatenate all strings of a stream.
 * <p>
 * Stream.reduce() is a common reduction method in Stream API, it overloaded 3 methods :
 * 1. Optional<T> reduce(BinaryOperator<T> accumulator);
 * 2. T reduce(T identity, BinaryOperator<T> accumulator);
 * 3. <U> U reduce(U identity, BiFunction<U,? super T,U> accumulator, BinaryOperator<U> combiner);
 */
public class OperateStreamDataReductionByReduceStudy {

    public static void main(String... strings) {
        Stream<String> stringStream = Stream.of("HELLO", "WORLD", "STREAM");
        /**
         * The first argument is seed for data reduction.
         */
        String joinedString = stringStream.reduce("", (prevStr, nextStr) -> prevStr.concat(nextStr + ", "));
        System.out.println("The result of joined string is : " + joinedString);

        /**
         * Accumulate the length of each string of a stream by reduce()
         */
        Stream<String> stringCntLenStream = Stream.of("HE", "W", "STR");
        int sumLen = stringCntLenStream.reduce(0,
                (len, str) -> len + str.length(),
                (prevLen, nextLen) -> prevLen + nextLen);
        System.out.println("The result of sum length of all strings is : " + sumLen);
    }
}
