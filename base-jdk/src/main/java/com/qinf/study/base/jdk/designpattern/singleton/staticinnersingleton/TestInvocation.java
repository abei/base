package com.qinf.study.base.jdk.designpattern.singleton.staticinnersingleton;

/**
 * Created by qinf on 2021-02-01.
 */
public class TestInvocation {

    public static void main(String... strings) {
        System.out.println(StaticInnerClassSingletonStudy.getInstance());
    }
}
