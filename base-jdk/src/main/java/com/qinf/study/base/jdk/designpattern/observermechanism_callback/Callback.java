package com.qinf.study.base.jdk.designpattern.observermechanism_callback;

/**
 * Created by qinf on 2017/12/28 PM10:31:58
 * <p>
 * It is a callback interface.
 */
public interface Callback {

    /**
     * It is a callback method.
     */
    public void onCompletion(String result, Exception exception);
}
