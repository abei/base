package com.qinf.study.base.jdk.designpattern.factory.abstractfactory;

/**
 * Created by qinf on 2017/7/25 PM10:00:13
 */
public abstract class Room {
    public abstract void live();
}
