package com.qinf.study.base.jdk.cpu.juc.base_atomic;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by qinf on 2017/12/13 PM10:30:42
 * <p>
 * AtomicXx(AtomicInteger, AtomicReference, etc.) = CAS of Unsafe + volatile
 * <p>
 * If you want to change multiple fields by atomically, you can convert these fields
 * into a bean, than use AtomicReference to change them atomically.
 * <p>
 * Additionally, should watch out ABA problem (use AtomicStampedReference to solve it).
 */
public class AtomicReferenceStudy {
    public static void main(String... strings) throws InterruptedException {
        AtomicReference<MyObject> myAtomicObj = new AtomicReference<MyObject>(new MyObject(0, 0));
        ExecutorService threadPool = Executors.newCachedThreadPool();

        for (int i = 0; i < 5; i++) {
            threadPool.execute(() -> {
                for (int j = 0; j < 100; j++) {
                    boolean flag = false;
                    while (!flag) {
                        MyObject oldObj = myAtomicObj.get();
                        MyObject newObj = new MyObject(oldObj.field1 + 1, oldObj.field2 + 1);
                        flag = myAtomicObj.compareAndSet(oldObj, newObj);
                    }
                }
            });
        }
        /** This method does not wait for previously submitted tasks to complete execution. */
        threadPool.shutdown();
        /**
         * Blocks until all tasks have completed execution after a shutdown
         * request, or the timeout occurs, or the current thread is interrupted,
         * whichever happens first.
         */
        threadPool.awaitTermination(1L, TimeUnit.MINUTES);
        System.out.println("field1 = " + myAtomicObj.get().field1 + "\n" + "field2 = " + myAtomicObj.get().field2);
    }

    static class MyObject {
        int field1;
        int field2;

        MyObject(int f1, int f2) {
            this.field1 = f1;
            this.field2 = f2;
        }
    }
}
