package com.qinf.study.base.jdk.language.java8;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by qinf on 2018/4/15.
 */
public class ListToMapByCollectorsStudy {

    public static void main(String... strings) {
        Vector<String> vector = new Vector<String>();
        vector.add("QIN");
        vector.add("FEN");
        vector.add("NAN");
        vector.add("NAN");
        Enumeration<String> enume = vector.elements();

        /**
         * NOTE 1 : You cannot convert list to stream by Stream.of(list).
         * NOTE 2 : You have to make suer each KEY you passed in is unique.
         * or, you will get a Exception "java.lang.IllegalStateException: Duplicate key NAN_VALUE".
         * But, for VALUE, has no so restriction.
         */
        Map<String, String> map = Collections.list(enume).stream()
                .filter(e -> !Objects.equals(e, "QIN"))
                /**
                 * If you can not guarantee each KEY is unique, you can remove duplicate
                 * the VALUEs by the distinct() method.
                 */
                .distinct()
                //.collect(Collectors.toMap(e -> e.toString(), e -> "VALUE"));
                /**
                 * About method of Collectors.toMap(keyMapper, valueMapper), the valueMapper
                 * cannot be null, keyMapper is null or not is not mandatory. but method
                 * collect(HashMap::new,... has no this restriction.
                 */
                .collect(Collectors.toMap(e -> e.toString(), e -> e.concat("_VALUE")));
                //.collect(HashMap::new, (m, k) -> m.put(k, k + "-V"), HashMap::putAll);

        map.forEach((k, v) -> System.out.println(k + " : " + v));
    }
}
