package com.qinf.study.base.jdk.language.reflectproxy.single;

/**
 * Created by qinf on 2020-10-05.
 */
public interface SingleProcessor {

    void process(Integer orderId);

}
