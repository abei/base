package com.qinf.study.base.jdk.language.collection;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created by qinf on 2020-06-23.
 */
public class TreeSetCustomizedObjStudy {

    /**
     * All elements inserted into the tree set must be mutually comparable
     * by the specified comparator, or must implement the comparable interface,
     * otherwise "cannot be cast to java.lang.Comparable" will be triggered.
     * No need overrides the hashCode() and equals() at the same time.
     * Actually, the elements, are the keys of TreeMap, additionally, because
     * the key object of Map has overridden hashCode() and equals() Of Object.class,
     * so the element of TreeSet no need implements above two method.
     */
    static class Order implements Comparable<Order> {
        private Long orderId;
        private String orderType;

        @Override
        public int compareTo(Order otherOrder) {
            //return (int) (this.orderId - otherOrder.getOrderId());
            return (int) (otherOrder.getOrderId() - this.orderId);
        }

        public Long getOrderId() {
            return orderId;
        }

        public void setOrderId(Long orderId) {
            this.orderId = orderId;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        @Override
        public String toString() {
            return "Order{" +
                    "orderId=" + orderId +
                    ", orderType='" + orderType + '\'' +
                    '}';
        }
    }

    public static void main(String... strings) {
        Set<Order> orders = new TreeSet();

        Order order1 = new Order();
        order1.setOrderId(1L);
        order1.setOrderType("electron");

        Order order2 = new Order();
        order2.setOrderId(2L);
        order2.setOrderType("food");

        Order order3 = new Order();
        order3.setOrderId(3L);
        order3.setOrderType("book");

        Order order4 = new Order();
        order4.setOrderId(3L);
        order4.setOrderType("book");

        orders.add(order3);
        orders.add(order1);
        orders.add(order2);
        orders.add(order4);

        orders.forEach(System.out::println);

        /**
         * If Order class not implements Comparable interface, you can specify
         * a comparator when create set.
         */
        //Set<Order> orders = new TreeSet<>(Comparator.comparing(Order::getOrderId));
    }
}
