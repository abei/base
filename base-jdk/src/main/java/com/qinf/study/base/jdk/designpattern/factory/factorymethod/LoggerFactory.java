package com.qinf.study.base.jdk.designpattern.factory.factorymethod;

/**
 * Created by qinf on 2017/7/25 PM8:54:01
 */
public interface LoggerFactory {

    public ILogger createLogger();

}
