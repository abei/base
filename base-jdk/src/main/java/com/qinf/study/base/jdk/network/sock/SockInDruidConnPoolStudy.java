package com.qinf.study.base.jdk.network.sock;

import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

/**
 * Created by qinf on 2021-09-14.
 */
public class SockInDruidConnPoolStudy {

    public static void main(String... strings) throws Exception {
        Properties pros = new Properties();
        pros.put("driverClassName", "com.mysql.jdbc.Driver");
        pros.put("url", "jdbc:mysql://xx.0.xxx.xx:3306/db");
        pros.put("username", "username");
        pros.put("password", "password");
        pros.put("initialSize", "5");
        pros.put("maxActive", "10");
        pros.put("maxWait", "3000"); // mean the timeout of getting connection from conn pool
        DataSource ds = DruidDataSourceFactory.createDataSource(pros);
        /**
         * Because the initial size is 5, so 5 TCP connection will be established with server:
         * TCP: 52106 → 3306 [SYN] Seq=0 Win=64240 Len=0 MSS=1460 WS=256 SACK_PERM=1
         * TCP: 53434 → 3306 [SYN] Seq=0 Win=64240 Len=0 MSS=1460 WS=256 SACK_PERM=1
         * TCP: 53435 → 3306 [SYN] Seq=0 Win=64240 Len=0 MSS=1460 WS=256 SACK_PERM=1
         * TCP: 53437 → 3306 [SYN] Seq=0 Win=64240 Len=0 MSS=1460 WS=256 SACK_PERM=1
         * TCP: 53441 → 3306 [SYN] Seq=0 Win=64240 Len=0 MSS=1460 WS=256 SACK_PERM=1
         *
         * At druid conn pool, even the number of tcp connection is 5, but the number of jdbc conn
         * in pool always is (initialSize - 1), has no idea why. But you can check the connection by
         * SQL command "show processlist" from MySQL client, for instance there are two connections:
         *   {
         *     "Id": 2309,
         *     "User": "username",
         *     "Host": "your ip:56639",
         *     "db": "db",
         *     "Command": "Sleep",
         *     "Time": 4,
         *     "State": "",
         *     "Info": null
         *   },
         *   {
         *     "Id": 2310,
         *     "User": "username",
         *     "Host": "your ip:56641",
         *     "db": "db",
         *     "Command": "Sleep",
         *     "Time": 4,
         *     "State": "",
         *     "Info": null
         *   }
         * "Command": "Sleep" means it's a idle connection.
         *
         * connectTimeout: timeout for socket connect (in milliseconds), with 0 being no timeout. Only works on JDK-1.4
         *                 or newer. Defaults to '0'.
         * socketTimeout: timeout on network socket operations (0, the default means no timeout).
         * When the jvm exit, i.e., the conn pool instance is disappeared, the tcp connection with timeout 0 previously
         * used in conn pool is still alive, you can get tcp keepalive packet by Wireshark.
         */
        try (Connection conn = ds.getConnection();
              Statement stmt = conn.createStatement();
              ResultSet rs = stmt.executeQuery("SELECT version()");) {
            rs.next();
            System.out.println(rs.getString("version()"));
        } /*finally {
            rs.close();
            stmt.close();
            conn.close(); // A jdbc connection will return to connection pool
        }*/
    }
}
