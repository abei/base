package com.qinf.study.base.jdk.cpu.juc.base_lock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * Created by qinf on 2017/12/17 PM10:37:18
 * <p>
 * AQS also supplies a framework to implement shared lock.
 * <p>
 * So you can implement your own shared lock by AQS, override 3 methods in AQS
 * is also necessary. Next, shared lock can be used to implement other specific
 * locks or sync tools. E.g., ReentrantReadWriteLock, Semaphore, CountDownLatch
 * etc.
 * At this class, we simulate the sync tool CountDownLatch.
 */
public class ExtAQS_SharedLock_Study {

    private final Sync sync = new Sync();

    public static void main(String... strings) throws InterruptedException {
        final ExtAQS_SharedLock_Study sharedLock = new ExtAQS_SharedLock_Study();
        MyThread t1 = new MyThread("Thread-01", sharedLock);
        MyThread t2 = new MyThread("Thread-02", sharedLock);
        MyThread t3 = new MyThread("Thread-03", sharedLock);

        t1.start();
        t2.start();
        t3.start();

        TimeUnit.SECONDS.sleep(5);
        System.out.println("\nMain thread wakeup all blocking threads.");
        sharedLock.unlock();
        t1.join();
        t2.join();
        t3.join();
        System.out.println("All thread complete their works.");
    }

    public void sharedLock() {
        sync.acquireShared(1);
    }

    public void unlock() {
        sync.releaseShared(1);
    }

    private static class Sync extends AbstractQueuedSynchronizer {
        private static final long serialVersionUID = 1L;

        protected Sync() {
            //super();
            /**
             * Here, set the state of shared lock is "1". That will block all
             * threads that try to acquire this shared lock. This is because if
             * state==1 means the shared lock has been acquired by other thread.
             */
            setState(1);
        }

        @Override
        protected int tryAcquireShared(int ignore) {
            /**
             * If return positive value, means the shared lock has been got by current thread.
             * If return 0, means the shared lock has not yet been got by current thread.
             * If return negative value, means the current thread failed to get the shared lock, it should be blocked.
             */
            //return compareAndSetState(0, 1) ? 1 : -1;
            return getState() == 0 ? 1 : -1;
        }

        @Override
        protected boolean tryReleaseShared(int ignore) {
            /*
             * setState(0);
			 * return true;
			 */
            /**
             * The CAS state operation is included in for statement. That can
             * make sure all threads blocking on the shared lock wakes up.
             */
            for (; ; ) {
                if (compareAndSetState(1, 0))
                    return true;
            }
        }
    }

    private static class MyThread extends Thread {
        private final String tName;
        private final ExtAQS_SharedLock_Study myLock;

        private MyThread(String tName, ExtAQS_SharedLock_Study myLock) {
            this.tName = tName;
            this.myLock = myLock;
        }

        @Override
        public void run() {
            try {
                System.out.println(tName + " is acquiring the sharedLock...");
                myLock.sharedLock();
                System.out.println(tName + " has acquired the sharedLock.");
                TimeUnit.SECONDS.sleep(2);
                System.out.println(tName + " has completed its work.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } /*finally {
                myLock.unlock();
				System.out.println(tName + " has released the sharedLock.");
			}*/
        }
    }
}
