package com.qinf.study.base.jdk.cpu.thread;

import java.util.stream.Stream;

/**
 * Created by qinf on 2018/1/30 PM8:24:09
 * <p>
 * Thread.getStackTrace() will return a Java invoking stack contains all
 * method-invoked stack frame in invoked order of the thread. By this way, you
 * can get the method name of the current invoking method, or the method name of
 * the invoker of current method.
 * <p>
 * The detail is : Returns an array of stack trace elements representing the
 * stack dump of this thread. If the returned array is of non-zero length then
 * the first element of the array represents the top of the stack, which is the
 * most recent method invocation in the sequence. The last element of the array
 * represents the bottom of the stack, which is the least recent method
 * invocation in the sequence.
 * The example of Java invoking stack and stack frame as follows :
 * [0]	StackTraceElement  (id=107335)
 * declaringClass	"java.lang.Thread" (id=107389)
 * fileName	"Thread.java" (id=107390)
 * lineNumber	1552
 * methodName	"getStackTrace" (id=107391)
 * [1]	StackTraceElement  (id=107337)
 * declaringClass	"com.wagerworks.framework.dao.ACommonDAO" (id=107453)
 * fileName	"ACommonDAO.java" (id=107454)
 * lineNumber	372
 * methodName	"getMethod" (id=107455)
 */
public class StackFrameOfThreadStudy {

    public static void main(String... strings) {
        StackTraceElement[] invokeStack = Thread.currentThread().getStackTrace();
        /*for (StackTraceElement ele : invokeStack) {
            System.out.println(ele.getMethodName());
		}*/

        Stream.of(invokeStack).forEachOrdered(System.out::println);
        Stream.of(invokeStack).forEachOrdered(ele -> System.out.println(ele.getMethodName()));
    }
}
