package com.qinf.study.base.jdk.language.exception;

public class FinallyStudy {

    @SuppressWarnings({"unused", "finally"})
    private static String testFinally() {
        String outsideFinally = null;
        String insideFinally = null;

        try {
            outsideFinally = "outsideFinally";
            try {
                insideFinally = "insideFinally";
                int i = 1 / 0;
            } catch (Exception e) {
                insideFinally = "inside finally encounter error";
            } finally {
                System.out.println(insideFinally);
                int j = 2 / 0;
            }
            System.out.println("inside finally end");
        } finally {
            System.out.println(outsideFinally);
            return outsideFinally;
        }
    }

    public static void main(String... strings) {
        String result = testFinally();
        System.out.println("I get the result is : " + result);
    }
}
