package com.qinf.study.base.jdk.language.collection;

/**
 * Created by qinf on 2018/08/24
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ListSortByDateStudy {
    public static void main(String[] args) {
        List<User> users = new ArrayList<>();
        User user1 = new User();
        User user2 = new User();
        User user3 = new User();
        user1.setName("Tom");
        user1.setBirthday("1966-11-01");
        user2.setName("Sam");
        user2.setBirthday("2018-12-01");
        user3.setName("Jak");
        user3.setBirthday("2022-12-01");
        users.addAll(Arrays.asList(user1, user2, user3));

        System.out.println("Before sort list:");
        users.forEach(user -> System.out.println(user.getName() + " : " + user.getBirthday()));

        // Sort list by date.
        users = users.stream().sorted(Comparator.comparing(User::getBirthday).reversed()).collect(Collectors.toList());

        System.out.println("After sort list:");
        users.forEach(user -> System.out.println(user.getName() + " : " + user.getBirthday()));

        System.out.println("Get latest user:");
        System.out.println(users.stream().findFirst().get().getBirthday());
    }

    private static class User {
        private String name;
        private String birthday;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getBirthday() {
            return birthday;
        }

        public void setBirthday(String birthday) {
            this.birthday = birthday;
        }
    }
}