package com.qinf.study.base.jdk.designpattern.singleton.hungrysingleton;

/**
 * Created by qinf on 2017/7/27 PM10:11:58
 */
public class HungrySingletonStudy {

    public static void main(String[] args) {
        HttpClientHungry httpClient = HttpClientHungry.getHttpClientInstance();
        httpClient.executeHttpGetRequest();
    }
}
