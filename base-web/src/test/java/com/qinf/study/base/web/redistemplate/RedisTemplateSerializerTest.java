package com.qinf.study.base.web.redistemplate;

import com.alibaba.fastjson.support.spring.FastJsonRedisSerializer;
import com.qinf.study.base.web.TestTemplate;
import lombok.Data;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.io.Serializable;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * Created by qinf on 2021-06-09.
 */
public class RedisTemplateSerializerTest extends TestTemplate {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    private static final String rateLimiterluaScript =
            "local counter_key = KEYS[1] " +
            "local threshold = tonumber(ARGV[1]) " +
            "local ttl_sec = tonumber(ARGV[2]) " +
            "local curr_invoke_sum = redis.call('INCR', counter_key) " +
            "if curr_invoke_sum == 1 then " +
                "redis.call('EXPIRE', counter_key, ttl_sec) " +
            "end " +
            "if curr_invoke_sum > threshold then " +
                "return true " +
            "end " +
            "return false";

    @Test
    public void setExpireZeroSec() {
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new StringRedisSerializer());
        redisTemplate.opsForValue().set("key::sec", "helloworld negtive second", 1, TimeUnit.SECONDS);
        System.out.println(redisTemplate.opsForValue().get("key"));
    }

    /**
     * If the template do not set key and value serializer, the default serializer will be
     * enabled, i.e., JdkSerializationRedisSerializer. Then from redis client, you will get
     * unreadable key content and value content.
     * E.g., the key "key" represent in redis will be "��"(binary data), same situation
     * also for value. Of cause, if the key serializer is set(StringRedisSerializer), the
     * key content will be readable, but the value content is still unreadable, maybe "��".
     */
    @Test
    public void testExecString() {
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        //redisTemplate.setValueSerializer(new StringRedisSerializer());
        redisTemplate.opsForValue().set("key", "value");
        Object result = redisTemplate.opsForValue().get("key");
        System.out.println(result);
    }

    /**
     * Following execution will throw error:
     * org.springframework.data.redis.RedisSystemException: Error in execution;
     * nested exception is io.lettuce.core.RedisCommandExecutionException:
     * ERR Error running script (call to f_15f2bbbb60f5786acb800c8fe6aff93677117d54):
     * @user_script:1: user_script:1: attempt to compare nil with number
     *
     * The inner default serialization is supplied by jdk.
     */
    @Test
    public void testExecLuaReproduceIssue() {
        //redisTemplate.setKeySerializer(new StringRedisSerializer()); // serialize keys to bytes(byte array)
        RedisScript redisScript = RedisScript.of(rateLimiterluaScript, Boolean.class);
        String counterKey = "RATE::LIMITER::COUNTER::KEY";
        Boolean isExceed = (Boolean) redisTemplate.execute(
                redisScript,
                Collections.singletonList(counterKey),
                String.valueOf(1),
                String.valueOf(60));
        /*Boolean isExceed = (Boolean) redisTemplate.execute(
                redisScript,
                new StringRedisSerializer(),
                new StringRedisSerializer(),
                Collections.singletonList(counterKey),
                String.valueOf(1),
                String.valueOf(60));*/
        System.out.println("Is exceed: " + isExceed);
    }

    @Test
    public void testExecLuaJsonSerializeArgs() {
        redisTemplate.setKeySerializer(new StringRedisSerializer()); // serialize keys
        String luaScriptText = "return tonumber(ARGV[2])";
        RedisScript redisScript = RedisScript.of(luaScriptText, Long.class);
        String counterKey = "COUNTER::KEY";
        Object result = redisTemplate.execute(
                redisScript,
                new FastJsonRedisSerializer<>(String.class), // serialize args
                new StringRedisSerializer(), // serialize result
                Collections.singletonList(counterKey),
                "2021",
                "2022");
        System.out.println("The result is: " + result);
    }

    @Test
    public void testStringRedisTemplate() {
        String luaScriptText = "return tonumber(ARGV[2])";
        RedisScript redisScript = RedisScript.of(luaScriptText, Long.class);
        String counterKey = "COUNTER::KEY";
        Object result = stringRedisTemplate.execute(
                redisScript,
                Collections.singletonList(counterKey),
                "2021",
                "2022");
        System.out.println("The result is: " + result);
    }

    @Test
    public void testExecLuaDirectGetBytesOfArgs() {
        /**
         * If the key is serialized by StringRedisSerializer, can considered that the key will be
         * a standard string type passed into redis.
         */
        redisTemplate.setKeySerializer(new StringRedisSerializer()); // serialize keys to bytes(byte array)
        String luaScriptText = "return tonumber(ARGV[2])";
        /**
         * Because the result returned from lua script is number, so the class type must be Long.class.
         */
        RedisScript redisScript = RedisScript.of(luaScriptText, Long.class);
        String counterKey = "COUNTER::KEY";
        Object result = redisTemplate.execute(
                redisScript,
                /**
                 * Serialize args to bytes(byte array).
                 * If the StringRedisSerializer is used, args just encoded by
                 * String.getBytes(UTF-8), the result is 2022.
                 * If JdkSerializationRedisSerializer is used, the result will be null.
                 * That means lua cannot deserialize the bytes serialized by jdk.
                 */
                new StringRedisSerializer(),  // serialize args
                new StringRedisSerializer(), // serialize result
                Collections.singletonList(counterKey),
                "2021",
                "2022");
        System.out.println("The result is: " + result);
    }

    @Test
    public void testExecLuaScript() {
        boolean result = this.isExceed(1);
        System.out.println(result);
    }

    private boolean isExceed(int threshold) {
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        RedisScript redisScript = RedisScript.of(rateLimiterluaScript, Boolean.class);
        String counterKey = "COUNTER::KEY";
        Boolean isExceed = (Boolean) redisTemplate.execute(
                redisScript,
                /**
                 * Serialize args from string type to bytes(byte array), here only
                 * StringRedisSerializer is suitable, other serializers not work.
                 * In other words, if StringRedisSerializer not set for serializing
                 * args from string to bytes, then default serializer(JdkSerializationRedisSerializer)
                 * of spring-data will convert args to bytes, then the value(content)
                 * of converted ARGV[1] in redis may be messy code, then the lua inner
                 * function tonumber(ARGV[1]) will not work.
                 */
                new StringRedisSerializer(),
                new StringRedisSerializer(),
                Collections.singletonList(counterKey),
                String.valueOf(threshold));
        return isExceed;
    }

    @Data
    static class Person implements Serializable {
        //static class Person {
        private String name;
        private Integer age;
    }

    @Test
    public void testSerializeBeanInRedis() {
        redisTemplate.setKeySerializer(new StringRedisSerializer()); // serialize keys to bytes(byte array)
        String luaScriptText = "return tonumber(ARGV[2])";
        /**
         * Because the result returned from lua script is number, so the class type must be Long.class.
         */
        RedisScript redisScript = RedisScript.of(luaScriptText, Long.class);
        String counterKey = "COUNTER::KEY";
        Person person = new Person();
        person.setName("QF");
        person.setAge(2021);
        Object result = redisTemplate.execute(
                redisScript,
                //new Jackson2JsonRedisSerializer<Object>(Object.class), // Member not need implement Serializable interface. Encoded by UTF-8
                new StringRedisSerializer(), // serialize args to bytes(byte array) // Just String.getBytes(UTF-8)
                //new JdkSerializationRedisSerializer(), // Member need implement Serializable interface. ObjectStream, encoding is UTF-8
                new StringRedisSerializer(), // serialize result to bytes
                Collections.singletonList(counterKey),
                person,
                "2021",
                "2022");
        System.out.println("The result is: " + result);
    }
}
