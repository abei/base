package com.qinf.study.base.web.proxy;

import com.qinf.study.base.web.TestTemplate;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by qinf on 2021-07-23.
 */
public class ProxyFinalClassTest extends TestTemplate {

    @Autowired
    private MovieRecommendProcessor movieRecommendProcessor;

    /**
     * I found in some springboot project the final class annotated by @Component
     * cannot be initialized by spring(the further reason known), but after test,
     * it can be initialized.
     */
    @Test
    public void testInitializeFinalClassBySprintProxy() {
        movieRecommendProcessor.process();
    }
}
