package com.qinf.study.base.web.mapper;

import com.qinf.study.base.web.mapper.entity.CustomerEntity;
//import org.apache.ibatis.annotations.Mapper;

/**
 * Created by qinf on 2019-05-30.
 */
//@Mapper
public interface CustomerMapper {

    int insertCustomer(CustomerEntity customerEntity);

    CustomerEntity selectCustomerByCustomerId(Long customerId);
}
