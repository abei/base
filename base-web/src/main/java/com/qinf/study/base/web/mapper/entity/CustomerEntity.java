package com.qinf.study.base.web.mapper.entity;

import lombok.Data;

import java.util.Date;

/**
 * Created by qinf on 2019-05-30.
 */
@Data
public class CustomerEntity {
    private Double id;
    private Long customerId;
    private String customerName;
    private String customerPhone;
    private String customerAddress;
    private Date createTime;
    private Date updateTime;
}
