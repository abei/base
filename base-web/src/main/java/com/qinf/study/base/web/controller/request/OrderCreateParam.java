package com.qinf.study.base.web.controller.request;

import lombok.Data;

/**
 * Created by qinf on 2019-07-01.
 */
@Data
public class OrderCreateParam implements Param {
}
