package com.qinf.study.base.web.mapper.entity;

import lombok.Data;

import java.util.Date;

/**
 * Created by qinf on 2019-05-30.
 */
@Data
public class GoodsEntity {
    private Double id;
    private Long goodsId;
    private String goodsName;
    private Long priceId;
    private Date createTime;
    private Date updateTime;
}
