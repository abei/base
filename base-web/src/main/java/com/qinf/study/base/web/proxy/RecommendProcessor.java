package com.qinf.study.base.web.proxy;

/**
 * Created by qinf on 2021-07-23.
 */
public abstract class RecommendProcessor implements Processor {

    public abstract void process();
}
