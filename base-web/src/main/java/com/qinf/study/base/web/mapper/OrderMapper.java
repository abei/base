package com.qinf.study.base.web.mapper;

import com.qinf.study.base.web.mapper.entity.OrderEntity;
//import org.apache.ibatis.annotations.Mapper;

/**
 * Created by qinf on 2019-05-28.
 */
//@Mapper
public interface OrderMapper {

    int insertOrder(OrderEntity orderEntity);

    OrderEntity selectOrderByOrderId(Long orderId);
}
