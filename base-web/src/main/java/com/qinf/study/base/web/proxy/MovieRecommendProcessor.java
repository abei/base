package com.qinf.study.base.web.proxy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Created by qinf on 2021-07-23.
 */
@Slf4j
@Component
public final class MovieRecommendProcessor extends RecommendProcessor {

    private MovieRecommendProcessor(){}

    @Override
    public void process() {
        log.info("Initialized by spring proxy!");
    }

    @Override
    public void doProcess() {

    }
}
