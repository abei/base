package com.qinf.study.base.web.mapper;

import com.qinf.study.base.web.mapper.entity.GoodsEntity;
//import org.apache.ibatis.annotations.Mapper;

/**
 * Created by qinf on 2019-05-30.
 */
//@Mapper
public interface GoodsMapper {

    int insertGoods(GoodsEntity goodsEntity);

    GoodsEntity selectGoodsByGoodsId(Long goodsId);
}
