package com.qinf.study.base.web.mapper.entity;

import lombok.Data;

import java.util.Date;

/**
 * Created by qinf on 2019-05-28.
 */
@Data
public class OrderEntity {
    private Double id;
    private Long orderId;
    private Integer orderSource;
    private Integer orderStatus;
    private Long goodsId;
    private String goodsName;
    private Integer payStatus;
    private String customerId;
    private Date createTime;
    private Date updateTime;
}
