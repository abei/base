package com.qinf.study.base.web.proxy;

/**
 * Created by qinf on 2021-07-23.
 */
public interface Processor {

    void doProcess();
}
