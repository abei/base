package com.qinf.study.base.web.controller;

import com.qinf.study.base.web.controller.request.OrderCreateParam;
import com.qinf.study.rest.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * Created by qinf on 2019-07-01.
 */
@Slf4j
@RestController
@RequestMapping("/v1/order")
public class OrderController {

    /**
     * Create order interface.
     *
     * @param createParam the parameters for creating order
     * @return Response
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Response createOrder(@RequestBody OrderCreateParam createParam) {
        return Response.succ();
    }

    /**
     * Query specified order by order id.
     *
     * @param orderId  order id
     * @param pageNum  the number of page
     * @param pageSize the size of page
     */
    @RequestMapping(value = "/query/{order_id}", method = RequestMethod.GET)
    public Response queryOrder(@PathVariable(name = "order_id", required = true) String orderId,
                               @RequestParam(name = "page_num", defaultValue = "1") Integer pageNum,
                               @RequestParam(name = "page_size", defaultValue = "20") Integer pageSize) {
        return Response.succ();
    }
}
