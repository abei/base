package com.qinf.study.base.kit.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Arrays;
import java.util.Properties;

/**
 * Created by qinf on Jun 28, 2018.
 */
public class KafkaConsumerClient2 {

    public static void main(String... strings) {
        KafkaConsumer<String, String> consumer = null;
        try {
            Properties props = new Properties();
            props.put("bootstrap.servers", "localhost:9090");
            props.put("group.id", "group-0");
            props.put("enable.auto.commit", "true");
            props.put("auto.commit.interval.ms", "1000");
            props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
            props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
            consumer = new KafkaConsumer<>(props);
            consumer.subscribe(Arrays.asList("topic-partition"));
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(100);
                records.forEach(record -> System.out.printf("consumer-1 : {partition = %d, offset = %s, message = %s, key = %s}%n",
                        record.partition(), record.offset(), record.value(), record.key()));
            }
        } finally {
            consumer.close();
        }
    }
}
