package com.qinf.study.base.kit.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * Created by qinf on 2018/6/20.
 * <p>
 * step-1. Instantiate User.class (reflect - newInstance)->
 * step-2. Populate properties (user.setxxx) ->
 * step-3. Check Aware (set dependencies) ->
 * step-4. -> postProcessBeforeInitialization() ->
 * step-5. Callback InitializingBean's afterPropertiesSet ->
 * step-6. Callback a custom init-method.
 */
@Component
public class MonitorBeanInitialization implements BeanPostProcessor {
    private static final Logger logger = LoggerFactory.getLogger(MonitorBeanInitialization.class);

    public MonitorBeanInitialization() {
        super();
        logger.info("Monitor : monitor of bean initialization created.");
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        logger.info("Monitor : the bean name is : " + beanName + ".");
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }
}
