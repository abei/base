package com.qinf.study.base.kit.mybatis;

import com.qinf.study.base.kit.mybatis.domain.Player;
import com.qinf.study.base.kit.mybatis.mapper.PlayerMapper;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by qinf on 2017-10-31
 */
public class MyBatisBySpringStudy {

    public static void main(String... strings) {
        try (ClassPathXmlApplicationContext appCxt = new ClassPathXmlApplicationContext(
                "config-spring/spring-cxt.xml")) {
            PlayerMapper playerDAO = appCxt.getBean(PlayerMapper.class);
            Player player = playerDAO.getPlayerById(25);
            System.out.println(player.getSkinCode() + " | " + player.getPlayerName());
        }
    }
}
