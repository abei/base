package com.qinf.study.base.kit.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

/**
 * Created by qinf on 2018-06-25.
 */
public class PropsUtil {
    private static final Logger logger = LoggerFactory.getLogger(PropsUtil.class);

    public static Properties loadProps(String propsPath) {
        Properties props = new Properties();
        InputStream inStream = null;
        try {
            inStream = PropsUtil.class.getClassLoader().getResourceAsStream(propsPath);
            props.load(inStream);
        } catch (FileNotFoundException e) {
            logger.error("Can not find the properties file, " + e.getMessage());
        } catch (IOException e) {
            logger.error("Exception occurred when get properties, " + e.getMessage());
        } finally {
            try {
                if (Objects.nonNull(inStream))
                    inStream.close();
            } catch (IOException e) {
                logger.error("Can not close the input stream, " + e.getMessage());
            }
        }
        return props;
    }
}
