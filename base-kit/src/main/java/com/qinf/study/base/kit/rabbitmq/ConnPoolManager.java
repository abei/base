package com.qinf.study.base.kit.rabbitmq;

import com.qinf.study.base.kit.utils.PropsUtil;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

/**
 * Created by qinf on 2018-12-12.
 */
public class ConnPoolManager {
    private static final ConnectionFactory connFactory;

    static {
        Properties props = PropsUtil.loadProps("config-rabbitmq/rabbitmq.properties");
        connFactory = new ConnectionFactory();
        connFactory.setHost(props.getProperty("rabbitmq.host"));
        connFactory.setPort(Integer.parseInt(props.getProperty("rabbitmq.port")));
        connFactory.setUsername(props.getProperty("rabbitmq.username"));
        connFactory.setPassword(props.getProperty("rabbitmq.password"));
        connFactory.setVirtualHost(props.getProperty("rabbitmq.virtualhost"));
    }

    private ConnPoolManager() {
    }

    public static Connection borrowConn() throws IOException, TimeoutException {
        return connFactory.newConnection();
    }

    public static void returnConn(Connection conn) throws IOException {
        if (Objects.nonNull(conn))
            conn.close();
    }
}
