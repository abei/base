package com.qinf.study.base.kit.rabbitmq;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by qinf on 2018-12-12.
 * <p>
 * 1.The based procedure of initiating and using producer as follows:<br>
 *   (1).Obtain connection(the concept of TCP connection) from connection pool.<br>
 *   (2).Create channel used for receiving message based on connection.<br>
 *   (3).Declare(or you can consider it as create) a queue.<br>
 *   (4).Bind generated queue on existent Exchange.<br>
 *   (5).Bind consumer on generated channel.<br>
 *   (6).Perform receiving.<br>
 * <p>
 * 2.The concept of binding key:<br>
 *   The binding key means what message(with a particular routing key) you interested in.
 *   You can customize your own binding key to a queue, then your interested message will
 *   be delivered to the queue.
 * <p>
 * NOTE :<br>
 * * - substitutes for exactly one word, for example: "this.is.my.*" matches "this.is.my.key"
 * but doesn't match "this.is.my.routing.key"<br>
 * # - substitutes for 0 or more words, for example: "this.is.#" matches "this.is.my.key"
 * as whell than "this.is.key"
 */
public class RabbitMQConsumerClient {

    static class Consumer {
        private String exchangeName;
        private String consumerName;

        Consumer(String exchangeName, String consumerName) {
            this.exchangeName = exchangeName;
            this.consumerName = consumerName;
        }

        private void doReceive(String queueName, String bindingKey) {
            try (Connection conn = ConnPoolManager.borrowConn();
                 Channel channel = conn.createChannel()) {
                channel.queueDeclare(queueName, true, false, false, null);
                channel.queueBind(queueName, exchangeName, bindingKey);
                channel.basicConsume(queueName, true, new DefaultConsumer(channel) {
                    @Override
                    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body){
                        String message = new String(body);
                        System.out.println(consumerTag + " received : " + message);
                    }
                });
                TimeUnit.SECONDS.sleep(40); // Make thread retain alive to accept callback to consume message.
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * ConsumerName : customized string
     * ExchangeName : ha.t_ + topicname(c1_allocate_service_topic)
     * QueueName : ha.t_ + consumername(customized string) + cid
     * BindingKey : ha.t_ + topicname(c1_allocate_service_topic) + . + tag(c1-allocate-service-tag) + . + *
     */
    public static void main(String... args) throws InterruptedException {
        //RabbitMQConsumerClient.Consumer consumer = new RabbitMQConsumerClient.Consumer("TOPIC_EXCHANGE", "ConsumerName");
        //consumer.doReceive("ALL_CURRENCY_QUEUE", "TOPIC_EXCHANGE.ROUTINGKEY.CURRENCY.*");

        ExecutorService threadPool = Executors.newCachedThreadPool();
        RabbitMQConsumerClient.Consumer consumer = new RabbitMQConsumerClient.Consumer("ha.t_c1_allocate_service_topic",
                "ConsumerName");

        /**
         * Two different threads(consumers) consume message from a same queue.
         * Just one consumer can get the message from the queue. Because a message
         * will disappear once it is consumed.
         */
        /*for (int i = 0; i < 2; i++) {
            threadPool.execute(() -> {
                consumer.doReceive("ha.t_clue-api_alloc_clue_c.01", "ha.t_c1_allocate_service_topic.*");
            });
        }
        threadPool.shutdown();
        threadPool.awaitTermination(35, TimeUnit.SECONDS);*/

        /**
         * Two different consumers consume message from two different queues.
         * Each consumer can receive message it interested from the queues
         * bound on a Topic Exchange.
         */
        threadPool.execute(() -> {
            consumer.doReceive("ha.t_clue-api_alloc_clue_c.02", "ha.t_c1_allocate_service_topic.*");
        });
        threadPool.execute(() -> {
            consumer.doReceive("ha.t_clue-api_alloc_clue_c.03", "ha.t_c1_allocate_service_topic.*");
        });
        threadPool.shutdown();
        threadPool.awaitTermination(1, TimeUnit.SECONDS);
    }

}
