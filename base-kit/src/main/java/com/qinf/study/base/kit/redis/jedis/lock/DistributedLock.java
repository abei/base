package com.qinf.study.base.kit.redis.jedis.lock;

import com.qinf.study.base.kit.redis.jedis.JedisPoolManager;
import com.qinf.study.base.kit.redis.jedis.RedisClient;
import com.qinf.study.base.kit.redis.jedis.RedisOperator;
import redis.clients.jedis.Jedis;

import java.util.Collections;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by qinf on 2018-11-17.
 *
 * The distributed lock not solve a problem that obtained lock will
 * expire before a thread complete its long time task.
 */
public class DistributedLock {
    private static final ThreadLocal<String> TRANSIENT_LOCK_VALUE_CACHE = new ThreadLocal<>();
    private static final Integer DEFAULT_TTL = 60000; // Time of lock to live, unit is milliseconds.
    private static final String KEY_PREX = "REDIS_LOCK::";
    private static final String UNLOCK_LUA_SCRIPT = "if redis.call('get',KEYS[1])==ARGV[1] then return redis.call('del',KEYS[1]) else return 0 end";

    public static boolean lock(String key) {
        return lock(key, DEFAULT_TTL.longValue());
    }

    public static boolean lock(String key, Long ttl) {
        Jedis jedis = JedisPoolManager.borrowJedis();
        try {
            key = KEY_PREX.concat(key);
            String value = getLockValue();
            /**
             * The expire time : SET key, value, [NX | XX], [EX(seconds) | PX(milliseconds)]
             * NX : Only set the key if it does not already exist.
             * XX : Only set the key if it already exist.
             * EX : The expire time of key, unit is second.
             * PX : The expire time of key, unit is millisecond.
             *
             * 1. SET {key} {value} EX {expiry} NX is atomic command. Because of the core of
             *    redis is single threaded, so nothing will run until the SET has completed.
             *    So, can regard SET command as lua script to obtain lock.
             * 2. Jedis.set(key, value, NX, EX, expire) is non-blocking method.
             */
            if (Objects.equals("OK", jedis.set(key, value, "NX", "PX", ttl))) {
                TRANSIENT_LOCK_VALUE_CACHE.set(value);
                return true;
            }
            return false;
        } finally {
            if (Objects.nonNull(jedis))
                jedis.close();
        }
    }

    public static boolean spinLock(String key) {
        Jedis jedis = JedisPoolManager.borrowJedis();
        try {
            key = KEY_PREX.concat(key);
            Long firstTryTime = new Date().getTime(); // The unit is milliseconds.
            do {
                if (lock(key))
                    return true;
                try {
                    TimeUnit.MILLISECONDS.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while ((new Date().getTime() - DEFAULT_TTL) < firstTryTime);

            return false;
        } finally {
            if (Objects.nonNull(jedis))
                jedis.close();
        }
    }

    public static boolean unLock(String key) {
        Objects.requireNonNull(key, "The lock key cannot be null when try to unlock it.");
        if (Objects.isNull(TRANSIENT_LOCK_VALUE_CACHE.get()))
            //throw new IllegalThreadStateException("It seems the thread locked nothing.");
            return true;

        RedisOperator<Boolean> oper = jedis -> {
            String jKey = KEY_PREX.concat(key);
            String jVlu = TRANSIENT_LOCK_VALUE_CACHE.get();
            try {
                if (Objects.equals(1L, jedis.eval(UNLOCK_LUA_SCRIPT, Collections.singletonList(jKey), Collections.singletonList(jVlu)))) {
                    TRANSIENT_LOCK_VALUE_CACHE.remove();
                    return true;
                }
            } finally {
                if (Objects.isNull(jedis.get(jKey)))
                    TRANSIENT_LOCK_VALUE_CACHE.remove();
            }
            return false;
        };

        return RedisClient.exec(oper);
    }

    private static String getLockValue() {
        return UUID.randomUUID().toString() + "_" + new Date().getTime();
    }

    public static void main(String... args) {
        String key = "QF";

        for (int i = 0; i < 1; i++) {
            new Thread(() -> {
                try {
                    // Lock key.
                    if (DistributedLock.lock(key)) {
                        System.out.println(Thread.currentThread().getName() + " lock key:" + key + " successfully.");
                        try {
                            TimeUnit.SECONDS.sleep(1);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        System.out.println(Thread.currentThread().getName() + " lock key:" + key + " failed.");
                    }
                } finally {
                    // Unlock key.
                    if (DistributedLock.unLock(key))
                        System.out.println(Thread.currentThread().getName() + " unlock key:" + key + " successfully.");
                    else
                        System.out.println(Thread.currentThread().getName() + " unlock key:" + key + "failed.");
                }
            }).start();
        }
    }
}
