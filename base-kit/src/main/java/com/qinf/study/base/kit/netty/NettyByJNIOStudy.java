package com.qinf.study.base.kit.netty;

/**
 * Created by qinf on 2018/1/8 PM10:54:32
 * <p>
 * The Netty app completed by Java NIO mode(encapsulate Java NIO).
 * Can run many OS, e.g., windows, Linux etc.
 */
public class NettyByJNIOStudy {

    // Check EchoServer.java, pay attention to code: bootstrap.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
}
