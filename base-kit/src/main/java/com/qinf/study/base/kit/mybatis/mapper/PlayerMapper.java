package com.qinf.study.base.kit.mybatis.mapper;

import com.qinf.study.base.kit.mybatis.domain.Player;

/**
 * Created by qinf on 2017-10-31
 */
public interface PlayerMapper {

    Player getPlayerById(int playerId);
}
