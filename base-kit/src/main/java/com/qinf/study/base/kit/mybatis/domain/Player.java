package com.qinf.study.base.kit.mybatis.domain;

/**
 * Created by qinf on 2017-10-31
 */
public class Player {
    private String skinCode;
    private String playerName;

    public String getSkinCode() {
        return skinCode;
    }

    public void setSkinCode(String skinCode) {
        this.skinCode = skinCode;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

}
