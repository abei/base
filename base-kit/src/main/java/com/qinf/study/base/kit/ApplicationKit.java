package com.qinf.study.base.kit;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by qinf on 2018-09-24.<p>
 *
 * This module is SpringBoot project, but not web, just a java application.
 * So, the spring-boot-starter-web is not mandatory, just the basic and core
 * spring-boot-starter is sufficient.<p>
 * In other words, if you want a spring boot web project, the spring-boot-starter-web
 * is necessary.
 */
@SpringBootApplication
public class ApplicationKit implements CommandLineRunner {
    public static void main(String... args){
        SpringApplication.run(ApplicationKit.class, args);
    }

    /**
     * This method make sure the main thread don't exit.
     */
    @Override
    public void run(String... strings) throws Exception {
        Thread.currentThread().join();
    }
}
