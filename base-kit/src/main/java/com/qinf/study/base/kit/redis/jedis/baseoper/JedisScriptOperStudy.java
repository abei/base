package com.qinf.study.base.kit.redis.jedis.baseoper;

import com.qinf.study.base.kit.redis.jedis.JedisPoolManager;
import com.qinf.study.base.kit.redis.jedis.RedisClient;
import redis.clients.jedis.Jedis;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * Created by qinf on 2021-06-09.
 */
public class JedisScriptOperStudy {

    private static void performLuaScript(Jedis jedis) throws InterruptedException {
        // Clear Reids.
        System.out.println(jedis.flushDB());

        //String luaScript = "return {tonumber('121')}";
        /**
         * Lua boolean true convert to redis 1, false convert redis nil.
         * The lua script and related keys and args will be encoded by String.getBytes("UTF-8") in redis.clients.util.SafeEncoder.class
         */
        String luaScript = "local counter_key = KEYS[1] local curr_invoke_sum = redis.call('INCR', counter_key) if curr_invoke_sum == 1 then redis.call('EXPIRE', counter_key, 10) end if curr_invoke_sum > tonumber(ARGV[1]) then return true end return false";
        Object result1 = jedis.eval(luaScript, Arrays.asList("rate::limiter::987654323"), Arrays.asList("2"));
        System.out.println("result1: " + result1);

        TimeUnit.SECONDS.sleep(1);
        Object result2 = jedis.eval(luaScript, Arrays.asList("rate::limiter::987654323"), Arrays.asList("2"));
        System.out.println("result2: " + result2);

        TimeUnit.SECONDS.sleep(1);
        Object result3 = jedis.eval(luaScript, Arrays.asList("rate::limiter::987654323"), Arrays.asList("2"));
        System.out.println("result3: " + result3);
    }

    public static void main(String... args) {
        Jedis jedis = null;
        try {
            jedis = JedisPoolManager.borrowJedis();
            performLuaScript(jedis);
        } catch (Exception e) {
            e.printStackTrace();
            RedisClient.destroy();
        } finally {
            JedisPoolManager.returnJedis(jedis);
            //RedisClient.destroy();
        }
    }
}
