package com.qinf.study.base.kit.redis.jedis;

import redis.clients.jedis.Jedis;

import java.util.Objects;

/**
 * Created by qinf on 2019-01-29.
 */
public class RedisClient {

    public static <T> T exec(RedisOperator<T> oper) {
        Objects.requireNonNull(oper, "Operator can not be null.");
        Jedis jedis = null;
        T result;
        try {
            jedis = JedisPoolManager.borrowJedis();
            result = oper.operate(jedis);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            JedisPoolManager.returnJedis(jedis);
        }
        return result;
    }

    public static void destroy() {
        JedisPoolManager.destroy();
    }

}
