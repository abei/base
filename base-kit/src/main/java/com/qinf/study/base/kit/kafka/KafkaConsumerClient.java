package com.qinf.study.base.kit.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Arrays;
import java.util.Properties;

/**
 * Created by qinf on Jun 28, 2018.
 * <p>
 * Here, I will record some understanding of using Kafka consumer.
 * 1. auto.offset.reset
 * Producer client produces 2 messages : HelloWorld HelloWorld,
 * Consumer client with GroupId "group-0" consume above two messages.
 * Consumer client with GroupId "group-1" cannot consume above two messages any more, even you specified a new GroupId.
 * But if you use a new GroupId, meanwhile, configure the "auto.offset.reset" as earliest, you can consume the two messages again.
 * <p>
 * 2. if there are multiple instances of Kafka consumer with a same GroupId
 * A consumer instance with GroupId "group-0" on a server(tomcat), meanwhile, another consumer instance with
 * GroupId "group-0" on another server. Only one of the two instances can consume a message from a Topic at the same time.
 * They cannot consume a message repeatedly. For short words : All consumer instances sharing the same group.id.
 */
public class KafkaConsumerClient {

    public static void main(String... strings) {
        KafkaConsumer<String, String> consumer = null;
        try {
            Properties props = new Properties();
            props.put("bootstrap.servers", "localhost:9090");
            props.put("group.id", "group-2");
            props.put("enable.auto.commit", "true");
            props.put("auto.commit.interval.ms", "1000");
            props.put("auto.offset.reset", "earliest"); // latest
            props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
            props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
            consumer = new KafkaConsumer<>(props);
            consumer.subscribe(Arrays.asList("topic-partition"));
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(100);
                records.forEach(record -> System.out.printf("consumer-0 : {partition = %d, offset = %s, message = %s, key = %s}%n",
                        record.partition(), record.offset(), record.value(), record.key()));
            }
        } finally {
            consumer.close();
        }
    }
}
