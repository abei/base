package com.qinf.study.base.kit.redis.jedis.serializer;

import com.qinf.study.base.kit.redis.jedis.JedisPoolManager;
import com.qinf.study.base.kit.redis.jedis.RedisClient;
import com.qinf.study.base.kit.redis.jedis.baseoper.Member;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.Objects;

/**
 * Created by qinf on 2021-06-11.
 *
 * If you use Jedis.set(byte[] key, byte[] value) and Jedis.get(byte[]), you have to
 * serialize and deserialize the data by your own.
 * By contrary, execute Jedis.set(String key, String value) and Jedis.get(byte[]), Jedis
 * will do the serialization and deserialization automatically.
 *
 * From Jedis perspective, Jedis.set("key".getBytes() -> key1) and Jedis.set(String "key" -> key2),
 * in redis the key1 and key2 means a same key, additionally, in redis the data just stored by bytes.
 */
public class JedisBeanSerializeStudy {

    public static void main(String... args) throws IOException, ClassNotFoundException {
        Jedis jedis = JedisPoolManager.borrowJedis();

        try {
            Member member = new Member();
            member.setName("QF");
            member.setAge(20);

            byte[] memberInBytes = Member.serialize(member);
            jedis.set("key".getBytes(), memberInBytes);
            /**
             * The content of string type of memberInSerialized as following:
             * �� sr 3com.qinf.study.base.kit.redis.jedis.baseoper.Member��Z���M� L aget Ljava/lang/Integer;L namet Ljava/lang/String;xpsr java.lang.Integer⠤���8 I valuexr java.lang.Number������  xp   t QF
             * The char array type can get from "serialized object in bytes(unicode).png"
             * The visible type can get from "serialized object in visible.png"
             */
            String memberInSerialized = jedis.get("key");
            System.out.println(memberInSerialized);

            jedis.set("key", "One more time!");
            byte[] stringInBytes = jedis.get("key".getBytes());
            /**
             * The content of string type of stringInDeserialized is: One more time
             */
            String stringInDeserialized = new String(stringInBytes);
            System.out.println(stringInDeserialized);
        } finally {
            if (Objects.nonNull(jedis)) {
                RedisClient.destroy();
            }
        }
    }

}
