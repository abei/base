package com.qinf.study.base.kit.redis.jedis;

import redis.clients.jedis.Jedis;

/**
 * Created by qinf on 2019-01-29.
 */
public interface RedisOperator<T> {
    T operate(Jedis jedis) throws Exception;
}
