package com.qinf.study.base.kit.redis.jedis.baseoper;

import com.qinf.study.base.kit.redis.jedis.JedisPoolManager;
import com.qinf.study.base.kit.redis.jedis.RedisClient;
import redis.clients.jedis.Jedis;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by qinf on 2018-09-10.
 */
public class JedisBasicOperStudy {
    /**
     * 1. Operate key of redis.
     */
    private static void operateKey(Jedis jedis) throws InterruptedException {
        // Clear Reids.
        System.out.println(jedis.flushDB());

        // Judge a key whether exists.
        //System.out.println("If key is expired : " + jedis.exists("k"));

        // Set key-value, additionally, set can work as override value by key.
        jedis.set("k", "v");
        System.out.println(jedis.get("k"));

        // Set expiration time of a key.
        jedis.expire("key", 5);
        TimeUnit.SECONDS.sleep(10);
        System.out.println("If can get a key : " + jedis.get("k"));

        // Delete a key.
        jedis.del("k");
        System.out.println("If key is existent : " + jedis.exists("k"));
    }

    /**
     * 2. Operate string of redis.
     */
    private static void operateString(Jedis jedis) {
        // Set key-values in batch.
        jedis.mset("key-string-1", "value-string-1", "key-string-2", "value-string-2");
        List<String> vlaues = jedis.mget("key-string-1", "key-string-2");
        System.out.println(vlaues);

        // Delete key-values in batch.
        System.out.println(jedis.del("key-string-1", "key-string-2"));
        System.out.println(jedis.exists("key-string-1"));
        System.out.println(jedis.exists("key-string-2"));
    }

    /**
     * 3. Operate list of redis.
     */
    private static void operateList(Jedis jedis) {
        // Add elements into list.
        jedis.rpush("key-list-1", "value-list-1");
        jedis.rpush("key-list-1", "value-list-2");

        // Get length of a list in redis.
        System.out.println("The length of list is : " + jedis.llen("key-list-1"));

        // Traverse a list in redis from index 0 to the index 1 that counting down.
        List<String> list = jedis.lrange("key-list-1", 0, -1);
        list.forEach(System.out::println);

        // Get a element of a list by index.
        System.out.println(jedis.lindex("key-list-1", 1));
    }

    /**
     * 4. Operate set of redis.
     */
    private static void operateSet(Jedis jedis) {
        // Add elements into set.
        jedis.sadd("key-set-1", "value-set-1", "value-set-2");
        jedis.sadd("key-set-2", "value-set-3", "value-set-4", "value-set-5");

        // Get count of elements in set.
        System.out.println(jedis.scard("key-set-1"));

        // Get all elements of a key from a set.
        Set<String> set = jedis.smembers("key-set-2");
        set.forEach(System.out::println);

        // If a element belong to a set.
        System.out.println(jedis.sismember("key-set-1", "value-set-2"));

        // Delete one or more element from a set.
        jedis.srem("key-set-2", "value-set-4");
    }

    /**
     * 5. Operate sorted set of redis.
     *  Each member of saved in sorted set can associate with a score,
     *  redis sort all the members asc by its score.
     */
    private static void operateSortedSet(Jedis jedis) {
        // Add data to sorted set.
        Map<String, Double> map = new HashMap();
        map.put("Hello", 100.0);
        map.put("World", 200.0);
        jedis.zadd("key-sset", 300.0, "Redis");
        jedis.zadd("key-sset", map);

        // Get count of element from a sorted set.
        System.out.println("Count of elements is : " + jedis.zcard("key-sset"));

        // Get all elements during the range of index from a sorted set.
        Set<String> sets = jedis.zrange("key-sset", 0, -1);
        sets.forEach(System.out::println);

        // Delete elements
        //System.out.println(jedis.zrem("key-sset", "Redis"));
        //System.out.println(jedis.zremrangeByRank("key-sset", 0, -1));
        //System.out.println(jedis.zrange("key-sset", 0, 0));
    }

    /**
     * 6. Operate hash of redis.
     */
    private static void operateHash(Jedis jedis) {
        // Add data to hash.
        Map<String, String> hash = new HashMap<>();
        hash.put("H", "Hello");
        hash.put("E", "Earth");
        jedis.hmset("key-hash", hash);
        jedis.hset("key-hash", "J", "Java");

        // Get all keys or values of a hash from redis.
        System.out.println(jedis.hkeys("key-hash"));
        System.out.println(jedis.hvals("key-hash"));

        // Get count of elements of a hash.
        System.out.println(jedis.hlen("key-hash"));

        // Get all elements in map of hash.
        Map<String, String> elements = jedis.hgetAll("key-hash");
        System.out.println(elements);

        // If a key exist in hash.
        System.out.println(jedis.hexists("key-hash", "J"));

        // Get a value from hash by key.
        System.out.println(jedis.hget("key-hash", "H"));

        // Get values from hash by keys.
        System.out.println(jedis.hmget("key-hash", "H", "J"));

        //Delete value by key.
        System.out.println(jedis.hdel("key-hash", "J"));
        System.out.println(jedis.hgetAll("key-hash"));
    }

    /**
     * 6. Operate binary of redis.
     */
    private static void operateBinary(Jedis jedis) {
        byte[] key1 = "NANNAN".getBytes();
        byte[] value1 = "study java".getBytes();
        jedis.select(5);
        jedis.set(key1, value1);

        byte[] key2 = "NANNAN".getBytes();
        byte[] value2 = "study python".getBytes();
        jedis.select(10);
        jedis.set(key2, value2);

        jedis.select(5);
        String result = new String(jedis.get(key1));
        System.out.println(result);
    }

    public static void main(String... args) throws InterruptedException {
        Jedis jedis = null;

        try {
            jedis = JedisPoolManager.borrowJedis();

            operateKey(jedis);
            //operateString(jedis);
            //operateList(jedis);
            //operateSet(jedis);
            //operateSortedSet(jedis);
            //operateHash(jedis);
            //operateBinary(jedis);
        } catch (Exception e) {
            e.printStackTrace();
            RedisClient.destroy();
        } finally {
            JedisPoolManager.returnJedis(jedis);
            RedisClient.destroy();
        }
    }

}
