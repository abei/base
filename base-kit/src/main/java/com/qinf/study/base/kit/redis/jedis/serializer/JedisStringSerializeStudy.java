package com.qinf.study.base.kit.redis.jedis.serializer;

import com.qinf.study.base.kit.redis.jedis.JedisPoolManager;
import com.qinf.study.base.kit.redis.jedis.RedisClient;
import redis.clients.jedis.Jedis;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Objects;

/**
 * Created by qinf on 2021-08-17.
 */
public class JedisStringSerializeStudy {

    public static void main(String... args) throws IOException, ClassNotFoundException {
        Jedis jedis = JedisPoolManager.borrowJedis();

        try {
            byte[] key1 = "k1".getBytes();
            byte[] value1 = "2022".getBytes();
            jedis.select(5);
            jedis.set(key1, value1);

            byte[] key2 = "k2".getBytes();
            byte[] value2 = serializeString("2022");
            jedis.select(5);
            jedis.set(key2, value2);
        } finally {
            if (Objects.nonNull(jedis)) {
                RedisClient.destroy();
            }
        }
    }

    private static byte[] serializeString(String string) {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(baos);) {
            oos.writeObject(string);
            oos.flush();
            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
