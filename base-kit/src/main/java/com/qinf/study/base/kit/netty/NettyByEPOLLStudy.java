package com.qinf.study.base.kit.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.socket.SocketChannel;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;

/**
 * Created by qinf on 2018/1/8 PM10:43:05
 * <p>
 * If your are sure that your netty app will run on Linux platform, you can
 * accomplish netty app by epoll mode. The epoll mode will handle net IO in OS
 * level by JNI.
 * <p>
 * NOTE : The netty app just run on Linux. E.g., if you run it on windows you will get a exception :
 * java.lang.UnsatisfiedLinkError: failed to load the required native library
 */
public class NettyByEPOLLStudy {
    public static void main(String... strings) throws InterruptedException {
        NettyServer.start(8080);
    }

    private static class NettyServer {
        static void start(int port) throws InterruptedException {
            final ByteBuf buf = Unpooled
                    .unreleasableBuffer(Unpooled.copiedBuffer("Hi Welcome to Netty!", Charset.forName("UTF-8")));

            EpollEventLoopGroup loopGroup = new EpollEventLoopGroup();

            try {
                ServerBootstrap bootstrap = new ServerBootstrap();
                bootstrap.group(loopGroup)
                        .channel(EpollServerSocketChannel.class)
                        .localAddress(new InetSocketAddress(port))
                        .childHandler(new ChannelInitializer<SocketChannel>() {
                            @Override
                            protected void initChannel(SocketChannel ch) throws Exception {
                                System.out.println("SocketChannel from server : " + ch.hashCode());
                                System.out.println("ChannelPipeline from server : " + ch.pipeline().hashCode());
                                ch.pipeline().addLast(new ChannelInboundHandlerAdapter() {
                                    public void channelActive(ChannelHandlerContext ctx) {
                                        ctx.writeAndFlush(buf.duplicate()).addListener(ChannelFutureListener.CLOSE);
                                    }
                                });
                            }
                        });
                ChannelFuture cfuture = bootstrap.bind().sync();
                cfuture.channel().closeFuture().sync();
            } finally {
                loopGroup.shutdownGracefully().sync();
            }
        }
    }
}
