package com.qinf.study.base.kit.netty.echo;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * Created by qinf on 2018/1/7 PM4:32:16
 */
public class EchoServer {

    private final int port;

    EchoServer(int port) {
        this.port = port;
    }

    public static void main(String... strings) throws InterruptedException {
        new EchoServer(8080).start();
    }

    public void start() throws InterruptedException {
        /**
         * 1. Create thread groups :
         * 	bossGroup used to handle accept connection from client.
         * 	workerGroup used to handle read & write(communicate with client).
         */
        NioEventLoopGroup bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            /**
             * 2. Create a server bootstrap instance :
             * 	  server bootstrap used to configure Netty(server app).
             */
            ServerBootstrap bootstrap = new ServerBootstrap();
            /** 2. Configure the bootstrap. */
            bootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class) // NioServerSocketChannel is used to instantiate a new Channel to accept incoming connections.
                    .option(ChannelOption.SO_BACKLOG, 128) // The option() used to configure NioServerSocketChannel.
                    .childOption(ChannelOption.SO_KEEPALIVE, true) // The childOption() used to configure accepted SocketChannel.
                    //.localAddress(new InetSocketAddress(port))
                    /**
                     * 3. Initialize channel used to handle data.
                     * NOTE : When server accepted a new connection from client,
                     * a new channel will be created. And then, a handler
                     * instance will be added into pipeline of the new created
                     * channel.
                     */
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            System.out.println("SocketChannel from server : " + ch.hashCode());
                            System.out.println("ChannelPipeline from server : " + ch.pipeline().hashCode());
                            ch.pipeline().addLast(new EchoServerHandler());
                        }
                    });

            /** 4. Bind the bootstrap and start to accept incoming connections. */
            ChannelFuture cfuture = bootstrap.bind(port).sync();
            System.out.println("Server has started and listening on : " + cfuture.channel().localAddress());
            cfuture.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully().sync();
            bossGroup.shutdownGracefully().sync();
        }
    }
}
