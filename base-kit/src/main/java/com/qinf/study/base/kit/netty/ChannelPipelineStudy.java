package com.qinf.study.base.kit.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;

/**
 * Created by qinf on 2018/1/7 PM10:45:05
 * <p>
 * Here we test the relation of Channel and ChannelPipeline. One Channel refer
 * to one ChannelPipleline, further to say is if server received a connection
 * from client, will create a Channel instance and a corresponding
 * ChannelPipeline instance of the Channel instance.
 */
public class ChannelPipelineStudy {

    public static void main(String... strings) throws InterruptedException {
        NettyServer.start(8080);
    }

    private static class NettyServer {
        static void start(int port) throws InterruptedException {
            final ByteBuf buf = Unpooled
                    .unreleasableBuffer(Unpooled.copiedBuffer("Hi Welcome to Netty!", Charset.forName("UTF-8")));
            NioEventLoopGroup loopGroup = new NioEventLoopGroup();
            try {
                ServerBootstrap bootstrap = new ServerBootstrap();
                bootstrap.group(loopGroup)
                        .channel(NioServerSocketChannel.class)
                        .localAddress(new InetSocketAddress(port))
                        .childHandler(new ChannelInitializer<SocketChannel>() {
                            @Override
                            protected void initChannel(SocketChannel ch) throws Exception {
                                //System.out.println("SocketChannel from server : " + ch.hashCode());
                                //System.out.println("ChannelPipeline from server : " + ch.pipeline().hashCode());
                                ch.pipeline().addLast(new ChannelInboundHandlerAdapter() {
                                    public void channelActive(ChannelHandlerContext ctx) {
                                        ctx.writeAndFlush(buf.duplicate()).addListener(ChannelFutureListener.CLOSE);
                                    }
                                });
                            }
                        });
                ChannelFuture cfuture = bootstrap.bind().sync();
                cfuture.channel().closeFuture().sync();
            } finally {
                loopGroup.shutdownGracefully().sync();
            }
        }
    }
}
