package com.qinf.study.base.kit.redis.jedis;

import com.qinf.study.base.kit.utils.PropsUtil;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.Objects;
import java.util.Properties;

/**
 * Created by qinf on 2018-09-09.
 */
public class JedisPoolManager {
    private static final Integer DEFAULT_DB_INDEX = 0;
    private static final JedisPool jPool;

    static {
        Properties props = PropsUtil.loadProps("config-redis/redis.properties");
        JedisPoolConfig jPoolConfig = new JedisPoolConfig();
        jPoolConfig.setMaxTotal(Integer.parseInt(props.getProperty("redis.pool.maxTotal", "4")));
        jPoolConfig.setMaxIdle(Integer.parseInt(props.getProperty("redis.pool.maxIdle", "4")));
        jPoolConfig.setMinIdle(Integer.parseInt(props.getProperty("redis.pool.minIdle", "1")));
        jPoolConfig.setMaxWaitMillis(Long.parseLong(props.getProperty("redis.pool.maxWaitMillis", "1024")));
        jPoolConfig.setTestOnBorrow("true".equalsIgnoreCase(props.getProperty("redis.pool.testOnBorrow", "true")));
        jPoolConfig.setTestOnReturn("true".equalsIgnoreCase(props.getProperty("redis.pool.testOnReturn", "true")));

        String[] redisServerAddr = props.getProperty("redis.server").split(":");
        jPool = new JedisPool(jPoolConfig, redisServerAddr[0], Integer.parseInt(redisServerAddr[1]), 2000, "123456");
    }

    private JedisPoolManager() {
    }

    public static Jedis borrowJedis() {
        Jedis jedis = jPool.getResource();
        jedis.select(DEFAULT_DB_INDEX);
        return jedis;
    }

    public static Jedis borrowJedisInDBIndex(Integer dbIndex) {
        Jedis jedis = jPool.getResource();
        jedis.select(Objects.isNull(dbIndex) ? DEFAULT_DB_INDEX : dbIndex);
        return jedis;
    }

    public static void returnJedis(Jedis jedis) {
        if (Objects.nonNull(jedis))
            jedis.close();
    }

    /**
     * When closing your application invoke this method.
     * Destroy whole connection(jedis) pool.
     */
    static void destroy() {
        if (Objects.nonNull(jPool))
            jPool.destroy();
    }

    public boolean ifIdle() {
        return jPool.getNumActive() > 0;
    }
}
