package com.qinf.study.base.kit.mybatis;

import com.qinf.study.base.kit.mybatis.domain.Player;
import com.qinf.study.base.kit.mybatis.mapper.PlayerMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;

/**
 * Created by qinf on 2017-10-01
 */

public class MyBatisNoSpringStudy {

    public static void main(String... strings) {
        SqlSessionFactory sqlSessionFactory;
        InputStream configIn = MyBatisNoSpringStudy.class
                .getClassLoader()
                .getResourceAsStream("config-mybatis/mybatis-config.xml");
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(configIn);
        SqlSession sqlSession = sqlSessionFactory.openSession();

        try {
            PlayerMapper playerDAO = sqlSession.getMapper(PlayerMapper.class);
            Player player = playerDAO.getPlayerById(25);
            System.out.println(player.getSkinCode() + " | " + player.getPlayerName());
        } finally {
            sqlSession.close();
        }
    }
}