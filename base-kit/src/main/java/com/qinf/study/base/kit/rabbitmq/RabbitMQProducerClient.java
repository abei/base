package com.qinf.study.base.kit.rabbitmq;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by qinf on 2018-12-12.
 * <p>
 * There are three important points need record:<p>
 * 1.The based procedure of initiating and using consumer as follows:<br>
 *   (1).Obtain connection(the concept of TCP connection) from connection pool.<br>
 *   (2).Create channel used for sending message based on connection.<br>
 *   (3).Bind the channel on Exchange.<br>
 *   (4).Perform sending.<br>
 *   (5).Resource collection.<br>
 * <p>
 * 2.The concept of routing key:<br>
 *   If you define a routing key consisted with three words delimited by dots,
 *   e.g., "<speed>.<colour>.<species>", used on Topic Exchange, then the messages
 *   sent to the topic exchange always tagged with the(a particular) routing key.
 * <p>
 * 3.The function of routing key:<br>
 *   A message sent with a particular routing key will be delivered to all
 *   the queues that are bound with a matching binding key.
 */
public class RabbitMQProducerClient {

    static class Producer {
        private String exchangeName;
        private BuiltinExchangeType exchangeType;
        private String routingKey;

        Producer(String exchangeName, BuiltinExchangeType exchangeType, String routingKey) {
            this.exchangeName = exchangeName;
            this.exchangeType = exchangeType;
            this.routingKey = routingKey;
        }

        private void doSend(String message) {
            try (Connection conn = ConnPoolManager.borrowConn();
                 Channel channel = conn.createChannel()) {
                channel.exchangeDeclare(exchangeName, exchangeType);
                channel.basicPublish(exchangeName, routingKey, null, message.getBytes("UTF-8"));
                TimeUnit.SECONDS.sleep(40);
            } catch (TimeoutException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * ExchangeName : ha.t_ + topicname(c1_allocate_service_topic)
     * ExchangeType : TOPIC
     * RoutingKey : ha.t_ + topicname(c1_allocate_service_topic) + . + tag(c1-allocate-service-tag)
     */
    public static void main(String... args) {
        /*RabbitMQProducerClient.Producer producer = new RabbitMQProducerClient.Producer("TOPIC_EXCHANGE", BuiltinExchangeType.TOPIC,
                "TOPIC_EXCHANGE.ROUTINGKEY.CURRENCY.RMB");*/

        RabbitMQProducerClient.Producer producer = new RabbitMQProducerClient.Producer(
                "ha.t_c1_allocate_service_topic",
                BuiltinExchangeType.TOPIC,
                "ha.t_c1_allocate_service_topic.c1-allocate-service-tag");
        /**
         * The message will be sent to topic exchange tagged with specified routing key.
         */
        producer.doSend("Hello rabbit mq!, my name is RRC.");
    }
}
