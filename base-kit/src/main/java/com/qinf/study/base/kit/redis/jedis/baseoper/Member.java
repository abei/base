package com.qinf.study.base.kit.redis.jedis.baseoper;

import java.io.*;

/**
 * Created by qinf on 2018-11-10.
 */
public class Member implements Serializable {
    private String name;
    private Integer age;

    public Member() {
    }

    public Member(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public static byte[] serialize(Member person) throws IOException{
        ByteArrayOutputStream baos = null;
        ObjectOutputStream oos = null;

        try {
            baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
            oos.writeObject(person);
            oos.flush();
            return baos.toByteArray();
        } finally {
            oos.close();
            baos.close();
        }
    }

    public static Member deserialize(byte[] byteArray) throws ClassNotFoundException, IOException {
        ObjectInputStream ois = null;
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(byteArray);
            ois = new ObjectInputStream(bais);
            return (Member) ois.readObject();
        } finally {
            ois.close();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Member{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
