package com.qinf.study.base.kit.netty.echo;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * Created by qinf on 2018/1/7 PM8:24:09
 */
public class EchoClient {

    private final static EventLoopGroup loopGroup;

    static {
        loopGroup = new NioEventLoopGroup();
    }

    private final String host;
    private final int port;

    EchoClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public static void main(String... strings) throws InterruptedException {
        for (int i = 0; i < 2; i++)
            new EchoClient("localhost", 8080).start();
    }

    public void start() throws InterruptedException {
        //EventLoopGroup loopGroup = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(loopGroup)
                    .channel(NioSocketChannel.class) // NioSocketChannel is being used to create a client-side Channel.
                    .option(ChannelOption.SO_KEEPALIVE, true) // Client-side SocketChannel does not have a parent.
                    //.remoteAddress(new InetSocketAddress(host, port))
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            //System.out.println("SocketChannel from client : " + ch.hashCode());
                            //System.out.println("ChannelPipeline from client : " + ch.pipeline().hashCode());
                            ch.pipeline().addLast(new EchoClientHandler());
                        }
                    });

            /** Connect to remote server until connection established. */
            ChannelFuture cfuture = bootstrap.connect(host, port).sync();
            /** Wait until the connection is closed. */
            cfuture.channel().closeFuture().sync();
        } finally {
            loopGroup.shutdownGracefully().sync();
        }
    }
}
