package com.qinf.study.base.kit.netty.echo;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;

import java.util.concurrent.TimeUnit;

/**
 * Created by qinf on 2018/1/7 PM8:24:26
 */
public class EchoClientHandler extends SimpleChannelInboundHandler<ByteBuf> {

    /**
     * Once the connection to server has established, will invoke this method.
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        //System.out.println("Client has sent a message to remote server.");
        System.out.println("Client has connected to remote server successfully.");
        TimeUnit.MILLISECONDS.sleep(500L);
        //ctx.writeAndFlush(Unpooled.copiedBuffer("HELLO NETTY", CharsetUtil.UTF_8));
    }

    /**
     * Once received a message from remote server will invoke this method.
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
        System.out.println("Client received data from remote server : " + msg.toString(CharsetUtil.UTF_8));
        TimeUnit.MILLISECONDS.sleep(500L);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }

}
