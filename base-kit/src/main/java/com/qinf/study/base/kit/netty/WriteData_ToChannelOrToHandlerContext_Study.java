package com.qinf.study.base.kit.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;

/**
 * Created by qinf on 2018/1/8 PM9:02:33
 */
public class WriteData_ToChannelOrToHandlerContext_Study {
    public static void main(String... strings) throws InterruptedException {
        NettyServer.start(8080);
    }

    private static class NettyServer {
        static final ByteBuf bufOfCxt = Unpooled
                .unreleasableBuffer(Unpooled.copiedBuffer("Hi, welcome to Netty from CXT!", Charset.forName("UTF-8")));
        static final ByteBuf bufOfChl = Unpooled
                .unreleasableBuffer(Unpooled.copiedBuffer("Hi, welcome to Netty from CHL!", Charset.forName("UTF-8")));

        static void start(int port) throws InterruptedException {
            NioEventLoopGroup loopGroup = new NioEventLoopGroup();
            try {
                ServerBootstrap bootstrap = new ServerBootstrap();
                bootstrap.group(loopGroup)
                        .channel(NioServerSocketChannel.class)
                        .localAddress(new InetSocketAddress(port))
                        .childHandler(new ChannelInitializer<SocketChannel>() {
                            @Override
                            protected void initChannel(SocketChannel ch) throws Exception {
                                /** 1. Write data to ChannelHandlerContext. */
                                ch.pipeline().addLast(new ChannelInboundHandlerAdapter() {
                                    public void channelActive(ChannelHandlerContext ctx) {
                                        ctx.writeAndFlush(bufOfCxt.duplicate()).addListener(ChannelFutureListener.CLOSE);
                                    }
                                });

                                /** 2. Write data directly to Channel. */
                                ChannelFuture statusOfWriteData = ch.writeAndFlush(bufOfChl);
                                statusOfWriteData.addListener(new ChannelFutureListener() {
                                    @Override
                                    public void operationComplete(ChannelFuture future) throws Exception {
                                        if (future.isSuccess())
                                            System.out.println("Write data to client successfully.");
                                        else
                                            System.out.println("Write data to client failed : " + future.cause());
                                    }
                                });
                                /** NOTE : At this time, we write twice to a same channel(connection from client) */
                            }
                        });
                ChannelFuture cfuture = bootstrap.bind().sync();
                cfuture.channel().closeFuture().sync();
            } finally {
                loopGroup.shutdownGracefully().sync();
            }
        }
    }
}
