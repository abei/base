package com.qinf.study.base.kit.redis.jedis.baseoper;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.qinf.study.base.kit.redis.jedis.JedisPoolManager;
import com.qinf.study.base.kit.redis.jedis.RedisClient;
import com.sun.corba.se.spi.orbutil.threadpool.Work;
import lombok.Data;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.CharsetDecoder;
import java.util.Objects;

/**
 * Created by qinf on 2018-11-10.
 */
public class JedisBeanOperStudy {

    @Data
    static class Worker {
        private String name;
        private Integer age;
    }


    public static void main(String... args) throws IOException, ClassNotFoundException {
        Jedis jedis = JedisPoolManager.borrowJedis();

        try {
            /**
             * Serialize bean by jdk serialization mechanism.
             */
            Member member = new Member();
            member.setName("QF");
            member.setAge(20);
            // Serialize object.
            byte[] memberInBytes = Member.serialize(member);
            jedis.set("key".getBytes(), memberInBytes);
            member = null;
            byte[] memberInBytesFromRedis = jedis.get("key".getBytes());
            // Deserialize object.
            member = Member.deserialize(memberInBytesFromRedis);
            System.out.println(member);

            /**
             * Serialize bean by json serialization mechanism.
             */
            Worker worker = new Worker(); // no need implements Serializable interface
            worker.setName("QS");
            worker.setAge(20);
            // Serialize object.
            byte[] workerInBytes = JSON.toJSONBytes(worker);
            jedis.set("key_json".getBytes(), workerInBytes);
            byte[] workerInBytesFromRedis = jedis.get("key_json".getBytes());
            // Deserialize object.
            String string = new String(workerInBytesFromRedis, "UTF-8");
            System.out.println(string);
            Object object = JSON.parse(workerInBytesFromRedis);
            System.out.println(object);
        } finally {
            if (Objects.nonNull(jedis)) {
                RedisClient.destroy();
            }
        }
    }
}
